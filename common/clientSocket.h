#ifndef CLIENT_SOCKET_H
#define CLIENT_SOCKET_H
#include "socket.h"
#include <string>

/**
 * Clase para conectarse a un socket en la dirección y puertos
 * pasados por parámetros. Llamar a connectSocket() para
 * realizar la conexión.
 */
class ClientSocket : public Socket{
public:
    ClientSocket(std::string &address, int port);
    ClientSocket(){};
    void assignIpPort(std::string &address, int port);
    void connectSocket();
};

#endif
