#ifndef LEVELGRID_H_
#define LEVELGRID_H_
#include <iostream>
#include <vector>


class LevelGrid{

public:
   typedef std::vector <int> vectorint_t;
   typedef std::vector <vectorint_t > grilla_t;
   void  create(int ancho,int alto);
  ~LevelGrid();
  std::vector <std::vector<int> > getGrilla();
  void setXY(int x,int y,int type);
  int getData(int x,int y);
  int getAlto();
  int getAncho();
  int getDificultad();
  void setEditado(bool ed);
  bool getEditado();
  void setGrilla(std::vector< std::vector<int> > grilla);
  void setDificultad(int dif);
  //std::vector<std::string> convertToString();
  //bool convertToXmlnSave(std::string filename);//hacer una vez que me funcione la logica de la grilla.
  protected:

  grilla_t grilla;
  int ancho,alto;
  int dificultad;
  //solo para el uso del editor
  bool editado;
};

#endif //Fin LEVELGRID_H_
  
  

