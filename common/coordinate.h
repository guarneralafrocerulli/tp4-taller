#ifndef COORDINATE_H
#define COORDINATE_H
struct Coordinate{
	int x, y;
	Coordinate(){
		x = 0;
		y = 0;
	}
	Coordinate(int x,int y){
		this->x = x;
		this->y = y;
	}
};
#endif