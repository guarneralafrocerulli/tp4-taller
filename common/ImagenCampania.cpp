#include "ImagenCampania.h"

ImagenCampania::ImagenCampania(){
	this->pixels = 0;
	this->rowstride=0;
	this->ancho=0;
	this->bitspersample=0;
	this->hasalpha=false;
	this->alto=0;
}

ImagenCampania::ImagenCampania(ImagenCampania* imagen){
	this->rowstride=imagen->rowstride;
	this->alto=imagen->alto;
	this->ancho=imagen->ancho;
	this->hasalpha=imagen->hasalpha;
	this->bitspersample=imagen->bitspersample;
	this->pixels = 0;
	if (imagen->pixels){
		this->pixels=imagen->pixels;
	}
	if(!imagen->imagenBase64.empty()){
		this->imagenBase64=imagen->imagenBase64;
	}
}

std::string ImagenCampania::getImagenBase64()
{
return (this->imagenBase64);
}
void ImagenCampania::setImagenBase64(std::string imagen64)
{
 this->imagenBase64= imagen64;

}

int ImagenCampania::getAlto()
{
return this->alto;
}
int ImagenCampania::getAncho()
{
return this->ancho;
}
bool ImagenCampania::gethasalpha()
{
return this->hasalpha;
}
int ImagenCampania::getBitspersample()
{
return this->bitspersample;
}

unsigned char* ImagenCampania::getPixels()
{
return this->pixels;
}
int ImagenCampania::getRowstride()
{
return this->rowstride;
}

void ImagenCampania::setAlpha(bool alpha)
{
this->hasalpha=alpha;
}
void ImagenCampania::setAlto(int alto)
{
this->alto=alto;
}
void ImagenCampania::setAncho(int ancho)
{
this->ancho=ancho;
}
void ImagenCampania::setBitspersample(int bits)
{
this->bitspersample=bits;
}
void ImagenCampania::setRowstride(int row)
{
this->rowstride=row;
}
void ImagenCampania::setPixels(unsigned char* pixels)
{
 this->pixels=pixels;
}

void ImagenCampania::setAlpha(std::string salpha)
{
   if (salpha.compare("true")==0){
     this->hasalpha=true;
   }else{
     this->hasalpha=false;
   }
}
