#include <cstdio>
#include "Serializadores.h"
#include <sstream>
////constantes////
#define COD11 'a'
#define COD21 'b'
#define COD31 'c'
#define COD41 'd'
#define COD12 'e'
#define COD22 'f'
#define COD32 'g'
#define COD42 'h'
#define COD13 'i'
#define COD23 'j'
#define COD33 'k'
#define COD43 'l'
///////////////////
using std::stringstream;

char SerializadorMapa::codificar(int cod)
{
  switch(cod){
    case 11 :
      return COD11;
    case 12 :
      return COD12;
    case 13 :
      return COD13;
    case 21 :
      return COD21;
    case 22 :
      return COD22;
    case 23 :
      return COD23;
    case 31 :
      return COD31;
    case 32 :
      return COD32;
    case 33 :
      return COD33;
    case 41 :
      return COD41;
    case 42 :
      return COD42;
    case 43 :
      return COD43;
    default :
      return '0';
  }
}
       
int SerializadorMapa::decodificar(char cod)
{  switch(cod){
    case COD11 :
      return 11;
    case COD12 :
      return 12;
    case COD13 :
      return 13;
    case COD21 :
      return 21;
    case COD22 :
      return 22;
    case COD23 :
      return 23;
    case COD31 :
      return 31;
    case COD32 :
      return 32;
    case COD33 :
      return 33;
    case COD41 :
      return 41;
    case COD42 :
      return 42;
    case COD43 :
      return 43;
    default :
      return 0;
  }
}
vector<vector<int> > SerializadorMapa::deserializar(string mapa){
	/*pasa de formato xml a vector<vector<int>>*/
	vector<vector<int> > aux;
	vector<string> vector_de_strings;
	TiXmlDocument xmap("mapa");
	xmap.Parse((const char*)mapa.c_str(),0,TIXML_ENCODING_UTF8);
	//xmap.SaveFile("pepe2.xml");
	TiXmlElement* root=xmap.FirstChildElement();
	for(TiXmlElement* element=root->FirstChildElement();element;
	    element=element->NextSiblingElement() ){
		vector_de_strings.push_back(element->Attribute("fila"));
	}
	aux=convertirStringAVector(vector_de_strings);
	//delete xmap;
	return aux;
}
TiXmlElement* SerializadorMapa::serializarAElemento(vector< vector< int > > mapa,string id) { 
	int x=0; 
	string f="fila";
	vector<string> aux=convertirVectorAString(mapa);
	TiXmlElement *root=new TiXmlElement("nivel"+id); 
	vector< string >::const_iterator iter_fila; 
	for (iter_fila=aux.begin();iter_fila!=aux.end();++iter_fila){ 
		stringstream ss; ss <<x; 
		TiXmlElement *fila=new TiXmlElement(f+ss.str()); 
		fila->SetAttribute("fila",(*iter_fila));
		root->LinkEndChild(fila);
		x++;
	} 
	return root; 
}
vector< vector< int > > SerializadorMapa::deserializarAElemento(TiXmlElement* elem)
{     
        vector<vector<int> > aux;
	vector<string> vector_de_strings;
	for(TiXmlElement* element=elem->FirstChildElement();element!=NULL; element=element->NextSiblingElement() ){
		vector_de_strings.push_back(element->Attribute("fila"));
		
	}
	
	aux=convertirStringAVector(vector_de_strings);
	
	return aux;
}
vector<vector<int> > SerializadorMapa::convertirStringAVector(vector<string> mapa){
	/*convierte un mapa en formato vector<string> a formato vector<vector<int>>*/
	vector<string>:: const_iterator iterador;
	vector< vector<int> > aux;
	for(iterador=mapa.begin();iterador!=mapa.end();++iterador){
		vector<int> aux_fila;
		string:: const_iterator iter_string;
		for(iter_string=(*iterador).begin();iter_string!=(*iterador).end();++iter_string){
			int aux = this->decodificar(*iter_string);
			if(aux==0){
				int valor;
				stringstream ss;
				ss<<(*iter_string);
				ss>>valor;
				aux_fila.push_back(valor);
			}else{
				aux_fila.push_back(aux);
			}
		}
		aux.push_back(aux_fila);
	}
	return aux;
}

vector<string> SerializadorMapa::convertirVectorAString(vector<vector<int> > mapa){
	/*convierte vector<vector<int>> a vector<string>*/
	vector<string> aux;

	vector< vector<int> >::const_iterator iter_fila;
	vector<int>::const_iterator iter_columna;
	for (iter_fila=mapa.begin();iter_fila!=mapa.end();++iter_fila){
		string fila="";
		for (iter_columna=(*iter_fila).begin();iter_columna!=(*iter_fila).end();++iter_columna){
		    char aux=this->codificar(*iter_columna);
		    if (aux=='0'){
			stringstream ss;
			ss<< (*iter_columna);
			fila=fila+ss.str();
		    }else{
		      fila.push_back(aux);
		    }
		}
		aux.push_back(fila);
	}
	return aux;
}



string SerializadorMapa::serializar(vector<vector<int> > mapa){
	/*convierte el mapa en un string con formato XML*/
	int x=0;
	string f="fila";
	TiXmlPrinter printer;
	vector<string> aux=convertirVectorAString(mapa);
	TiXmlDocument xmap("mapa");
	TiXmlElement *root=new TiXmlElement("Mapa");
	xmap.LinkEndChild(root);
	vector< string >::const_iterator iter_fila;
	for (iter_fila=aux.begin();iter_fila!=aux.end();++iter_fila){
		stringstream ss;
		ss <<x;
		TiXmlElement *fila=new TiXmlElement(f+ss.str());
		fila->SetAttribute("fila",(*iter_fila));
		root->LinkEndChild(fila);
		x++;
	}
	xmap.Accept(&printer);
	return printer.CStr();
}
	
	

string SerializadorEvento::serializar(EventoAsignarIdJugador* evento){
	TiXmlDocument xmap("evento");
	TiXmlPrinter printer;
	TiXmlElement *root=new TiXmlElement("EventoAsignarIdJugador");
	xmap.LinkEndChild(root);
	root->SetAttribute("id",evento->getId());
	xmap.Accept(&printer);
	return printer.CStr();
}
	
EventoAsignarIdJugador* SerializadorEvento::deserializarAsignarJugador(string evento){
	stringstream sid;
	int id;
	TiXmlDocument xml("evento");
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	string aux=root->Attribute("id");
	sid<<aux;
	sid>>id;
	return new EventoAsignarIdJugador(id);
}



string SerializadorEvento::serializar(EventoPosicion* evento){
	TiXmlDocument xmap("evento");
	TiXmlPrinter printer;
	TiXmlElement *root=new TiXmlElement("EventoPosicion");
	xmap.LinkEndChild(root);
	root->SetAttribute("id",evento->getId());
	root->SetAttribute("x",evento->getX());
	root->SetAttribute("y",evento->getY());
	xmap.Accept(&printer);
	return printer.CStr();
}

string SerializadorEvento::serializar(Evento* evento){
	return evento->serializar(this);
}

string SerializadorEvento::serializar(EventoJugador* evento){
	TiXmlDocument xml("evento");
	TiXmlPrinter printer;
	TiXmlElement *root=new TiXmlElement("EventoJugador");
	xml.LinkEndChild(root);
	root->SetAttribute("id",evento->getId());
	root->SetAttribute("tipo",evento->getTipo());
	xml.Accept(&printer);
	return printer.CStr();
}

string SerializadorEvento::serializar(EventoModificarPuntuacion* evento){
	TiXmlDocument xml("evento");
	TiXmlPrinter printer;
	TiXmlElement *root=new TiXmlElement("EventoModificarPuntuacion");
	xml.LinkEndChild(root);
	root->SetAttribute("id",evento->getId());
	root->SetAttribute("puntuacion",evento->getPunt());
	xml.Accept(&printer);
	return printer.CStr();
}

string SerializadorEvento::serializar(EventoModificarVisibilidad* evento){
	TiXmlDocument xml("evento");
	TiXmlPrinter printer;
	TiXmlElement *root=new TiXmlElement("EventoModificarVisibilidad");
	xml.LinkEndChild(root);
	root->SetAttribute("id",evento->getId());
	root->SetAttribute("visible",evento->getVisibilidad());
	xml.Accept(&printer);
	return printer.CStr();
}
		
Evento* SerializadorEvento::deserializar(string evento){
	TiXmlDocument xml("evento");
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	Evento* even=NULL;
	printf("Deserializando %s\n", root->ValueStr().c_str());
	if(root->ValueStr()=="EventoPosicion"){
		even=deserializarPosicion(evento);
	}
	if(root->ValueStr()=="EventoJugador"){
		even=deserializarEventoJugador(evento);
	}
	if(root->ValueStr()=="EventoNuevoElemento"){
		even=deserializarNuevoElemento(evento);
	}
	if(root->ValueStr()=="EventoModificarVisibilidad"){
		even=deserializarModificarVisibilidad(evento);
	}
	if(root->ValueStr()=="EventoModificarCantidadVidas"){
		even=deserializarCantVidas(evento);
	}
	return even;
}

EventoModificarCantidadVidas* SerializadorEvento::deserializarCantVidas(string evento){
	stringstream ssid;
	stringstream ssvida;
	int id;
	int vida;
	TiXmlDocument xml("evento");
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	string ids=root->Attribute("id");
	string vidas=root->Attribute("vidas");
	ssid<<ids;
	ssid>>id;
	ssvida<<vidas;
	ssvida>>vida;
	return new EventoModificarCantidadVidas(id,vida);
}

EventoModificarVisibilidad* SerializadorEvento::deserializarModificarVisibilidad(string evento){
	stringstream ss;
	stringstream ssid;
	int id;
	bool visible;
	TiXmlDocument xml("evento");
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	string ids=root->Attribute("id");
	string vis=root->Attribute("visible");
	ssid<<ids;
	ssid>>id;
	ss<<vis;
	ss>>visible;
	return new EventoModificarVisibilidad(id,visible);
}

EventoNuevoElemento* SerializadorEvento::deserializarNuevoElemento(string evento){
	stringstream sid;
	stringstream sx;
	stringstream sy;
	int id;
	int x;
	int y;
	TiXmlDocument xml("evento");
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	string tipo=root->Attribute("tipo");
	root=root->NextSiblingElement();
	string aux=root->Attribute("id");
	sid<<aux;
	sid>>id;
	aux=root->Attribute("x");
	sx<<aux;
	sx>>x;
	aux=root->Attribute("y");
	sy<<aux;
	sy>>y;
	return new EventoNuevoElemento(tipo,id,x,y);
}

EventoPosicion* SerializadorEvento::deserializarPosicion(string evento){
	stringstream sid;
	stringstream sx;
	stringstream sy;
	int id;
	int x;
	int y;
	TiXmlDocument xml("evento");
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	string aux=root->Attribute("id");
	sid<<aux;
	sid>>id;
	aux=root->Attribute("x");
	sx<<aux;
	sx>>x;
	aux=root->Attribute("y");
	sy<<aux;
	sy>>y;
	EventoPosicion* nuevo=new EventoPosicion(id,x,y);
	return nuevo;
}

EventoJugador* SerializadorEvento::deserializarEventoJugador(string evento){
	stringstream sid;
	int id;
	TiXmlDocument xml("evento");
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	string tipo=root->Attribute("tipo");
	string aux=root->Attribute("id");
	sid<<aux;
	sid>>id;
	return new EventoJugador(tipo,id);
}

string SerializadorEvento::serializar(EventoNuevoElemento* evento){
	TiXmlDocument xml("evento");
	TiXmlPrinter printer;
	TiXmlElement *root=new TiXmlElement("EventoNuevoElemento");
	xml.LinkEndChild(root);
	TiXmlElement *pos=new TiXmlElement("EventoPosicion");
	xml.LinkEndChild(pos);
	pos->SetAttribute("id",evento->getEventoAux().getId());
	pos->SetAttribute("x",evento->getEventoAux().getX());
	pos->SetAttribute("y",evento->getEventoAux().getY());
	root->SetAttribute("tipo",evento->getTipo());
	xml.Accept(&printer);
	return printer.CStr();
}

string SerializadorEvento::serializar(EventoModificarCantidadVidas* evento){
	TiXmlDocument xml("evento");
	TiXmlPrinter printer;
	TiXmlElement *root=new TiXmlElement("EventoModificarCantidadVidas");
	xml.LinkEndChild(root);
	root->SetAttribute("id",evento->getId());
	root->SetAttribute("vidas",evento->getCantVidas());
	xml.Accept(&printer);
	return printer.CStr();
}


EventoModificarPuntuacion* SerializadorEvento::deserializarPuntuacion(string evento){
	stringstream sid;
	stringstream spunt;
	int id;
	int punt;
	TiXmlDocument xml("evento");
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	string ids=root->Attribute("id");
	string aux=root->Attribute("puntuacion");
	sid<<ids;
	sid>>id;
	spunt<<aux;
	spunt>>punt;
	return new EventoModificarPuntuacion(id,punt);
}
//////////////////////////////////////////
string SerializadorEvento::serializar(EventoClienteCrearPartida* evento){
	TiXmlDocument xml("evento");
	TiXmlPrinter printer;
	TiXmlElement* root=new TiXmlElement("EventoCliente");
	TiXmlElement *cliente=new TiXmlElement("EventoClienteCrearPartida");
	xml.LinkEndChild(root);
	root->LinkEndChild(cliente);
	cliente->SetAttribute("id",evento->getId());
	cliente->SetAttribute("nombre_jugador",evento->getNombreJugador());
	cliente->SetAttribute("nombre_partida",evento->getNombrePartida());
	cliente->SetAttribute("cantidad_jugadores",evento->getCantJugadores());
	xml.Accept(&printer);
	return printer.CStr();
}

EventoClienteCrearPartida* SerializadorEvento::
		deserializarClienteCrearPartida(string evento){
	stringstream ids;
	stringstream cjug;
	
	TiXmlDocument xml("evento");
	int id;
	int cant_jugadores;
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	if(!root) return NULL;
	TiXmlElement* cliente=root->FirstChildElement();
	ids<<cliente->Attribute("id");
	ids>>id;
	cjug<<cliente->Attribute("cantidad_jugadores");////////////////sacar talvez
	cjug>>cant_jugadores;
	string nombreJugador=cliente->Attribute("nombre_jugador");
	string nombrePartida = cliente->Attribute("nombre_partida");
	return new EventoClienteCrearPartida(id, nombrePartida ,nombreJugador,cant_jugadores);
	}

	
string SerializadorEvento::serializar(EventoClientePedirLista* evento){
	TiXmlDocument xml("evento");
	TiXmlPrinter printer;
	TiXmlElement* root=new TiXmlElement("EventoCliente");
	TiXmlElement *cliente=new TiXmlElement("PedirLista");
	xml.LinkEndChild(root);
	root->LinkEndChild(cliente);
	cliente->SetAttribute("tipo",evento->getTipo());
	xml.Accept(&printer);
	return printer.CStr();
}
EventoClientePedirLista* SerializadorEvento::deserializarPedirLista(string evento){
	stringstream ss;
	TiXmlDocument xml("evento");
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	TiXmlElement* cliente=root->FirstChildElement();
	return new EventoClientePedirLista(cliente->Attribute("tipo"));
}

string SerializadorEvento::
	serializar(EventoClienteUnirsePartida* evento){
		TiXmlDocument xml("evento");
	TiXmlPrinter printer;
	TiXmlElement* root=new TiXmlElement("EventoCliente");
	TiXmlElement *cliente=new TiXmlElement("EventoClienteUnirsePartida");
	xml.LinkEndChild(root);
	root->LinkEndChild(cliente);
	cliente->SetAttribute("id",evento->getId());
	cliente->SetAttribute("nombre_jugador",evento->getNombreJugador());
	xml.Accept(&printer);
	return printer.CStr();
	
}

EventoClienteUnirsePartida* SerializadorEvento::
			deserializarUnirsePartida(string evento){
	stringstream ss;
	int id;
	TiXmlDocument xml("evento");
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	TiXmlElement* cliente=root->FirstChildElement();
	ss<<cliente->Attribute("id");
	ss>>id;
	string nombreJugador=cliente->Attribute("nombre_jugador");
	return new EventoClienteUnirsePartida(id,nombreJugador);
}
			
string SerializadorEvento::serializar(EventoServerLista* evento){
	int indice=0;
	string elem="elemento";
	TiXmlDocument xml("evento");
	TiXmlPrinter printer;
	TiXmlElement* root=new TiXmlElement("EventoServer");
	TiXmlElement *cliente=new TiXmlElement("EventoServerLista");
	xml.LinkEndChild(root);
	root->LinkEndChild(cliente);
	vector<PartidaCampania*> lista=evento->getLista();
	vector<PartidaCampania*>::const_iterator iter_lista; 
	for (iter_lista=lista.begin();iter_lista!=lista.end();++iter_lista){ 
		stringstream ss;
		ss <<indice; 
		TiXmlElement *elemento=new TiXmlElement(elem+ss.str()); 
		elemento->SetAttribute("id",(*iter_lista)->getId());
		elemento->SetAttribute("nombre",(*iter_lista)->getNombre());
		elemento->SetAttribute("dificultad",(*iter_lista)->getDificultad());
		elemento->SetAttribute("cantidad_jugadores",(*iter_lista)->getCantJugadores());
		/*elemento->SetAttribute("pixbufpixels",(*iter_lista)->getImagen()->getImagenBase64());
		elemento->SetAttribute("ancho",(*iter_lista)->getImagen()->getAncho());
		elemento->SetAttribute("alto",(*iter_lista)->getImagen()->getAlto());
		elemento->SetAttribute("rowstride",(*iter_lista)->getImagen()->getRowstride());
		elemento->SetAttribute("hasalpha",(*iter_lista)->getImagen()->gethasalpha());
		elemento->SetAttribute("bitpersample",(*iter_lista)->getImagen()->getBitspersample());*/
		cliente->LinkEndChild(elemento);
		indice++;
	} 
	xml.Accept(&printer);
	return printer.CStr();
}
string SerializadorEvento::serializar(EventoServerImagen* evento){
	TiXmlDocument xml("evento");
	TiXmlPrinter printer;
	TiXmlElement* root=new TiXmlElement("EventoServer");
	TiXmlElement *elemento=new TiXmlElement("EventoServerImagen");
	elemento->SetAttribute("pixbufpixels",evento->getImagen()->getImagenBase64());
	elemento->SetAttribute("ancho",evento->getImagen()->getAncho());
	elemento->SetAttribute("alto",evento->getImagen()->getAlto());
	elemento->SetAttribute("rowstride",evento->getImagen()->getRowstride());
	elemento->SetAttribute("hasalpha",evento->getImagen()->gethasalpha());
	elemento->SetAttribute("bitpersample",evento->getImagen()->getBitspersample());
	root->LinkEndChild(elemento);
	xml.LinkEndChild(root);
	xml.Accept(&printer);
	return printer.CStr();
}

EventoServerImagen* SerializadorEvento::deserializarImagen(string evento){
	TiXmlDocument xml("evento");
	//EventoServerImagen* eventol=new EventoServerImagen();
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	if(!root) return NULL;
	if(root->ValueStr()!="EventoServer")return NULL;
	TiXmlElement* element=root->FirstChildElement();
	if(!element) return NULL;
	if(element->ValueStr()!="EventoServerImagen")return NULL;
	ImagenCampania* imagen=NULL;
	if(!element->Attribute("pixbufpixels"))return NULL;
	if(!element->Attribute("alto"))return NULL;
	if(!element->Attribute("ancho"))return NULL;
	if(!element->Attribute("hasalpha"))return NULL;
	if(!element->Attribute("bitpersample"))return NULL;
	if(!element->Attribute("rowstride"))return NULL;
	imagen=new ImagenCampania();
	imagen->setImagenBase64(element->Attribute("pixbufpixels"));
	imagen->setAlto(atoi(element->Attribute("alto")));
	imagen->setAncho(atoi(element->Attribute("ancho")));
	imagen->setAlpha(element->Attribute("hasalpha"));
	imagen->setBitspersample(atoi(element->Attribute("bitpersample")));
	imagen->setRowstride(atoi(element->Attribute("rowstride")));
	return new EventoServerImagen(imagen);
}


EventoServerLista* SerializadorEvento::deserializarListas(string evento){
	TiXmlDocument xml("evento");
	EventoServerLista* eventol=new EventoServerLista();
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	root=root->FirstChildElement();
	for(TiXmlElement* element=root->FirstChildElement();element;
			element=element->NextSiblingElement() ){
		stringstream ss;
		stringstream ids;
		stringstream dif;
		int id;
		int cant_jugadores;
		int dificultad;
		ids<<element->Attribute("id");
		ids>>id;
		ss<<element->Attribute("cantidad_jugadores");
		ss>>cant_jugadores;
		dif<<element->Attribute("dificultad");
		dif>>dificultad;
		/*ImagenCampania* imagen=NULL;
		imagen=new ImagenCampania();
		imagen->setImagenBase64(element->Attribute("pixbufpixels"));
		imagen->setAlto(atoi(element->Attribute("alto")));
		imagen->setAncho(atoi(element->Attribute("ancho")));
		imagen->setAlpha(element->Attribute("hasalpha"));
		imagen->setBitspersample(atoi(element->Attribute("bitpersample")));
		imagen->setRowstride(atoi(element->Attribute("rowstride")));*/
		eventol->agregarPartida(id,element->Attribute("nombre"),dificultad,cant_jugadores);
	}
	return eventol;	
}

string SerializadorEvento::serializar(EventoServerEmpezarPartida* evento){
	TiXmlDocument xml("evento");
	TiXmlPrinter printer;
	TiXmlElement* root=new TiXmlElement("EventoServer");
	TiXmlElement* evento_s=new TiXmlElement("EventoServerEmpezarPartida");
	evento_s->SetAttribute("empezar",evento->getEmpezar());
	xml.LinkEndChild(root);
	root->LinkEndChild(evento_s);
	xml.Accept(&printer);
	return printer.CStr();
}

string SerializadorEvento::serializar(EventoServerUnirPartida* evento){
	TiXmlDocument xml("evento");
	TiXmlPrinter printer;
	TiXmlElement* root=new TiXmlElement("EventoServer");
	TiXmlElement* evento_s=new TiXmlElement("EventoServerUnirPartida");
	evento_s->SetAttribute("unir",evento->getUnir());
	xml.LinkEndChild(root);
	root->LinkEndChild(evento_s);
	xml.Accept(&printer);
	return printer.CStr();
}

EventoServerUnirPartida* SerializadorEvento::deserializarServerUnirPartida(string evento){
	TiXmlDocument xml("evento");
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	root=root->FirstChildElement();
	stringstream ss;
	bool unido;
	ss<<root->Attribute("unir");
	ss>>unido;
	return new EventoServerUnirPartida(unido);
}

EventoServerEmpezarPartida* SerializadorEvento::deserializarServerEmpezarPartida(string evento){
	TiXmlDocument xml("evento");
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	if(!root) return NULL;
	if(root->ValueStr()!="EventoServer") return NULL;
	root=root->FirstChildElement();
	if(!root) return NULL;
	if(root->ValueStr()!="EventoServerEmpezarPartida") return NULL;
	stringstream ss;
	bool empezar;
	std::cout<<evento<<std::endl;
	ss<<root->Attribute("empezar");
	ss>>empezar;
	return new EventoServerEmpezarPartida(empezar);
}

string SerializadorEvento::serializar(EventoClienteEmpezarPartida* ){
	TiXmlDocument xml("evento");
	TiXmlPrinter printer;
	TiXmlElement* root=new TiXmlElement("EventoCliente");
	TiXmlElement* evento_s=new TiXmlElement("EventoClienteEmpezarPartida");
	xml.LinkEndChild(root);
	root->LinkEndChild(evento_s);
	xml.Accept(&printer);
	return printer.CStr();
}

EventoClienteEmpezarPartida* SerializadorEvento::
			deserializarClienteEmpezarPartida(string){
			return new EventoClienteEmpezarPartida();
			}
			
string SerializadorEvento::serializar(EventoNuevoMapa* evento){
	SerializadorMapa ser;
	return ser.serializar(evento->getMapa());
}

EventoNuevoMapa* SerializadorEvento::deserializarNuevoMapa(string evento){
		SerializadorMapa ser;
		vector< vector<int> > mapa;
		mapa=ser.deserializar(evento);
		return new EventoNuevoMapa(mapa);
}

string SerializadorEvento::serializar(EventoDesconectar* evento){
	TiXmlDocument xml("evento");
	TiXmlPrinter printer;
	TiXmlElement* root=new TiXmlElement("EventoDesconectar");
	root->SetAttribute("id",evento->getId());
	xml.LinkEndChild(root);
	xml.Accept(&printer);
	return printer.CStr();
}

EventoDesconectar* SerializadorEvento::deserializarDesconectar(string evento){
	TiXmlDocument xml("evento");
	int id;
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	root=root->FirstChildElement();
	stringstream ss;
	ss<<root->Attribute("id");
	ss>>id;
	return new EventoDesconectar(id);
}


string SerializadorEvento::serializar(EventoFinDelJuego* ){
	TiXmlDocument xml("evento");
	TiXmlPrinter printer;
	TiXmlElement* root=new TiXmlElement("EventoFinDelJuego");
	xml.LinkEndChild(root);
	xml.Accept(&printer);
	return printer.CStr();
}

string SerializadorEvento::serializar(EventoClientePedirImagen* evento){
	TiXmlDocument xml("evento");
	TiXmlPrinter printer;
	TiXmlElement* root=new TiXmlElement("EventoCliente");
	TiXmlElement* evento_s=new TiXmlElement("EventoClientePedirImagen");
	evento_s->SetAttribute("img",evento->getId());
	xml.LinkEndChild(root);
	root->LinkEndChild(evento_s);
	xml.Accept(&printer);
	return printer.CStr();
}
EventoClientePedirImagen* SerializadorEvento::deserializarPedirImagen(string evento){
	TiXmlDocument xml("evento");
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	root=root->FirstChildElement();
	return new EventoClientePedirImagen(atoi(root->Attribute("img")));
}

string SerializadorEvento::serializar(EventoListaPuntuacion* evento){
	int indice=0;
	string elem="jugador";
	TiXmlDocument xml("evento");
	TiXmlPrinter printer;
	TiXmlElement* root=new TiXmlElement("EventoServer");
	TiXmlElement *cliente=new TiXmlElement("EventoListaPuntuacion");
	xml.LinkEndChild(root);
	root->LinkEndChild(cliente);
	vector<Jugador> lista=evento->getJugadores();
	vector<Jugador>::iterator iter_lista; 
	for (iter_lista=lista.begin();iter_lista!=lista.end();++iter_lista){ 
		stringstream ss;
		ss <<indice; 
		TiXmlElement *elemento=new TiXmlElement(elem+ss.str()); 
		elemento->SetAttribute("nombre",(*iter_lista).getNombre());
		elemento->SetAttribute("puntuacion",(*iter_lista).getPuntuacion());
		cliente->LinkEndChild(elemento);
		indice++;
	} 
	xml.Accept(&printer);
	return printer.CStr();
}


EventoListaPuntuacion* SerializadorEvento::
				deserializarListaPuntuacion(string evento){
	TiXmlDocument xml("evento");
	EventoListaPuntuacion* eventol=new EventoListaPuntuacion();
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	if(root->ValueStr()!="EventoServer") return NULL;
	root=root->FirstChildElement();
	if(root->ValueStr()!="EventoListaPuntuacion") return NULL;
	for(TiXmlElement* element=root->FirstChildElement();element;
			element=element->NextSiblingElement() ){
		stringstream ss;
		stringstream ids;
		int puntuacion;
		ids<<element->Attribute("puntuacion");
		ids>>puntuacion;
		eventol->agregarJugador(element->Attribute("nombre"),puntuacion);
	}
	return eventol;	
	
}