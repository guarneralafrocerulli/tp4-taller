#include "eventList.h"
#include "Serializadores.h"

void EventList::addEvent(Evento* event){
	Lock lock(mutex);
	eventList.push(event);
}

std::string EventList::getSerializedEvents(){
	Lock lock(mutex);
	std::string result;
	SerializadorEvento serializer;
	/*while (! eventList.empty()){
		Evento* event = eventList.front();
		printf("Evento: %p\n", event);
		result += serializer.serializar(event);
		delete event;
		eventList.pop();
	}*/
	if(!this->eventList.empty()){
		Evento* event = eventList.front();
		result= serializer.serializar(event);
		delete event;
		this->eventList.pop();
	}
	return result;
}
