#include "common/LevelGrid.h"
#define PASTO 0



void LevelGrid::create(int ancho,int alto)
{ //this->editado=false;
 this->ancho=ancho;
 this->alto=alto;
 if (!this->grilla.empty()){
   this->grilla.erase(this->grilla.begin(),this->grilla.end());
 }
//inicializo todas las posiciones del grid en 0 =pasto
for(int i=0;i<alto;i++){
  vectorint_t fila;
  for(int j=0;j<ancho;j++){

    fila.push_back(0);
    
  }
  this->grilla.push_back(fila);
}

}
void LevelGrid::setXY(int x, int y, int type)
{
    this->grilla[y][x]=type;
}
int LevelGrid::getData(int x, int y)
{
  return this->grilla[x][y];
}

std::vector<std::vector< int > > LevelGrid::getGrilla()
{
return this->grilla;
}
int LevelGrid::getAlto()
{ int alto =this->grilla.size();
 return alto;
}

int LevelGrid::getAncho()
{int ancho=this->grilla[0].size();
return ancho;
}
int LevelGrid::getDificultad()
{
 return this->dificultad;
}
void LevelGrid::setDificultad(int dif)
{
 this->dificultad=dif;
}

void LevelGrid::setGrilla(std::vector< std::vector< int > > grilla)
{
  this->grilla=grilla;
}

bool LevelGrid::getEditado()
{
 return this->editado;
}
void LevelGrid::setEditado(bool ed)
{
 this->editado=ed;
}

LevelGrid::~LevelGrid()
{
}


