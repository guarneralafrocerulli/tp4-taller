#ifndef EVENTOS
#define EVENTOS
#include <string>
#include "partidasCampanias.h"
#include "tiposUsadosEnEventos.h"
#include "coordinate.h"
#include "common/jugador.h"
#include <vector>
using std::vector;
using std::string;
class SerializadorEvento;



class Evento{
protected:
	int id;
public:
	Evento(){}
	virtual ~Evento(){}
	Evento(int id){this->id=id;}
	virtual void setId(int id){this->id=id;}
	virtual int getId(){return id;}
	//virtual string serializar()=0;
	virtual string serializar(SerializadorEvento* serializador);
};

/***********************************************************************
 * 
 * 			Eventos que envia el cliente antes de comenzar
 * 			una partida
 * 
 * ********************************************************************/

class EventoCliente:public Evento{
public:
	EventoCliente(int id):Evento(id){}
};

class EventoClienteCrearPartida:public EventoCliente{
	/*el cliente envia el id de una campania
	 el nombre de la partida y la cantidad de jugadores*/
private:
	string nombre_jugador;
	string nombre_partida;
	int cant_jugadores;	
public:
	EventoClienteCrearPartida(int id,string nombrePartida,string nombreJugador,int cant);
	string getNombreJugador(){return nombre_jugador;}
	string getNombrePartida(){return nombre_partida;}
	int getCantJugadores(){return cant_jugadores;}
	string serializar(SerializadorEvento* serializador);
};

class EventoClientePedirImagen:public EventoCliente{
	/*evneto q envia el cliente para pedir
	 * la imagen de una campania*/
public:
	EventoClientePedirImagen(int img):EventoCliente(img){}
	string serializar(SerializadorEvento* ser);
};

class EventoClienteUnirsePartida:public EventoCliente{
	/*el cliente envia el id de la partida 
	 * a la que desea unirse y el nombre del jugador*/
private:
	string nombre_jugador;
public:
	EventoClienteUnirsePartida(int id,string nombreJugador);
	string getNombreJugador(){return nombre_jugador;}
	string serializar(SerializadorEvento* serializador);
};


class EventoClientePedirLista:public EventoCliente{
	/*evento q el cliente envia para pedir
	 * lista de partidas si se quiere unir o lista
	 * de campanias si desea crear*/
private:
	string tipo;
public:
	EventoClientePedirLista(string tipo);
	string getTipo(){return tipo;}
	string serializar(SerializadorEvento* serializador);
};

class EventoClienteEmpezarPartida:public EventoCliente{
	/*evento q envia el cliente para saber si
	 * puede empezar la partida*/
public:
	EventoClienteEmpezarPartida():EventoCliente(-1){}
	string serializar(SerializadorEvento* serializar);
};





/***********************************************************************
 * 
 * 			Eventos que envia el server antes de comenzar
 * 			una partida
 * 
 * ********************************************************************/

class EventoServer:public Evento{
public:
	EventoServer():Evento(-1){}
};

class EventoServerLista:public EventoServer{
	/*lista de partidas o campanias
	 * q envia el server al cliente*/
private:
	vector<PartidaCampania*> lista;
public:
	EventoServerLista():EventoServer(){};
	vector<PartidaCampania*> getLista(){return lista;}
	void agregar(PartidaCampania* elemento);
	void agregarCampania(int id,string nombre,int dificultad);
	void agregarPartida(int id,string nombre,int dificultad, int can);
	string serializar(SerializadorEvento* serializador);
	string serializar();
};


class EventoServerEmpezarPartida:public EventoServer{
	/* evento q envia el server despues de q el cliente
	 * envie EventoClienteEmpezarPartida. 
	 * empezar es true si el cliente puede empezar la partida
	 * (se lleno la partida si es para el cliente q la creo o
	 * se lleno la partida y el cliente q creo la partida 
	 * comenzo ya la comenzo, si es para los clientes q se unene a la partida)*/
private:
	bool empezar;
public:
	EventoServerEmpezarPartida(bool comienzo){empezar=comienzo;}
	string serializar(SerializadorEvento* serializador);
	bool getEmpezar(){return empezar;}
};

class EventoServerUnirPartida:public EventoServer{
	/* 
	 * Evento que envia el server despues de que el cliente
	 * envie EventoClienteUnirsePartida. 
	 * Devuelve true si se pudo unir, false si no pudo (partida llena)
	 */
private:
	bool unido;
public:
	EventoServerUnirPartida(bool unido): unido(unido){}
	string serializar(SerializadorEvento* serializador);
	bool getUnir(){return this->unido;}
};




class EventoServerImagen:public EventoServer{
private:
	ImagenCampania* img;
public:
	ImagenCampania* getImagen(){return img;}
	EventoServerImagen(ImagenCampania* img);
	string serializar(SerializadorEvento* ser);
};


/*************************************************************************
 *			Eventos q el server envia al cliente cuando	*
 *			el juego esta en marcha                          * 
 * 
 **************************************************************************/

class EventoPosicion:public Evento{
protected:
	int x;
	int y;
public:
	string serializar(SerializadorEvento*serializador);
	EventoPosicion();
	EventoPosicion(int id,int x,int y);
	void setXY(int x,int y);
	int getY(){return y;}
	int getX(){return x;}
};

class EventoNuevoMapa:public Evento{
private:
	vector<vector<int> > mapa;
public:
	EventoNuevoMapa(vector<vector<int> > mapa):Evento(-1){this->mapa=mapa;}
	vector<vector<int> > getMapa(){return mapa;}
	string serializar(SerializadorEvento* ser);
};


class EventoNuevoElemento:public Evento{
protected:
	string tipo;
	EventoPosicion evento;
public:
	string serializar(SerializadorEvento*serializador);
	EventoNuevoElemento(string tipo,int id, int x, int y);
	EventoNuevoElemento(string tipo,int id, Coordinate coord);
	int getX(){ return evento.getX();}
	int getY(){return evento.getY();}
	string getTipo(){return tipo;}
	EventoPosicion getEventoAux(){return evento;}
};

class EventoModificarVisibilidad:public Evento{
private:
	bool visibilidad;
public:
	string serializar(SerializadorEvento* serializador);
	EventoModificarVisibilidad(int id,bool vis);
	void setVisibilidad(bool vis){visibilidad=vis;}
	bool getVisibilidad(){return visibilidad;}
};


class EventoModificarCantidadVidas:public Evento{
private:
	int cant_vidas;
public:
	EventoModificarCantidadVidas(int id,int cant_vidas);
	void setCantVidas(int cant){this->cant_vidas=cant;}
	int getCantVidas(){return cant_vidas;}
	string serializar(SerializadorEvento*serializador);
};	

class EventoModificarPuntuacion:public Evento{
private:
	int puntuacion;
public:
	EventoModificarPuntuacion(int id,int puntuacion);
	void setPunt(int punt){puntuacion=punt;}
	int getPunt(){return puntuacion;}
	string serializar(SerializadorEvento* serializador);
};

class EventoAsignarIdJugador:public Evento{
public:
	EventoAsignarIdJugador(int id):Evento(id){};
	string serializar(SerializadorEvento* serializador);
};

class EventoListaPuntuacion:EventoServer{
	/*lista de jugadores con sus
	 * puntuaciones para enviar
	 * una ves q el juego termino*/
private:
	vector<Jugador> jugadores;
public:
	void agregarJugador(string nombre,int puntuacion);
	void agregarJugador(Jugador jugador);
	vector<Jugador> getJugadores(){return jugadores;}
	string serializar(SerializadorEvento* serializar);
};

class EventoFinDelJuego:public EventoServer{
	/*evento q envia el server 
	 * para indicar q termino el juego, se cabaron los niveles o 
	 * se desconecto alguien*/
public:
	string serializar(SerializadorEvento* serializador);
};



/************************************************************************
 * 			eventos que envia el jugador
 * 			una vez q comenzo el juego
 * 		     movimientos , colocacion de bombas
 * 			y desconeccion
 * 
 * ************************************************************************/



class EventoJugador:public Evento{
private:
	string tipo;
public:
	EventoJugador(string tipo,int id);
	~EventoJugador(){}
	string serializar(SerializadorEvento*serializador);
	void setTipo(string tipo){this->tipo=tipo;}
	string getTipo(){return tipo;}
};


class EventoDesconectar:public Evento{
	/*evento q el cliente evnia al server para
	 indicar q desea desconectarse*/
public:
	EventoDesconectar(int id):Evento(id){}
	string serializar(SerializadorEvento* serializador);
};



#endif
