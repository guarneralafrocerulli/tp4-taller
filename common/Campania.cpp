#include "common/Campania.h"

Campania::Campania(std::string nombrecampania,int dif) : docCampania(nombrecampania + ".xml"){
	
	this->dificultad=dif;
	this->nivelAeditar=-1;
	this->edicion=false;
	this->error=false;
	this->imagencampania=NULL;
	this->folder=nombrecampania +".xml";
}
Campania::Campania(std::string nombrearchivo) : docCampania(nombrearchivo){
	this->imagencampania = 0;
bool isload=this->docCampania.LoadFile();
   if (isload){
  this->error=this->cargarCampania();
  this->nivelAeditar=-1;
 }else{ this->error=true;}
 this->edicion=true;
 this->folder=nombrearchivo;
}
int Campania::getCantNiveles()
{
 return this->niveles.size();
}

Campania::Campania(const Campania& campania){
	this->dificultad = campania.dificultad;
	this->docCampania = campania.docCampania;
	this->edicion = campania.edicion;
	this->error = campania.error;
	this->imagencampania = new ImagenCampania(campania.imagencampania);
	this->nivelAeditar = campania.nivelAeditar;
	vector<LevelGrid*>::const_iterator it;
	for (it=campania.niveles.begin();it!=campania.niveles.end();++it){
		LevelGrid* level=new LevelGrid();
		level->create((*it)->getAncho(),(*it)->getAlto());
		level->setEditado((*it)->getEditado());
		level->setDificultad((*it)->getDificultad());
		level->setGrilla((*it)->getGrilla());
		this->niveles.push_back(level);
	}
	//this->niveles=campania.niveles;
	this->nombre = campania.nombre;
	//this->serializador = campania.serializador;
	this->folder=campania.folder;
}

std::string Campania::getNombre()
{
 return this->nombre;
}
int Campania::getDificultad()
{
return this->dificultad;
}

PartidaCampania Campania::getPartidaCampania(){
	PartidaCampania campania(-1, this->nombre, this->dificultad, -1);
	/*if(!this->getInfoImagen()) std::cerr<<"NO ANDA"<<std::endl;
	campania->setImagen(new ImagenCampania(this->getInfoImagen()));*/
	return campania;
}

bool Campania::cargarCampania(){

	TiXmlElement* root =this->docCampania.FirstChildElement();
	if(root==NULL) {
		return true;
	}
	TiXmlElement* name =root->FirstChildElement();
	if(name==NULL) {
		return true;
	}
	TiXmlElement* dificulty=name->NextSiblingElement();
	if(dificulty==NULL) {
		return true;
	}
	TiXmlElement* imagen=dificulty->NextSiblingElement();
	if(imagen==NULL){
	       return true;
	}
	TiXmlElement* niveles=imagen->NextSiblingElement();
	if(niveles==NULL) {
		return true;
	}
	for (TiXmlElement* nivel = niveles->FirstChildElement(); nivel != NULL; nivel= nivel->NextSiblingElement()) {
		LevelGrid* unnivel= new LevelGrid();
		unnivel->setGrilla(serializador.deserializarAElemento(nivel));
		unnivel->setDificultad(atoi(dificulty->Attribute("dificultad")));
		this->niveles.push_back(unnivel);
	}
	this->dificultad=atoi(dificulty->Attribute("dificultad"));
	this->nombre=name->Attribute("nombre");
	this->imagencampania=new ImagenCampania();
	this->imagencampania->setImagenBase64(imagen->Attribute("pixbufpixels"));
	this->imagencampania->setAlto(atoi(imagen->Attribute("alto")));
        this->imagencampania->setAncho(atoi(imagen->Attribute("ancho")));
	this->imagencampania->setAlpha(imagen->Attribute("hasalpha"));
	this->imagencampania->setBitspersample(atoi(imagen->Attribute("bitpersample")));
	this->imagencampania->setRowstride(atoi(imagen->Attribute("rowstride")));
	return false;
}

void Campania::guardar()
{ if(this->edicion){
  TiXmlDocument nuevoDoc(this->folder);
  this->docCampania = nuevoDoc;
  }
  TiXmlElement* root= new TiXmlElement("Campania");
  TiXmlElement* name=new TiXmlElement("Nombre");
  name->SetAttribute("nombre",this->nombre);
  TiXmlElement* dificulty=new TiXmlElement("Dificultad");
  //agregar imagen de la campania
  dificulty->SetAttribute("dificultad",this->dificultad);
  root->LinkEndChild(name);
  root->LinkEndChild(dificulty);
  TiXmlElement* imagen= new TiXmlElement("Imagen");
  /////toda la info necesaria ṕara rearmar un pixbuf///////////////////////////
  imagen->SetAttribute("pixbufpixels",this->imagencampania->getImagenBase64());
  imagen->SetAttribute("alto",this->imagencampania->getAlto());
  imagen->SetAttribute("ancho",this->imagencampania->getAncho());
  imagen->SetAttribute("hasalpha",this->imagencampania->gethasalpha());
  imagen->SetAttribute("bitpersample",this->imagencampania->getBitspersample());
  imagen->SetAttribute("rowstride",this->imagencampania->getRowstride());
  root->LinkEndChild(imagen);
  TiXmlElement* niv= new TiXmlElement("niveles");
  niv->SetAttribute("cantidad",this->niveles.size());
  
  for(int i=0;i<(this->niveles.size());i++)
{ std::stringstream ss;
  ss <<i;
  TiXmlElement* nivel =serializador.serializarAElemento((this->niveles[i]->getGrilla()),ss.str());
  niv->LinkEndChild(nivel);  
}
  root->LinkEndChild(niv);
  this->docCampania.LinkEndChild(root);
  this->docCampania.SaveFile(this->folder);    
}

int Campania::getAEditar()
{
return this->nivelAeditar;
}
LevelGrid* Campania::getNivel(int n)
{
 return this->niveles[n];
}
bool Campania::setNivelEditar(int nivel)
{
  if(!this->niveles.empty()){
  if ((nivel>=0)&&(nivel<=(this->niveles.size()))){
 this->nivelAeditar=nivel;
 return true;
}else{
   return false;}
  }else{return false;}
}
void Campania::unsetNivelEditar()
{
 this->nivelAeditar=-1;
}

void Campania::agregarNivel(LevelGrid* unnivel)
{   
    this->niveles.push_back(unnivel);   
}
bool Campania::eliminarNivel(int nivel)
{
    if(!this->niveles.empty()){
     this->niveles.erase(this->niveles.begin()+nivel-1);
     return true;
    }else{ return false;}
}
ImagenCampania* Campania::getInfoImagen()
{
	return this->imagencampania;
}

void Campania::setDificultad(int dif)
{
 this->dificultad=dif;
}
void Campania::setImagen(ImagenCampania* imagen)
{
this->imagencampania=new ImagenCampania(imagen);
}
void Campania::setNombre(string nombre)
{
  this->nombre=nombre;
}
void Campania::setFolder(string folder)
{
 this->folder=folder;
}

Campania::~Campania(){
	delete imagencampania;
	vector<LevelGrid*>::iterator it;
	for (it=niveles.begin();it!=niveles.end();++it){
		delete (*it);
	}

}

