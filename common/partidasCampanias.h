#ifndef PARTIDA_CAMPANIA_H
#define PARTIDA_CAMPANIA_H
#include <string>
#include "common/ImagenCampania.h"
using std::string;

class PartidaCampania{
private:
	int id;
	int dificultad;
	string nombre;
	int cant_jugadores;
public:
	PartidaCampania(int id,string nombre,int dificultad,int cant_jugadores);
	int getId(){return id;}
	void setId(int id){this->id=id;}
	string getNombre(){return nombre;}
	int getDificultad(){return dificultad;}
	int getCantJugadores(){return cant_jugadores;}
	PartidaCampania();
};

class PartidaCommon:public PartidaCampania{
public:
	PartidaCommon(int idp,string nombrep, int dificultadp,int cant_jugadoresp,ImagenCampania* imagen);
};

class CampaniaCommon:public PartidaCampania{
public:
	CampaniaCommon(int id,string nombre, int dificultad);
};
#endif