#ifndef SHARED_PTR_H
#define SHARED_PTR_H
#include <stdio.h>
#include "refCounter.h"
template <typename T> class SharedPtr{
public:
	SharedPtr(){
		this->pointer = 0;
		this->refCounter = new RefCounter();
	}
	SharedPtr(T* pointer){
		this->pointer = pointer;
		this->refCounter = new RefCounter();
		this->refCounter->addCounter();
	}
	~SharedPtr(){
		if (this->refCounter->removeCounter() <= 0){
			delete this->pointer;
			delete this->refCounter;
		}
	}
	T& operator*(){
		return *this->pointer;
	}
	T* operator->(){
		return this->pointer;
	}
	//Constructor copia
	SharedPtr<T>(const SharedPtr<T>& sp){
		this->pointer = sp.pointer;
		this->refCounter = sp.refCounter;
		this->refCounter->addCounter();
	}
	//Operador =
	SharedPtr<T>& operator=(const SharedPtr<T>& sp)	{
		if (this != &sp){ // Si no me copio a mi mismo
			if(this->refCounter->removeCounter() <= 0){
				//Si ya nadie referencia la copia
				delete this->pointer;
				delete this->refCounter;
			}
			this->pointer = sp.pointer;
			this->refCounter = sp.refCounter;
			refCounter->addCounter();
		}
		return *this;
	}

	bool isEmpty(){
		return (this->pointer == 0);
	}
private:
	T* pointer;
	RefCounter* refCounter;
};
#endif
