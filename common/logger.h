#ifndef LOGGER_H
#define LOGGER_H
#include <string>
#include "mutex.h"
class Logger{
public:
	static Logger* getInstance();
	~Logger();
	void log(std::string msg);
	static void destroy();
private:
	Mutex mutex;
	Logger();
	static Logger* instance;
};
#endif
