#ifndef CAMPANIA_H_
#define CAMPANIA_H_
#include "common/Serializadores.h"
#include "LevelGrid.h"
#include "partidasCampanias.h"
#include "ImagenCampania.h"
#include <string>
#include <vector>
#include <iostream>

class Campania {

public:
  //este constructor no setea nombre de campania 
  Campania(std::string nombrecampania,int dif);
  //este constructor carga desde archivo y setea todos los atributos.
  Campania(std::string nombrearchivo);
  //constructor de copia
  Campania(const Campania& campania);
  void agregarNivel(LevelGrid* unnivel);
  void guardar();
  int getCantNiveles();
  int getDificultad();
  bool eliminarNivel(int nivel);
  int getAEditar();
  bool getError(){return error;}
  void setImagen(ImagenCampania *imagen);
  ImagenCampania* getInfoImagen();
  LevelGrid* getNivel(int n);
  PartidaCampania getPartidaCampania();
  std::string getNombre();
  bool setNivelEditar(int nivel);
  void unsetNivelEditar();
  void setDificultad(int dif);
  void setNombre(std::string nombre);
  void setFolder(std::string folder);
  bool cargarCampania();
  ~Campania();
private:
 ///////////ATRIBUTOS//////////////////////////////////////////////////
  TiXmlDocument docCampania;
  std::string folder;
  typedef std::vector <LevelGrid*> niveles_t;
  niveles_t niveles;
  std::string nombre;
  /////info imagen de la campaña//////
  ImagenCampania *imagencampania;
  ///////////////////////////////////
  int dificultad;
  bool error;
  bool edicion;
  //para cuando quiero editar un nivel especifico.
  int nivelAeditar;
  SerializadorMapa serializador;
  ///////////////////////////////////////////////////////////////////// 
};
#endif 
  
