#ifndef SOCKET_H
#define SOCKET_H
#include <string>
#include <netinet/in.h>
#include "mutex.h"
/**
 * Wrapper del socket con la implementación de nuestro protocolo
 * de mensajes con longitud preconcatenada.
 */
class Socket{
public:
	Socket();
	Socket(int fd, struct sockaddr_in &address);
	/**
	 * Llamo a shutdownSocket y closeSocket
	 */
	~Socket();
	/*
	 * Envía un string con el largo preconcatenado
	 */
	void sendString(std::string str);
	/*
	 * Recibe un string, si se preconcatenó su largo
	 * (o sea, si fue enviado con send o similar)
	 */
	std::string recvString();
	/**
	 * Wrapper del equivalente en C
	 */
	void shutdownSocket(int how = 0);
	/**
	 * Wrapper del equivalente en C
	 */
	void closeSocket();
	/**
	 * id del file descriptor
	 */
	int getFD();
	bool isConnected();
	std::string getAddress();

protected:
	int fd;
	struct sockaddr_in address;
	bool connected;

private:
	Mutex mutex_send;
	Mutex mutex_recive;
	Socket(const Socket& otherSocket);
	const Socket& operator=(const Socket& otherSocket);
	// Acá utilizo el recv de la librería de C, en un loop
	void recvVoid(void* buffer, int length);
	// RecvLength utiliza recvVoid para recibir un único int
	int recvLength();
	// RecvString utiliza recvVoid para leer una cadena de chars de largo size
	std::string recvString(int size);
	// Acá utilizo el send de C, en un loop.
	void sendVoid(void* buffer, int length);
	// File descriptor del socket
};
#endif
