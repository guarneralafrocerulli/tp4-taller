#include "partidasCampanias.h"

PartidaCampania::PartidaCampania(int id, string nombre, int dificultad, int cant_jugadores){
	this->id=id;
	this->nombre=nombre;
	this->dificultad=dificultad;
	this->cant_jugadores=cant_jugadores;
	
}

PartidaCommon::PartidaCommon(int idp, string nombrep, int dificultadp, int cant_jugadoresp, ImagenCampania*):
	PartidaCampania(cant_jugadoresp, nombrep, cant_jugadoresp, cant_jugadoresp){}

CampaniaCommon::CampaniaCommon(int id, string nombre, int dificultad):
	PartidaCampania(id,nombre,dificultad,-1){}
