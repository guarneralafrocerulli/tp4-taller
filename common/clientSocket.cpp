#include "clientSocket.h"
#include <string>
#include <cstring> //memset
#include <arpa/inet.h>

ClientSocket::ClientSocket(std::string &address, int port){
	this->fd = socket(PF_INET, SOCK_STREAM, 0);
	if (this->fd == -1){
		throw "Socket exception";
	}
	//IPv4
	this->address.sin_family = AF_INET;
	this->address.sin_port = htons(port);
	this->address.sin_addr.s_addr = inet_addr(address.c_str());
	memset(this->address.sin_zero, 0, sizeof this->address.sin_zero);
}

void ClientSocket::assignIpPort(std::string& address, int port){
	this->fd = socket(PF_INET, SOCK_STREAM, 0);
	if (this->fd == -1){
		throw "Socket exception";
	}
	//IPv4
	this->address.sin_family = AF_INET;
	this->address.sin_port = htons(port);
	this->address.sin_addr.s_addr = inet_addr(address.c_str());
	memset(this->address.sin_zero, 0, sizeof this->address.sin_zero);
}

	
void ClientSocket::connectSocket(){
	int connected = connect(this->fd, (struct sockaddr *) &this->address,
			sizeof(struct sockaddr_in));
	if (connected == -1){
		throw "Falló al conectarse";
	} else {
		this->connected = true;
	}
}
