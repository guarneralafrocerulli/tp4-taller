#include "socket.h"
#include <sys/socket.h>
#include <unistd.h>
#include <cstdio>
#define INT_SIZE 4

#include <iostream>
#include <string>

#include <string.h>
#include <errno.h>
#include <arpa/inet.h>

Socket::Socket(){
	this->connected = false;
}

const Socket& Socket::operator=(const Socket &otherSocket){
	return otherSocket;
}

Socket::Socket(const Socket&){}

Socket::Socket(int fd, sockaddr_in &address){
	//std::string msg("Creando el socket ");
	//Logger::getInstance()->log(msg << fd);
	this->fd = fd;
	this->address = address;
	this->connected = true;
}


void Socket::sendString(std::string str){
	Lock locker(mutex_send);
	int size = str.size();;
	sendVoid((void*) &size, INT_SIZE);
	sendVoid((void*) str.c_str(), size);
}

std::string Socket::recvString(){
	Lock locker(mutex_recive);
	int size = recvLength();
	return recvString(size);
}


void Socket::shutdownSocket(int how){
	printf("Se apaga el socket %d\n", this->fd);
	shutdown(this->fd, how);
}

void Socket::closeSocket(){
	printf("Se cierra el socket %d\n", this->fd);
	close(this->fd);
	this->connected = false;
}

Socket::~Socket(){
	printf("Se destruye el socket %d\n", this->fd);
	shutdownSocket();
	closeSocket();
}

int Socket::getFD(){
	return this->fd;
}

bool Socket::isConnected(){
	return this->connected;
}

std::string Socket::getAddress(){
	return inet_ntoa(this->address.sin_addr);
}

/**
 * Privadas
 */

int Socket::recvLength(){
	int length = 0;
	recvVoid((void*) &length, INT_SIZE);
	return length;
}

std::string Socket::recvString(int size){
	std::string result;
	char* buffer = new char[size];
	try{
		recvVoid((void*) buffer, size);
		result.assign(buffer, size);
	} catch (const char* e){
		delete buffer;
		throw e;
	}
	delete[] buffer;
	return result;
}

void Socket::recvVoid(void* buffer, int length){
	int totalReceived = 0;
	int received;
	do {
		received = recv(this->fd, buffer, length - totalReceived, 0);
		totalReceived += received;
	} while (totalReceived < length && received > 0);
	if (received <= 0){
		printf("recv: %d, Error capturado: %s\n", received, strerror(errno));
		printf("Tirando una excepción: recvVoid(buffer,%d) -- Recibidos: %d\n", length, totalReceived);
		throw "Excepción en recvVoid\n";
	}
}

void Socket::sendVoid(void* buffer, int length){
	int totalSent = 0;
	int sent;
	do {
		//MSG_NOSIGNAL para que no explote cuando hay un error.
		sent = send(this->fd, buffer, length - totalSent, MSG_NOSIGNAL);
		totalSent += sent;
	} while (totalSent < length && sent > 0);
	if (sent <= 0){
		printf("Sent: %d, Error capturado: %s\n", sent, strerror(errno));
		printf("Tirando una excepción: sendVoid(buffer,%d)\n", length);
		throw "Excepción en sendVoid\n";
	}
}
