#include "refCounter.h"

RefCounter::RefCounter(){
	this->count = 0;
}

void RefCounter::addCounter(){
	++this->count;
}

int RefCounter::removeCounter(){
	return --this->count;
}
