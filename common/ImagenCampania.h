#ifndef IMAGENCAMPANIA_H_
#define IMAGENCAMPANIA_H_
#include <iostream>

class ImagenCampania{
private:
////////atributos////////
  int ancho;
  int alto;
  int rowstride;
  bool hasalpha;
  int bitspersample;
  unsigned char* pixels;
  std::string imagenBase64;
 public:
 //////metodos//////////
      ImagenCampania();
      ImagenCampania(ImagenCampania* imagen);
  int getAncho();
  int getAlto();
  int getRowstride();
  bool gethasalpha();
  int getBitspersample();
  unsigned char* getPixels();
  std::string getImagenBase64();
  void setAncho(int ancho);
  void setAlto(int alto);
  void setRowstride(int row);
  void setBitspersample(int bits);
  void setAlpha(bool alpha);
  void setAlpha(std::string salpha);
  void setPixels(unsigned char* pixels);
  void setImagenBase64(std::string imagen64);
};
#endif