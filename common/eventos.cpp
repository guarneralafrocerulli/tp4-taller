#include "eventos.h"
#include "common/Serializadores.h"

string Evento::serializar(SerializadorEvento* serial){
	return serial->serializar(this);
}

EventoPosicion::EventoPosicion(){
	x=0;
	y=0;
	id=0;
}
EventoModificarCantidadVidas::EventoModificarCantidadVidas(int id,int cant):Evento(id){
	this->cant_vidas=cant;
}

string EventoModificarCantidadVidas::serializar(SerializadorEvento* serializador){
	return serializador->serializar(this);
}
	
EventoPosicion::EventoPosicion(int id, int x, int y):Evento(id){
	this->x=x;
	this->y=y;
}

void EventoPosicion::setXY(int x, int y){
	this->x = x;
	this->y = y;
}

string EventoPosicion::serializar(SerializadorEvento* serializador){
	return serializador->serializar(this);
}
string EventoNuevoElemento::serializar(SerializadorEvento* serializador){
	return serializador->serializar(this);
}
string EventoJugador::serializar(SerializadorEvento* serializador){
	return serializador->serializar(this);
}

EventoJugador::EventoJugador(string tipo,int id):Evento(id){
	this->tipo=tipo;
}

EventoNuevoElemento::EventoNuevoElemento(string tipo,int id, int x, int y):Evento(id){
	evento.setXY(x,y);
	evento.setId(id);
	this->tipo=tipo;
}

EventoNuevoElemento::EventoNuevoElemento(string tipo, int id, Coordinate coord): Evento(id){
	this->evento.setXY(coord.x, coord.y);
	evento.setId(id);
	this->tipo=tipo;
}


EventoModificarVisibilidad::EventoModificarVisibilidad(int id,bool vis):Evento(id){
	this->visibilidad=vis;
}

string EventoModificarVisibilidad::serializar(SerializadorEvento* serial){
	return serial->serializar(this);
}

EventoModificarPuntuacion::EventoModificarPuntuacion(int id,int punt):Evento(id){
	puntuacion=punt;
}
string EventoModificarPuntuacion::serializar(SerializadorEvento* serializador){
	return serializador->serializar(this);
}


string EventoAsignarIdJugador::serializar(SerializadorEvento* serializador){
	return serializador->serializar(this);
}

string EventoNuevoMapa::serializar(SerializadorEvento* serializador){
	return serializador->serializar(this);
}

/***********************************************************************
 * 
 * 			Eventos que envia el cliente antes de comenzar
 * 			una partida
 * 
 * ********************************************************************/
EventoClienteUnirsePartida::EventoClienteUnirsePartida(int id,string nombreJugador):EventoCliente(id){
	nombre_jugador=nombreJugador;
}

EventoClienteCrearPartida::EventoClienteCrearPartida(int id,string nombrePartida,string nombreJugador,int cant):EventoCliente(id){
	this->nombre_partida=nombrePartida;
	this->nombre_jugador=nombreJugador;
	this->cant_jugadores=cant;
}

string EventoClienteCrearPartida::serializar(SerializadorEvento* serializador){
	return serializador->serializar(this);
}

EventoClientePedirLista::EventoClientePedirLista(string tipo):EventoCliente(-1){
	this->tipo=tipo;
}

string EventoClientePedirLista::serializar(SerializadorEvento* serializador){
	return serializador->serializar(this);
}

string EventoClienteUnirsePartida::serializar(SerializadorEvento* serializador){
	return serializador->serializar(this);
}

void EventoServerLista::agregar(PartidaCampania* elemento){
	lista.push_back(elemento);
}
void EventoServerLista::agregarPartida(int id, string nombre,int dificultad,int cant){
	PartidaCampania* partida=new PartidaCampania(id,nombre,dificultad,cant);
	lista.push_back(partida);
}

void EventoServerLista::agregarCampania(int id,string nombre, int dificultad){
	agregarPartida(id,nombre,dificultad,-1);
}

string EventoServerLista::serializar(SerializadorEvento* serializador){
	return serializador->serializar(this);
}

string EventoServerEmpezarPartida::serializar(SerializadorEvento* serializador){
	return serializador->serializar(this);
}

string EventoServerUnirPartida::serializar(SerializadorEvento* serializador){
	return serializador->serializar(this);
}

string EventoClienteEmpezarPartida::serializar(SerializadorEvento* serializador){
	return serializador->serializar(this);
}

string EventoDesconectar:: serializar(SerializadorEvento* serializador){
	return serializador->serializar(this);
}

string EventoFinDelJuego::serializar(SerializadorEvento* serializador){
	return serializador->serializar(this);
}



void EventoListaPuntuacion::agregarJugador(string nombre,int puntuacion){
	Jugador jug(nombre,puntuacion);
	vector<Jugador>::iterator iterador;
	for (iterador=jugadores.begin();iterador!=jugadores.end();++iterador){
		if(puntuacion>(*iterador).getPuntuacion()){
			break;
		}
	}
	jugadores.insert(iterador,jug);
}

void EventoListaPuntuacion::agregarJugador(Jugador jugador){
	jugadores.push_back(jugador);
}

string EventoListaPuntuacion::serializar(SerializadorEvento* serializador){
	return serializador->serializar(this);
}


EventoServerImagen::EventoServerImagen(ImagenCampania* img):EventoServer(){
	this->img=img;
}

string EventoServerImagen::serializar(SerializadorEvento* ser){
	return ser->serializar(this);
}


string EventoClientePedirImagen::serializar(SerializadorEvento* ser){
	return ser->serializar(this);
}