#ifndef SERIALIZADORES
#define SERIALIZADORES
#include <string>
#include <vector>
#include "eventos.h"
#include "tinyxml.h"
#include <map>
using std::vector;
using std::string;
using std::map;


class SerializadorMapa{
public:
	TiXmlElement* serializarAElemento(vector< vector< int > > mapa,string id);
	vector<vector<int> > deserializarAElemento(TiXmlElement* elem);
        string serializar(vector< vector<int> > mapa);
	vector<string> convertirVectorAString(vector<vector<int> > mapa);
	vector<vector<int> > convertirStringAVector(vector<string> mapa);
	vector< vector<int> > deserializar(string mapa);
	 char codificar(int cod);
	int decodificar(char cod);
};

class SerializadorEvento{
public:
	EventoServerImagen* deserializarImagen(string evento);
	EventoDesconectar* deserializarDesconectar(string evento);
	EventoListaPuntuacion* deserializarListaPuntuacion(string evento);
	/////////////////Eventos cliente///////////////
	EventoClienteCrearPartida* deserializarClienteCrearPartida(string evento);
	EventoClientePedirLista* deserializarPedirLista(string evento);
	EventoClienteUnirsePartida* deserializarUnirsePartida(string evento);
	EventoClienteEmpezarPartida* deserializarClienteEmpezarPartida(string evento);
	////////////Eventos Server//////////////////
	EventoServerLista* deserializarListas(string evento);
	EventoServerUnirPartida* deserializarServerUnirPartida(string evento);
	EventoServerEmpezarPartida* deserializarServerEmpezarPartida(string evento);
	///////////////////////////////////////////////
	EventoAsignarIdJugador* deserializarAsignarJugador(string evento);
	EventoClientePedirImagen* deserializarPedirImagen(string evento);
	EventoNuevoMapa* deserializarNuevoMapa(string evento);
	EventoModificarPuntuacion* deserializarPuntuacion(string evento);
	EventoModificarCantidadVidas* deserializarCantVidas(string evento);
	EventoNuevoElemento* deserializarNuevoElemento(string evento);
	EventoPosicion * deserializarPosicion(string evento);
	EventoJugador* deserializarEventoJugador(string evento);
	EventoModificarVisibilidad* deserializarModificarVisibilidad(string evento);
	string serializar(EventoListaPuntuacion* evento);
	string serializar(EventoFinDelJuego* evento);
	string serializar(EventoDesconectar* evento);
	string serializar(Evento* evento);
	string serializar(EventoCliente* evento);
	string serializar(EventoClienteCrearPartida* evento);
	string serializar(EventoClienteEmpezarPartida* evento);
	string serializar(EventoClientePedirLista* evento);
	string serializar(EventoClienteUnirsePartida* evento);
	string serializar(EventoModificarCantidadVidas* evento);
	string serializar(EventoPosicion* evento);
	string serializar(EventoNuevoElemento* evento);
	string serializar(EventoJugador* evento);
	string serializar(EventoModificarVisibilidad* evento);
	string serializar(EventoModificarPuntuacion* evento);
	string serializar(EventoAsignarIdJugador* evento);
	string serializar(EventoServerEmpezarPartida* evento);
	string serializar(EventoServerLista* evento);
	string serializar(EventoServerUnirPartida* evento);
	string serializar(EventoNuevoMapa* evento);
	string serializar(EventoServerImagen* evento);
	string serializar(EventoClientePedirImagen* evento);
	Evento* deserializar(string evento);
};


	
#endif
