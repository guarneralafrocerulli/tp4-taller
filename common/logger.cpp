#include "logger.h"
#include <iostream>
#include <string>

Logger* Logger::instance = 0;

Logger::Logger(){}

Logger::~Logger(){}

Logger* Logger::getInstance(){
	if (!instance){
		instance = new Logger();
	}
	return instance;
}

void Logger::log(std::string msg){
	Lock lock(mutex);
	std::cout << "Log: " << msg << std::endl;
}

void Logger::destroy(){
	delete instance;
	instance = 0;
}
