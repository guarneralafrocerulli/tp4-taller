#ifndef JUGADOR_H
#define JUGADOR_H
#include <string>
using std::string;

class Jugador{
private:
	string nombre;
	int puntuacion;
public:
	Jugador(string nombre,int puntuacion);
	string getNombre(){return nombre;}
	int getPuntuacion(){return puntuacion;}
};	

#endif