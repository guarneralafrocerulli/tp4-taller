#ifndef REF_COUNTER_H
#define REF_COUNTER_H
/**
 * Contador de referencias para el shared pointer
 */

class RefCounter{
	public:
	RefCounter();
	void addCounter();
	int removeCounter();
	private:
	int count;
};
#endif
