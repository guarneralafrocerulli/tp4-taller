#Clases comunes a los 3 proyectos (o que podrían ser comunes)
add_library(common
	partidasCampanias.cpp
	Campania.cpp
	LevelGrid.cpp
	jugador.cpp
	clientSocket.cpp
	socket.cpp
	thread.cpp
	logger.cpp
	mutex.cpp
	Serializadores.cpp
	eventos.cpp
	eventList.cpp
        ImagenCampania.cpp)

target_link_libraries(common pthread tinyxml)
