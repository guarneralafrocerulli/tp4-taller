#ifndef EVENT_LIST
#define EVENT_LIST
#include <queue>
#include <string>
#include "mutex.h"

class Evento;

class EventList{
public:
	/**
	 * Agrega un evento a la lista
	 */
	void addEvent(Evento* event);
	/**
	 * @Post: la lista queda vacía
	 */
	std::string getSerializedEvents();
	std::queue<Evento*> eventList;
	Mutex mutex;
private:
	
	
};
#endif