#ifndef FACTORYDIBUJABLEREPRODUCIBLE
#define FACTORYDIBUJABLEREPRODUCIBLE
#include <client/clases_sdl/DibujableReproducible.h>

#define TIERRA 0
#define BOMBERMAN_NOR 1
#define BOMBERMAN_SUR 2
#define BOMBERMAN_IZQ 3
#define BOMBERMAN_DER 4
#define CAJA_METAL 5
#define ZOMBIE_VERDE_NOR 6
#define ZOMBIE_VERDE_SUR 7
#define ZOMBIE_VERDE_IZQ 8
#define ZOMBIE_VERDE_DER 9
#define ZOMBIE_ROJO_NOR 50
#define ZOMBIE_ROJO_SUR 51
#define ZOMBIE_ROJO_IZQ 53
#define ZOMBIE_ROJO_DER 54
#define ZOMBIE_AZUL_NOR 55
#define ZOMBIE_AZUL_SUR 56
#define ZOMBIE_AZUL_IZQ 57
#define ZOMBIE_AZUL_DER 58
#define VIDA 59
#define BOMBA_PLANTA 60
#define BOMBA_BOMBERMAN 61
#define CAJA_MADERA 62
#define PLANTA_LENTA_NORTE 11
#define PLANTA_LENTA_SUR 21
#define PLANTA_LENTA_IZQUIERDA 31
#define PLANTA_LENTA_DERECHA 41
#define PLANTA_REGULAR_NORTE 12
#define PLANTA_REGULAR_SUR 22
#define PLANTA_REGULAR_IZQUIERDA 32
#define PLANTA_REGULAR_DERECHA 42
#define PLANTA_RAPIDA_NORTE 13
#define PLANTA_RAPIDA_SUR 23
#define PLANTA_RAPIDA_IZQUIERDA 33
#define PLANTA_RAPIDA_DERECHA 43
#define EXPLOSION 64

class Accion;
class AccionNuevoBomberman;
class AccionNuevoBombaBomberman;
class AccionNuevoZombieAzul;
class AccionNuevoZombieRojo;
class AccionNuevoZombieVerde;
class AccionNuevoBombaPlanta;
class AccionNuevoExplosion;
class AccionNuevoCaja;

class FactoryDibRep{
	/*Factory de dibujables reproducibles */
private:
	map<int,FrameSprite*> frames;
	map<string,Reproductor*> sonidos;
public:
	FactoryDibRep();
	~FactoryDibRep();
	void cargarFrames();
	void cargarSonidos();
	void cargarFrame(string archivo,int clave); 
	DibujableReproducible * crear(AccionNuevoExplosion* accion); 
	DibujableReproducible * crear(AccionNuevoBomberman* accion);
	DibujableReproducible * crear(AccionNuevoBombaBomberman* accion);
	DibujableReproducible * crear(AccionNuevoZombieAzul* accion);
	DibujableReproducible * crear(AccionNuevoZombieRojo* accion);
	DibujableReproducible * crear(AccionNuevoZombieVerde* accion);
	DibujableReproducible * crear(AccionNuevoBombaPlanta* accion);
	DibujableReproducible * crear(AccionNuevoCaja* accion);
	DibujablesFijos * crearDibujablesFijos();
	DatosJugador* crearDatosJugador();
};



#endif