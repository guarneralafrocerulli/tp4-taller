#ifndef FACTORY_ACCIONES_H
#define FACTORY_ACCIONES_H
#include "common/eventos.h"
#include "common/tiposEventoNuevoElemento.h"
#include <client/controladores/Accion.h>

class FactoryAcciones{
	/*se encarga de traducir los eventos q envia el servidor
	 * en acciones comprensibles por el controlador imagen audio*/
private:
	static Accion* obtenerAccionNuevoElemento(EventoNuevoElemento* evento);
	static AccionModificarPuntuacion* crearAccionModificarPuntuacion(EventoModificarPuntuacion* evento);
	static AccionCambioPosicion* crearAccionCambioPosicion(EventoPosicion* evento);
	static AccionModificarVisibilidad* crearAccionModificarVisibilidad(EventoModificarVisibilidad* evento);
	static AccionModificarCantVidas* crearAccionModificarCantVidas(EventoModificarCantidadVidas* evento);
	static AccionAsignarJugador* crearAccionAsignarIdJugador(EventoAsignarIdJugador* evento);
public:
	static Accion* crear(string evento);
};

#endif