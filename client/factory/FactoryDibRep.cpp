#include "FactoryDibRep.h"
#include <client/controladores/Accion.h>
#include <iostream>
#include <vector>
using std::vector;

 FactoryDibRep::FactoryDibRep(){
	/*this->cargarFrames();
	this->cargarSonidos();*/
}
FactoryDibRep::~FactoryDibRep(){
	map<int,FrameSprite*>::const_iterator iterF;
	map<string,Reproductor*>::const_iterator iterS;
	for (iterF=frames.begin();iterF!=frames.end();++iterF){
			delete (*iterF).second;
	}
	for (iterS=sonidos.begin();iterS!=sonidos.end();++iterS){
		delete (*iterS).second;
	}
}
void FactoryDibRep::cargarSonidos(){//falta caja,y capas planta
/*cargo los sonidos del juego*/ 
		ReproductorWav *wav=NULL;
		///////////muerte bomnberman////////
		wav=new ReproductorWav();
		wav->cargarSonido("sounds/bomberman_dead.wav");
		sonidos["bomberman_dead"]=wav;
		//////explosion bomba/////////
		wav=new ReproductorWav();
		wav->cargarSonido("sounds/explosion.wav");
		sonidos["explosion"]=wav;
		////////zombie muerto////////
		wav=new ReproductorWav();
		wav->cargarSonido("sounds/zombie_dead.wav");
		sonidos["zombie_dead"]=wav;
		///// zombie spawn////////////////////
		wav=new ReproductorWav();
		wav->cargarSonido("sounds/zombie_spawn.wav");
		sonidos["zombie_spawn"]=wav;
		////bomba plantada por bomberman//////////
		wav=new ReproductorWav();
		wav->cargarSonido("sounds/bomb_bomberman.wav");
		sonidos["bomb_bomberman"]=wav;
		////bomba disparada porplanta//////////
		wav=new ReproductorWav();
		wav->cargarSonido("sounds/bomb_plant.wav");
		sonidos["bomb_plant"]=wav;
}

	
void FactoryDibRep::cargarFrame(string archivo,int clave){
	FrameSprite* frame=NULL;
	frame=new FrameSprite();
	frame->cargar("sprites/"+archivo);
	frames[clave]=frame;
}

void FactoryDibRep::cargarFrames(){
		cargarFrame("bombermanhead.png",VIDA);
		cargarFrame("bomberman_sur.png",BOMBERMAN_SUR);
		cargarFrame("bomberman_norte.png",BOMBERMAN_NOR);
		cargarFrame("bomberman_izquierda.png",BOMBERMAN_IZQ);
		cargarFrame("bomberman_derecha.png",BOMBERMAN_DER);
		cargarFrame("bombab.png",BOMBA_BOMBERMAN);
		cargarFrame("bombap.png",BOMBA_PLANTA);
		cargarFrame("zombie_verde_sur.png",ZOMBIE_VERDE_SUR);
		cargarFrame("zombie_verde_norte.png",ZOMBIE_VERDE_NOR);
		cargarFrame("zombie_verde_derecha.png",ZOMBIE_VERDE_DER);
		cargarFrame("zombie_verde_izquierda.png",ZOMBIE_VERDE_IZQ);
		cargarFrame("zombie_rojo_sur.png",ZOMBIE_ROJO_SUR);
		cargarFrame("zombie_rojo_norte.png",ZOMBIE_ROJO_NOR);
		cargarFrame("zombie_rojo_derecha.png",ZOMBIE_ROJO_DER);
		cargarFrame("zombie_rojo_izquierda.png",ZOMBIE_ROJO_IZQ);
		cargarFrame("zombie_azul_sur.png",ZOMBIE_AZUL_SUR);
		cargarFrame("zombie_azul_norte.png",ZOMBIE_AZUL_NOR);
		cargarFrame("zombie_azul_derecha.png",ZOMBIE_AZUL_DER);
		cargarFrame("zombie_azul_izquierda.png",ZOMBIE_AZUL_IZQ);
		cargarFrame("grass.png",TIERRA);
		cargarFrame("metal.png",CAJA_METAL);
		cargarFrame("planta_rapida_izquierda.png",PLANTA_RAPIDA_IZQUIERDA);
		cargarFrame("planta_rapida_derecha.png",PLANTA_RAPIDA_DERECHA);
		cargarFrame("planta_rapida_sur.png",PLANTA_RAPIDA_SUR);
		cargarFrame("planta_rapida_norte.png",PLANTA_RAPIDA_NORTE);			cargarFrame("planta_regular_izquierda.png",PLANTA_REGULAR_IZQUIERDA);
		cargarFrame("planta_regular_derecha.png",PLANTA_REGULAR_DERECHA);
		cargarFrame("planta_regular_sur.png",PLANTA_REGULAR_SUR);
		cargarFrame("planta_regular_izquierda.png",PLANTA_REGULAR_IZQUIERDA);
		cargarFrame("planta_regular_norte.png",PLANTA_REGULAR_NORTE);
		cargarFrame("planta_lenta_izquierda.png",PLANTA_LENTA_IZQUIERDA);
		cargarFrame("planta_lenta_derecha.png",PLANTA_LENTA_DERECHA);
		cargarFrame("planta_lenta_sur.png",PLANTA_LENTA_SUR);
		cargarFrame("planta_lenta_norte.png",PLANTA_LENTA_NORTE);
		cargarFrame("box.png",CAJA_MADERA);
		cargarFrame("fire.png",EXPLOSION);
	}

DibujableReproducible* FactoryDibRep::crear(AccionNuevoBomberman* accion){
	DibujableReproducible *nuevo=new DibujableReproducible();
	nuevo->agregarFrame(frames[BOMBERMAN_NOR]);
	nuevo->agregarFrame(frames[BOMBERMAN_SUR]);
	nuevo->agregarFrame(frames[BOMBERMAN_DER]);
	nuevo->agregarFrame(frames[BOMBERMAN_IZQ]);
	nuevo->agregarSonidoDesaparecer(sonidos["bomberman_dead"]);
	nuevo->setPos(accion->getX(),accion->getY());
	nuevo->setVisibilidad(true);
	nuevo->setNoBorrable();
	return nuevo;
}

DibujableReproducible* FactoryDibRep::crear(AccionNuevoExplosion* accion){
	DibujableReproducible *nuevo=new DibujableReproducible();
	nuevo->agregarFrame(frames[EXPLOSION]);
	nuevo->setPos(accion->getX(),accion->getY());
	nuevo->setVisibilidad(true);
	return nuevo;
}


DibujableReproducible* FactoryDibRep::crear(AccionNuevoBombaBomberman* accion){
	DibujableReproducible *nuevo=new DibujableReproducible();
	nuevo->agregarFrame(frames[BOMBA_BOMBERMAN]);
	nuevo->agregarSonidoAparecer(sonidos["bomb_bomberman"]);
	nuevo->agregarSonidoDesaparecer(sonidos["explosion"]);
	nuevo->setVisibilidad(true);
	nuevo->setPos(accion->getX(),accion->getY());
	return nuevo;
}

DibujableReproducible* FactoryDibRep::crear(AccionNuevoZombieAzul* accion){
	DibujableReproducible *nuevo=new DibujableReproducible();
	nuevo->agregarFrame(frames[ZOMBIE_AZUL_NOR]);
	nuevo->agregarFrame(frames[ZOMBIE_AZUL_SUR]);
	nuevo->agregarFrame(frames[ZOMBIE_AZUL_DER]);
	nuevo->agregarFrame(frames[ZOMBIE_AZUL_IZQ]);
	nuevo->agregarSonidoDesaparecer(sonidos["zombie_dead"]);
	nuevo->agregarSonidoAparecer(sonidos["zombie_spawn"]);
	nuevo->setPos(accion->getX(),accion->getY());
	nuevo->setVisibilidad(true);
	return nuevo;
}
DibujableReproducible* FactoryDibRep::crear(AccionNuevoZombieRojo* accion){
	DibujableReproducible *nuevo=new DibujableReproducible();
	nuevo->agregarFrame(frames[ZOMBIE_ROJO_NOR]);
	nuevo->agregarFrame(frames[ZOMBIE_ROJO_SUR]);
	nuevo->agregarFrame(frames[ZOMBIE_ROJO_DER]);
	nuevo->agregarFrame(frames[ZOMBIE_ROJO_IZQ]);
	nuevo->agregarSonidoDesaparecer(sonidos["zombie_dead"]);
	nuevo->agregarSonidoAparecer(sonidos["zombie_spawn"]);
	nuevo->setPos(accion->getX(),accion->getY());
	nuevo->setVisibilidad(true);
	return nuevo;
}

DibujableReproducible* FactoryDibRep::crear(AccionNuevoZombieVerde* accion){
	DibujableReproducible *nuevo=new DibujableReproducible();
	nuevo->agregarFrame(frames[ZOMBIE_VERDE_NOR]);
	nuevo->agregarFrame(frames[ZOMBIE_VERDE_SUR]);
	nuevo->agregarFrame(frames[ZOMBIE_VERDE_DER]);
	nuevo->agregarFrame(frames[ZOMBIE_VERDE_IZQ]);
	nuevo->agregarSonidoDesaparecer(sonidos["zombie_dead"]);
	nuevo->agregarSonidoAparecer(sonidos["zombie_spawn"]);
	nuevo->setPos(accion->getX(),accion->getY());
	nuevo->setVisibilidad(true);
	return nuevo;
}

DibujableReproducible* FactoryDibRep::crear(AccionNuevoBombaPlanta* accion){
	DibujableReproducible *nuevo=new DibujableReproducible();
	nuevo->agregarFrame(frames[BOMBA_PLANTA]);
	nuevo->agregarSonidoAparecer(sonidos["bomb_plant"]);
	nuevo->agregarSonidoDesaparecer(sonidos["explosion"]);
	nuevo->setVisibilidad(true);
	nuevo->setPos(accion->getX(),accion->getY());
	return nuevo;
}

DibujableReproducible* FactoryDibRep::crear(AccionNuevoCaja* accion){
	DibujableReproducible *nuevo=new DibujableReproducible();
	nuevo->agregarFrame(frames[CAJA_MADERA]);
	//nuevo->agregarSonidoDesaparecer(sonidos["explosion"]);
	nuevo->setVisibilidad(true);
	nuevo->setPos(accion->getX(),accion->getY());
	return nuevo;
}


DibujablesFijos *  FactoryDibRep::crearDibujablesFijos(){
	DibujablesFijos* terreno=new DibujablesFijos();
	terreno->agregarFrame(TIERRA,frames[TIERRA]);
	terreno->agregarFrame(CAJA_METAL,frames[CAJA_METAL]);
	terreno->agregarFrame(PLANTA_LENTA_NORTE,frames[PLANTA_LENTA_NORTE]);
	terreno->agregarFrame(PLANTA_LENTA_SUR,frames[PLANTA_LENTA_SUR]);
	terreno->agregarFrame(PLANTA_LENTA_IZQUIERDA,frames[PLANTA_LENTA_IZQUIERDA]);
	terreno->agregarFrame(PLANTA_LENTA_DERECHA,frames[PLANTA_LENTA_DERECHA]);
	terreno->agregarFrame(PLANTA_REGULAR_NORTE,frames[PLANTA_REGULAR_NORTE]);
	terreno->agregarFrame(PLANTA_REGULAR_SUR,frames[PLANTA_REGULAR_SUR]);
	terreno->agregarFrame(PLANTA_REGULAR_IZQUIERDA,frames[PLANTA_REGULAR_IZQUIERDA]);
	terreno->agregarFrame(PLANTA_REGULAR_DERECHA,frames[PLANTA_REGULAR_DERECHA]);
	terreno->agregarFrame(PLANTA_RAPIDA_NORTE,frames[PLANTA_RAPIDA_NORTE]);
	terreno->agregarFrame(PLANTA_RAPIDA_SUR,frames[PLANTA_RAPIDA_SUR]);
	terreno->agregarFrame(PLANTA_RAPIDA_IZQUIERDA,frames[PLANTA_RAPIDA_IZQUIERDA]);
	terreno->agregarFrame(PLANTA_RAPIDA_DERECHA,frames[PLANTA_RAPIDA_DERECHA]);
	return terreno;
}
DatosJugador* FactoryDibRep::crearDatosJugador(){
	DatosJugador* datos=new DatosJugador();
	datos->agregarFrame(frames[VIDA]);
	return datos;
}