#include "factoryAcciones.h"
#include "common/Serializadores.h"
#include <tinyxml.h>
#include <client/controladores/Accion.h>
#include "common/tiposEventoNuevoElemento.h"

Accion* FactoryAcciones::crear(string evento){
	//std::cerr<<"creo accion"<<std::endl;
	/*deserializa el evento y crea la accion q corresponda*/
	SerializadorEvento ser;
	TiXmlDocument xml("evento");
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	if(!root)return NULL;
	Accion* accion=NULL;
	if(root->ValueStr()=="EventoPosicion"){
		EventoPosicion*evento_pos=ser.deserializarPosicion(evento);
		accion=crearAccionCambioPosicion(evento_pos);
		delete evento_pos;
	}
	if(root->ValueStr()=="EventoNuevoElemento"){
		EventoNuevoElemento* nuevo_elem=ser.deserializarNuevoElemento(evento);
		accion=obtenerAccionNuevoElemento(nuevo_elem);
		delete nuevo_elem;
	}
	if(root->ValueStr()=="EventoModificarVisibilidad"){
		EventoModificarVisibilidad* modificar_vis=ser.deserializarModificarVisibilidad(evento);
		accion=crearAccionModificarVisibilidad(modificar_vis);
		delete modificar_vis;
		
	}
	if(root->ValueStr()=="EventoModificarCantidadVidas"){
		EventoModificarCantidadVidas* modificar_vid=ser.deserializarCantVidas(evento);
		accion=crearAccionModificarCantVidas(modificar_vid);
		delete modificar_vid;
	}
	if(root->ValueStr()=="EventoModificarPuntuacion"){
		EventoModificarPuntuacion* modificar_punt=ser.deserializarPuntuacion(evento);
		accion=crearAccionModificarPuntuacion(modificar_punt);
		delete modificar_punt;
	}
	if(root->ValueStr()=="EventoAsignarIdJugador"){
		EventoAsignarIdJugador* asignar_jugador=ser.deserializarAsignarJugador(evento);
		accion=crearAccionAsignarIdJugador(asignar_jugador);
		delete asignar_jugador;
	}
	if(root->ValueStr()=="Mapa"){
		EventoNuevoMapa* map=ser.deserializarNuevoMapa(evento);
		accion=new AccionCargarMapa(map->getMapa());
		delete map;
	}
	return accion;
}
Accion* FactoryAcciones::obtenerAccionNuevoElemento(EventoNuevoElemento* evento){
	/*crea el la accion nuevo del tipo q se especifique en el evento*/
	AccionConPosicion* nueva=NULL;
	int x=0;
	if(evento->getTipo()==BOMBERMAN){
		std::cerr<<x<<std::endl;
		x++;
		nueva=new AccionNuevoBomberman();
		nueva->setId(evento->getId());
		nueva->setXY(evento->getX(),evento->getY());
	}
	if(evento->getTipo()==CAJA){
		nueva=new AccionNuevoCaja();
		nueva->setId(evento->getId());
		nueva->setXY(evento->getX(),evento->getY());
	}
	if(evento->getTipo()==ZVERDE){
		nueva=new AccionNuevoZombieVerde();
		nueva->setId(evento->getId());
		nueva->setXY(evento->getX(),evento->getY());
	}
	if(evento->getTipo()==ZAZUL){
		nueva=new AccionNuevoZombieAzul();
		nueva->setId(evento->getId());
		nueva->setXY(evento->getX(),evento->getY());
	}
	if(evento->getTipo()==ZROJO){
		nueva=new AccionNuevoZombieRojo();
		nueva->setId(evento->getId());
		nueva->setXY(evento->getX(),evento->getY());
	}
	if(evento->getTipo()==BOMBAB){
		nueva=new AccionNuevoBombaBomberman();
		nueva->setId(evento->getId());
		nueva->setXY(evento->getX(),evento->getY());
	}
	if(evento->getTipo()==BOMBAP){
		nueva=new AccionNuevoBombaPlanta();
		nueva->setId(evento->getId());
		nueva->setXY(evento->getX(),evento->getY());
	}
	if(evento->getTipo()==FIRE){
		nueva=new AccionNuevoExplosion();
		nueva->setId(evento->getId());
		nueva->setXY(evento->getX(),evento->getY());
	}
	return nueva;
}

AccionCambioPosicion* FactoryAcciones::crearAccionCambioPosicion(EventoPosicion* evento){
	AccionCambioPosicion* accion=new AccionCambioPosicion();
	accion->setXY(evento->getX(),evento->getY());
	accion->setId(evento->getId());
	return accion;
}

AccionModificarVisibilidad* FactoryAcciones::crearAccionModificarVisibilidad(EventoModificarVisibilidad*evento){
	AccionModificarVisibilidad* accion=new AccionModificarVisibilidad();
	accion->setId(evento->getId());
	accion->setVisibilidad(evento->getVisibilidad());
	return accion;
}
AccionModificarCantVidas* FactoryAcciones::crearAccionModificarCantVidas(EventoModificarCantidadVidas* evento){
	AccionModificarCantVidas* accion=new AccionModificarCantVidas();
	accion->setId(evento->getId());
	accion->setCant(evento->getCantVidas());
	return accion;
}


AccionModificarPuntuacion* FactoryAcciones::crearAccionModificarPuntuacion(EventoModificarPuntuacion* evento){
	AccionModificarPuntuacion* accion=new AccionModificarPuntuacion();
	accion->setId(evento->getId());
	accion->setPunt(evento->getPunt());
	return accion;
}

AccionAsignarJugador* FactoryAcciones::crearAccionAsignarIdJugador(EventoAsignarIdJugador* evento){
	AccionAsignarJugador* accion=new AccionAsignarJugador();
	accion->setId(evento->getId());
	return accion;
}