#ifndef SUPERFICIE
#define SUPERFICIE
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "Sprite.h"


class Superficie{
	/*superficie en la q se van a dibujar los
	 * dibujables reproducibles*/
	private:
		SDL_Surface* superficie;
		SDL_Surface* buffer;
	public:
		Superficie();
		void setModoVideo(int largo,int ancho,bool comp);
		void actualizarSuperficie();
		SDL_Surface* getSuperficie(){return superficie;}
	};
			
#endif
