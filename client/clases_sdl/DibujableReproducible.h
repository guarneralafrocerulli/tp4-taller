#ifndef DIBUJABLE_REPRODUCIBLE	
#define DIBUJABLE_REPRODUCIBLE
#include "Superficie.h"
#include "Fuentes.h"
#include "Sonido.h"
#include <string>
#include <map>
#include <vector>
using std::map;
using std::string;
using std::vector;

class DibujableReproducible{
	/*cada dibujable repdorucible tiene asociado 2 sonidos
	 uno q se reproducira cuando se cambie la visibilidad de
	 true a false y otro de false a true.
	 los distintos frames se van cambiando dependiendo
	 de la posicion anterior*/
	protected:
		Reproductor* aparecer;// sonido q se reproducira cuando visible=true
		Reproductor* desaparecer;// visible=false
		Sprite sprite;
		bool borrable;
	public:
		DibujableReproducible();
		virtual ~DibujableReproducible(){}
		void agregarSonidoAparecer(Reproductor* son){aparecer=son;}
		void agregarSonidoDesaparecer(Reproductor* son){desaparecer=son;}
		void reproducirAparecer();
		void reproducirDesaparecer();
		void agregarFrame(FrameSprite* frame);
		void agregarOffset(int ofx,int ofy);
		bool setFrame(int i);
		virtual void dibujar(Superficie sup);
		void setPos(int x, int y);
		void setVisibilidad(bool vis);
		void setNoBorrable(){borrable=false;}
		bool esBorrable(){return borrable;}
		int getX(){return sprite.getX();}
		int getY(){return sprite.getY();}
		bool esVisible(){return sprite.getVisibilidad();}
	};


class DibujablesFijos:public DibujableReproducible{
	/*
	 * Se encarga de dibujar los elementos q no cambian de posicion
	 * en pantalla como las plantas, el pasto y las cajas 
	 * de metal
	 */
	private:
		vector< vector<int> > mapa;
		void dibujarBloque(int xInicial, int yInicial,
						   int xFinal, int yFinal, int tipo, Superficie &sup);
	public:
		DibujablesFijos();
		~DibujablesFijos(){};
		void agregarFrame(int cod,FrameSprite* frame);
		void cargarMapa(vector< vector<int> > map){mapa=map;}
		void dibujar(Superficie sup);
};
	
class DatosJugador:public DibujableReproducible{
	/*dibuja los datos del juegador
	 * vidas y puntuacion*/
	private:	
		int puntuacion;
		int cant_vidas;
		Texto punt;
	public:
		DatosJugador(int vidas);
		DatosJugador();
		~DatosJugador();
		void setPuntuacion(int pt){ puntuacion=pt;}
		void setCantVidas(int vid){cant_vidas=vid;}
		void dibujar(Superficie sup);
	};
#endif 
