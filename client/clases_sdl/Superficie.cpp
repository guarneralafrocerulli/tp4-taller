#include "Superficie.h"
#include <string>

Superficie::Superficie(){
	/*centra la ventana sdl*/
	putenv("SDL_VIDEO_CENTERED=1");
}
/*
Superficie::Superficie(int largo, int ancho){
	putenv("SDL_VIDEO_CENTERED=1");
	//putenv("SDL_VIDEODRIVER=dga");
	//buffer=SDL_CreateSurfaceRGB(A);
	superficie=SDL_SetVideoMode(largo,ancho,32,SDL_HWSURFACE|SDL_NOFRAME|SDL_DOUBLEBUF); //|SDL_FULLSCREEN pantalla completa
}*/

void Superficie::setModoVideo(int largo, int ancho,bool comp){
  if(!comp){
	superficie=SDL_SetVideoMode(largo,ancho,32,SDL_HWSURFACE|SDL_DOUBLEBUF);
	return;
	}
  superficie=SDL_SetVideoMode(largo,ancho,32,SDL_HWSURFACE|SDL_NOFRAME|SDL_DOUBLEBUF|SDL_FULLSCREEN);
}

void Superficie::actualizarSuperficie(){
	/*actualiza la superficie q se va a mostrar en pantalla*/
		SDL_Flip(superficie);
		SDL_FreeSurface(superficie);
	}
