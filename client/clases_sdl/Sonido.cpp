#include "Sonido.h"
#include <iostream>
using std::cerr;
using std::endl;
bool ReproductorWav::cargarSonido(string dsonido){
	sonido=Mix_LoadWAV(dsonido.c_str());
	if(!sonido){
		 cerr<<"error cargar: "<<dsonido<<endl;
		 return false;
	 }
	return true;
}

bool ReproductorWav::reproducir(){
	if(sonido){
		Mix_PlayChannel(-1,sonido,0);
		return true;
	}
	return false;
}


bool ReproductorMusica::cargarSonido(string dsonido){
	tema=Mix_LoadMUS(dsonido.c_str());
	if(!tema){
		 cerr<<"error cargar: "<<dsonido<<Mix_GetError()<<endl;
		 return false;
	 }
	return true;
}

bool ReproductorMusica::reproducir(){
	if(tema){
		Mix_PlayMusic(tema,-1);
		return true;
	}
	return false;
}
