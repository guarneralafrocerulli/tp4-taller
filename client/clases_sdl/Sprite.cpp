#include "Sprite.h"
#include <string>
#include <iostream>
#include <stdexcept>
using std::cerr;
using std::endl;

bool FrameSprite::cargar(string direccion){
	/*carga una imagen en el frame*/
	SDL_Surface* optimizada=NULL;
	imagen=IMG_Load(direccion.c_str());
	if(imagen==NULL){
		string error="error al cargar imagen: ";
		error=error+direccion+"\n";
		cerr<<error<<endl;
		return false;
	}
	optimizada=SDL_DisplayFormat(imagen);
	SDL_FreeSurface(imagen);
	imagen=optimizada;
	SDL_SetColorKey(imagen,SDL_SRCCOLORKEY|SDL_RLEACCEL,SDL_MapRGB(imagen->format,192,192,192)); ///teoricamente hace q el color blanco no se vea
	return true;
}

void FrameSprite::descargar(){
	SDL_FreeSurface(imagen);
	imagen=NULL;
}

FrameSprite:: ~FrameSprite(){
	if(imagen){
		descargar();
	}
}

Sprite:: Sprite(){
	offset_x=0;
	offset_y=0;
	pos_x=0;
	pos_y=0;
	estado=0;
	visible=true;
}
	
void Sprite::agregarFrameSprite(int pos,FrameSprite* frame){
	/*agrega un nuevo frame al sprite*/
	//frames.push_back(frame);
	frames[pos]=frame;
	}

void Sprite::establecerX(int x){
	pos_x=x;
}
void Sprite::establecerY(int y){
	pos_y=y;
}

bool Sprite::setEstado(unsigned int nuevo_estado){
	/*cambia el frame si el nuevo estado no existe
	 automaticamente cambia al estado 0*/
	try{
		frames.at(nuevo_estado);
	}
	catch(const std::out_of_range& oor){
		estado=0;
		return true;
	}
	estado=nuevo_estado;
	return true;
}
void Sprite::setVisibilidad(bool vis){
	visible=vis;
}
void Sprite::dibujar(SDL_Surface *sup){
	/*dibuja en la superficie de dibujo imagen centrada en el punto*/
	if (visible==false) return;
	SDL_Rect pos;
	pos.w=frames[estado]->getSuperficie()->w;
	pos.h=frames[estado]->getSuperficie()->h;
	pos.x=(pos_x-(pos.w/2))-offset_x;
	pos.y=(pos_y-(pos.h/2))-offset_y;
	if (frames[estado]->getSuperficie()){
		SDL_BlitSurface(frames[estado]->getSuperficie(),NULL,sup,&pos);
	}
	
}

void Sprite::agregarOffset(int ofx,int ofy){
	offset_x=ofx;
	offset_y=ofy;
}
int Sprite::getAnchoFrameActual(){
			return frames[estado]->getSuperficie()->w;
		}
