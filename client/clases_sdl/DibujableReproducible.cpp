#include "DibujableReproducible.h"
#include <sstream>
#define PX 700  ////posicion de la puntuacion del jugador cambiar////////////////
#define PY 0
#define METAL 5 //Codigo del metal
#include <iostream>
using std::stringstream;

DibujableReproducible::DibujableReproducible(){
	aparecer=NULL;
	desaparecer=NULL;
	borrable=true;
}
 
void DibujableReproducible::dibujar(Superficie sup){
	sprite.dibujar(sup.getSuperficie());
}
void DibujableReproducible::agregarOffset(int ofx,int ofy){
	sprite.agregarOffset(ofx,ofy);
}
void DibujableReproducible::reproducirAparecer(){
	/*reproduce aparecer si existe*/
	if(aparecer){
		aparecer->reproducir();
	}
}	
void DibujableReproducible::reproducirDesaparecer(){
	/*reproduce aparecer si existe*/
	if(desaparecer){
		desaparecer->reproducir();
	}
}		
void DibujableReproducible::setPos(int x, int y){
	/*establece posicion y cambia el frame segun
	 * en la direccion q se mueva*/
	int aux=getX();
	if(aux>x) setFrame(3);
	if(aux<x) setFrame(2);
	aux=getY();
	if(aux>y) setFrame(0);
	if(aux<y) setFrame(1);
	sprite.establecerX(x);
	sprite.establecerY(y);
}

void DibujableReproducible::setVisibilidad(bool vis){
	/* hace invisible al DibujableReproducible y reproduce el
	 * sonido q corresponda*/
	if(vis==true) reproducirAparecer();
	if(vis==false) reproducirDesaparecer();
	sprite.setVisibilidad(vis);
}

void DibujableReproducible::agregarFrame(FrameSprite* frame){
	if(frame){
		sprite.agregarFrameSprite(sprite.getCantidadFrames(),frame);
	}
}	
bool DibujableReproducible::setFrame(int i){
	/*establece el frame a mostrar*/
	return sprite.setEstado(i);
}

DibujablesFijos::DibujablesFijos(){
}

void DibujablesFijos::agregarFrame(int cod,FrameSprite* frame){
	if(frame){
		sprite.agregarFrameSprite(cod,frame);
	}
}
void DibujablesFijos::dibujar(Superficie sup){
	/*dibuja el mapa almacenado */
	if(!mapa.size()> 0) return ;
	int x;
	int y;
	int cod_mapa;
	/* cargo el mapa*/
	for(unsigned int i = 0; i<mapa.size(); i++){
		for(unsigned int j = 0;j<mapa[0].size(); j++){
			cod_mapa = mapa[i][j];
			x=j*sprite.getAnchoFrameActual();
			y=(i)*sprite.getAnchoFrameActual(); ///p-1
			sprite.setEstado(cod_mapa);
			sprite.establecerX(x);
			sprite.establecerY(y);
			sprite.dibujar(sup.getSuperficie());
		}
	}
	/*relleno de metal los bordes*/
	int xSize = this->mapa[0].size();
	int ySize = this->mapa.size();
	//Borde superior
	dibujarBloque(-6, -6, xSize + 6, 0, METAL, sup);
	//Borde inferior
	dibujarBloque(-6, ySize, xSize + 6, ySize + 6, METAL, sup);
	//Lateral izquierdo
	dibujarBloque(-6, 0, 0, ySize, METAL, sup);
	//Lateral derecho
	dibujarBloque(xSize, 0, xSize+6, ySize, METAL, sup);
}

void DibujablesFijos::dibujarBloque(int xInicial, int yInicial,
							int xFinal, int yFinal, int tipo, Superficie &sup){
	int i, j, x, y;
	sprite.setEstado(tipo);
	for(j = yInicial; j < yFinal; j++){
		y=j*sprite.getAnchoFrameActual();
		for(i = xInicial; i < xFinal; i++){
			x=(i)*sprite.getAnchoFrameActual(); ///p-1
			sprite.establecerX(x);
			sprite.establecerY(y);
			sprite.dibujar(sup.getSuperficie());
		}
	}
}

DatosJugador::DatosJugador(int vidas){
	cant_vidas=vidas;
	puntuacion=0;
	punt.setFuente("fuente.ttf",30);
	//agregarFrame("sprites/bombermanhead.png");
}
DatosJugador::DatosJugador(){
	cant_vidas=0;
	puntuacion=0;
	punt.setFuente("arial.ttf",40);
	punt.setPosicion(PX,PY);
}
void DatosJugador::dibujar(Superficie sup){
	int y=0;
	int x=getX();
	for (y=1; y<=cant_vidas; y++){
		sprite.dibujar(sup.getSuperficie());
		sprite.establecerX(x+(y*sprite.getAnchoFrameActual()));
	}
	sprite.establecerX(x);
	stringstream puntua;
	puntua<<puntuacion;
	punt.setTexto(puntua.str().c_str(),sup.getSuperficie());
}

DatosJugador::~DatosJugador(){
}
