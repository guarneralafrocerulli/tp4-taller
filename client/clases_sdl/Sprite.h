#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <vector>
#include <string>
#include <map>
using std::string;
using std::vector;
using std::map;
class FrameSprite{
	/*cada FrameSprite contiene una imagen*/
	private:
		SDL_Surface* imagen;
	public:
		SDL_Surface* getSuperficie(){return imagen;}
		bool cargar(string direccion);
		void descargar();
	
		~FrameSprite();
};


class Sprite{
	private:
		bool visible;
		int offset_x;
		int offset_y;
		int pos_x;
		int pos_y;
		unsigned int estado;
		map<int,FrameSprite*> frames;
	public:	
		Sprite();
		int getCantidadFrames(){return frames.size();}
		void agregarOffset(int ofx,int ofy);
		int getX(){return pos_x;}
		int getY(){ return pos_y;}
		void setVisibilidad(bool vis);
		bool getVisibilidad(){return visible;}
		void agregarFrameSprite(int pos,FrameSprite* frame);
		void establecerX(int x);
		void establecerY(int y);
		void dibujar(SDL_Surface* marco);
		bool setEstado(unsigned int nuevo_estado);
		int getAnchoFrameActual();
	};
