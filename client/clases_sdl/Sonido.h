#ifndef SONIDO
#define SONIDO
#include <SDL/SDL_mixer.h>
#include <string>
using std::string;




class Reproductor{
	public:
		virtual bool cargarSonido(string sonido)=0;
		virtual bool reproducir(){return true;};
		virtual ~Reproductor(){}
		
};

class ReproductorMusica:public Reproductor{
	/*se usa para reproducir el tema 
	 * principal del juego*/
	private:
		Mix_Music* tema;
	public:
		bool cargarSonido(string sonido);
		bool reproducir();
		~ReproductorMusica(){Mix_FreeMusic(tema);}
	};
	
class ReproductorWav:public Reproductor{
	/*se usa para reproducir los sonidos
	 * del juego por ejemplo cuando un bomberman
	 * muere*/
	private:
		Mix_Chunk *sonido;
	public:
		bool cargarSonido(string sonido);
		bool reproducir();
		~ReproductorWav(){Mix_FreeChunk(sonido);}
	};
#endif
