#ifndef FUENTES_H
#define FUENTES_H
#include <SDL/SDL_ttf.h>
#include <string>
using std::string;
class Texto{
	/*clase q permite mostrar texto sdl en pantalla*/
	private:
		TTF_Font* fuente;
		SDL_Surface* texto;
		SDL_Rect rectangulo;
		SDL_Color color_letra;
		SDL_Color color_fondo;
	public:
		bool setFuente(string fuente_s,int tam);
		bool setTexto(const char* msj,SDL_Surface* sup);
		bool setPosicion(int x, int y);
		~Texto();
			
	};
	
#endif
