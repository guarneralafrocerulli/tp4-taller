#include "Fuentes.h"
#include <iostream>
using std::cerr;
using std::endl;
bool Texto::setFuente(string fuente_s,int tam){
	/*establece la fuente y el tama;o de la misma*/
	fuente=NULL;
	fuente=TTF_OpenFont(fuente_s.c_str(),tam);
	if(!fuente) {
		cerr <<"no se pudo cargar fuente: "<<fuente_s <<SDL_GetError()<< endl;
		return false;
	}
	return true;
}
bool Texto::setTexto(const char* msj, SDL_Surface* sup ){
		if(fuente) {
		color_fondo.r=0;
		color_fondo.g=0;
		color_fondo.b=0;
		color_letra.r=0;
		color_letra.g=0;
		color_letra.b=0;
		texto=TTF_RenderText_Blended(fuente,msj,color_letra);
		rectangulo.w=texto->w;
		rectangulo.h=texto->h;
		SDL_Rect rect;
		rect.x=rectangulo.x;
		rect.y=rectangulo.y;
		SDL_SetColorKey(texto,SDL_SRCCOLORKEY|SDL_RLEACCEL,SDL_MapRGB(texto->format,0,0,0));
		SDL_BlitSurface(texto,NULL,sup,&rect);
		return true;}
		
	return false;
}
bool Texto::setPosicion(int x, int y){
	rectangulo.y=y;
	rectangulo.x=x;
	return true;
}

Texto::~Texto(){
	SDL_FreeSurface(texto);
	TTF_CloseFont(fuente);
}