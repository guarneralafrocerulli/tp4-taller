#include "ControladorTeclado.h"
#include <sstream>
#include <iostream>
#include "common/Serializadores.h"
#include "common/eventos.h"
#include <unistd.h>

using std::cerr;
using std::endl;

Tecla::Tecla(){
	this->abajo = false;
	this->arriba = false;
	this->derecha = false;
	this->izquierda = false;
	this->bomba = false;
	this->salir = false;
}


ThreadManejoTelclado::ThreadManejoTelclado(){
	termino=false;
	id_jugador=NULL;
	socket=NULL;
}
void ThreadManejoTelclado::runFunction(){
	sleep(1);
	escucharTeclas();
}
void ThreadManejoTelclado::asignarJugador(int* id){
	id_jugador=id;
}


void ThreadManejoTelclado:: escucharTeclas(){
	/* detecta teclas presionadas y envia la accion realizada
	 * junto con el id del jugador*/
	SerializadorEvento serializador;
	Tecla tecla_actual;
	SDL_Event event;
	while (termino==false){
		SDL_PollEvent(&event);
		if(event.type==SDL_KEYDOWN){
			if(event.key.keysym.sym==SDLK_UP){
				tecla_actual.arriba=true;
			}else if(event.key.keysym.sym==SDLK_DOWN){
				tecla_actual.abajo=true;
			}else if(event.key.keysym.sym==SDLK_RIGHT){
				tecla_actual.derecha=true;
			}else if(event.key.keysym.sym==SDLK_LEFT){
				tecla_actual.izquierda=true;
			}else if(event.key.keysym.sym==SDLK_ESCAPE){
				tecla_actual.salir=true;
			}else if(event.key.keysym.sym==SDLK_SPACE){
				tecla_actual.bomba=true;
			}
		}else if(event.type==SDL_KEYUP){
			if(event.key.keysym.sym==SDLK_UP){
				tecla_actual.arriba=false;
			}else if(event.key.keysym.sym==SDLK_DOWN){
				tecla_actual.abajo=false;
			}else if(event.key.keysym.sym==SDLK_RIGHT){
				tecla_actual.derecha=false;
			}else if(event.key.keysym.sym==SDLK_LEFT){
				tecla_actual.izquierda=false;
			}else if(event.key.keysym.sym==SDLK_ESCAPE){
				tecla_actual.salir=false;
			}else if(event.key.keysym.sym==SDLK_SPACE){
				tecla_actual.bomba=false;
			}
		}
		try{
		if((*id_jugador)!=-1){
			if(tecla_actual.arriba==true){
				EventoJugador evento("UP",*id_jugador);
				string msg=evento.serializar(&serializador);
				socket->sendString(msg);
			}
			if(tecla_actual.abajo==true){
				EventoJugador evento("DOWN",*id_jugador);
				string msg=evento.serializar(&serializador);
				socket->sendString(msg);
			}
			if(tecla_actual.izquierda==true){
				EventoJugador evento("LEFT",*id_jugador);
				string msg=evento.serializar(&serializador);
				socket->sendString(msg);
			}
			if(tecla_actual.derecha==true){
				EventoJugador evento("RIGHT",*id_jugador);
				string msg=evento.serializar(&serializador);
				socket->sendString(msg);
			}
			if(tecla_actual.bomba==true){
				tecla_actual.bomba=false;
				EventoJugador evento("BOMB",*id_jugador);
				string msg=evento.serializar(&serializador);
				socket->sendString(msg);
			}
			if(tecla_actual.salir==true){
				tecla_actual.salir=false;
				EventoDesconectar desconectar(*id_jugador);
				string msg=desconectar.serializar(&serializador);
				socket->sendString(msg);
				terminar();
			}
		}
		SDL_Delay(25);
	}
	catch(char const* err){
		cerr<<"Poblema Conexion desde CONTROLADOR TECLADO"<<endl;
		terminar();
		}
	}
}
/*
void ThreadManejoTelclado:: escucharTeclas(){
	/* detecta teclas presionadas y envia la accion realizada
	 * junto con el id del jugador*/
	/*SDL_Event event;
	try{
	if (!socket) return;
	while (termino==false){
		EventoJugador evento("0",0);
		bool enviar=false;
		SerializadorEvento serializador;
		SDL_PollEvent(&event);
		if(event.type==SDL_KEYUP){
			if( event.key.keysym.sym==SDLK_ESCAPE){
				termino=true;
				EventoDesconectar desconectar(*id_jugador);
				enviar=true;
				string msg=serializador.serializar(&desconectar);
				if(enviar){
					cerr<<msg<<endl;///////////////////////////////////////////////////////////////
					socket->sendString(msg);
					terminar();
				}
				//socket->shutdownSocket();
			}
			if( event.key.keysym.sym==SDLK_SPACE ){
				evento.setId(*id_jugador);
				evento.setTipo("BOMB");
				enviar=true;
				event.key.keysym.sym=SDLK_p;
				string msg=serializador.serializar(&evento);
				if(enviar){
					cerr<<msg<<endl;///////////////////////////////////////////////////////////////
					socket->sendString(msg);
				}
			}	
		}
		while((event.type==SDL_KEYDOWN)&&((*id_jugador)!=-1)){
				if( event.key.keysym.sym==SDLK_UP){
					evento.setId(*id_jugador);
					evento.setTipo("UP");
					enviar=true;
					}
				if( event.key.keysym.sym==SDLK_DOWN){
					evento.setId(*id_jugador);
					evento.setTipo("DOWN");
					enviar=true;
				}
				if( event.key.keysym.sym==SDLK_RIGHT){
					evento.setId(*id_jugador);
					evento.setTipo("RIGHT");
					enviar=true;
					}
				if( event.key.keysym.sym==SDLK_LEFT){
					evento.setId(*id_jugador);
					evento.setTipo("LEFT");
					enviar=true;
					}
				string msg=serializador.serializar(&evento);
				if(enviar){
					cerr<<msg<<endl;///////////////////////////////////////////////////////////////
					socket->sendString(msg);
				}
				SDL_Delay(50);
				SDL_PollEvent(&event);	
		}
	}
}
	catch(char const* err){////// q tire una expcecion para tomarlo desde arriba
		cerr<<err<<endl;
		terminar();
	}*/
