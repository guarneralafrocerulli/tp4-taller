#include "ControladorClienteJuego.h"

ControladorClienteJuego::ControladorClienteJuego(Configuracion &config){
	SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO);
	TTF_Init();
	Mix_OpenAudio(44100,AUDIO_S16,2,4096);
	imagen_audio=new ControladorImagenAudio(config);
	configuracion=&config;
	control_teclado=new ThreadManejoTelclado();
	escuchador.asignarControladorImagenAudio(imagen_audio);
	socket=NULL;
}



void ControladorClienteJuego::comenzarJuego(){
	control_teclado->asignarSocket(*socket);
	escuchador.asignarSocket(*socket);
	control_teclado->run();
	escuchador.run();
	/* si cambia el id en el imagen_sonido cambia en todos
	 * lados*/
	id_jugador=(imagen_audio->obtenerIdJugador());
	control_teclado->asignarJugador(id_jugador);
	////// recibir acciones aca///////
	while ((!control_teclado->yaTermino()&&!escuchador.yaTermino())){
		imagen_audio->actualizarYMostrar();
	}
	escuchador.terminar();
	control_teclado->terminar();
	escuchador.join();
	control_teclado->join();
}

void ControladorClienteJuego::encolarAccion(Accion* accion){
	imagen_audio->encolarAccion(accion);
}
ControladorClienteJuego::~ControladorClienteJuego(){
	///////joinear threads///////////////////////////////////////////////////////////
		/*escuchador.terminar();
		control_teclado.terminar();*/
		int fin=-1;
		/*cambio el id para q no me tire invalid read
		 cuando delete imagen_audio*/
		control_teclado->asignarJugador(&fin);
		escuchador.asignarControladorImagenAudio(NULL);
		delete control_teclado;
		delete imagen_audio;
		Mix_CloseAudio();
		TTF_Quit();
		SDL_Quit();
		
}
