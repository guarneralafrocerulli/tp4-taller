#include "Accion.h"


void AccionConPosicion::setXY(int x,int y){
	this->x=x;
	this->y=y;
}

void AccionNuevoBomberman::realizarAccion(Accionable &ctrl){
	ctrl.realizarAccion(this);
}
void AccionNuevoExplosion::realizarAccion(Accionable &ctrl){
	ctrl.realizarAccion(this);
}
void AccionNuevoCaja::realizarAccion(Accionable &ctrl){
	ctrl.realizarAccion(this);
}
void AccionNuevoZombieVerde::realizarAccion(Accionable &ctrl){
	ctrl.realizarAccion(this);
}
void AccionNuevoZombieAzul::realizarAccion(Accionable &ctrl){
	ctrl.realizarAccion(this);
}
void AccionNuevoZombieRojo::realizarAccion(Accionable &ctrl){
	ctrl.realizarAccion(this);
}

void AccionNuevoBombaPlanta::realizarAccion(Accionable &ctrl){
	ctrl.realizarAccion(this);
}

void AccionNuevoBombaBomberman::realizarAccion(Accionable &ctrl){
	ctrl.realizarAccion(this);
}

void AccionCambioPosicion::realizarAccion(Accionable &ctrl){
	ctrl.realizarAccion(this);
}

void AccionModificarCantVidas::realizarAccion(Accionable &ctrl){
	ctrl.realizarAccion(this);
}

void AccionModificarPuntuacion::realizarAccion(Accionable &ctrl){
	ctrl.realizarAccion(this);
}

void AccionModificarVisibilidad::realizarAccion(Accionable &ctrl){
	ctrl.realizarAccion(this);
}
void AccionCargarMapa::realizarAccion(Accionable &ctrl){
	ctrl.realizarAccion(this);
}
void AccionAsignarJugador::realizarAccion(Accionable &ctrl){
	ctrl.realizarAccion(this);
}
