#include "ControladorImagenAudio.h"
#include "Accion.h"
#define VIDAS 3/////////despues por xml
#define POS_X 30///// define la pos de las vidas en pant
#define POS_Y 30
#include <iostream>
ControladorImagenAudio::ControladorImagenAudio(Configuracion& config){
		id_jugador=-1;///no hay jugador asociado
		datos=NULL;
		superficie.setModoVideo(config.getAnchoResolucion(),config.getLargoResolucion(),config.getPantallaCompleta());
		if(!superficie.getSuperficie()) throw "RESOLUCION INVALIDA";
		fabrica_dibujables=new FactoryDibRep();
		fabrica_dibujables->cargarFrames();
		fabrica_dibujables->cargarSonidos();
		datos=fabrica_dibujables->crearDatosJugador();
		datos->setCantVidas(VIDAS);
		datos->setPos(POS_X,POS_Y);
		camara_juego.establecerAnchoLargo(config.getAnchoResolucion(),config.getLargoResolucion());
			////cargo el temna principal del juego///
		terreno_plantas=fabrica_dibujables->crearDibujablesFijos();
			///// podria estar afuera comienza la reproduccion del tma principal/////////
			//mus->reproducir();
		
}



bool ControladorImagenAudio::asociarACamara(int id){
	/*asocia un jugador a la camara q es la encargada del scroll*/
	id_jugador=id;
	if(hayDibujable(id)){
		camara_juego.asociarJugador(id,personajes[id]);
		//camara_juego.primerOffset();
		return true;
	}
	camara_juego.asociarJugador(-1,NULL);
	return false;
}

void ControladorImagenAudio::cargarNuevoMapa(vector< vector<int> > mapa){
	camara_juego.setCantTiles(mapa[0].size(),mapa.size());
	terreno_plantas->cargarMapa(mapa);
}
	

	
void ControladorImagenAudio::realizarAccion(AccionModificarCantVidas* accion){
	if(id_jugador==accion->getId()){
		modificarCantVidas(accion->getCant());
	}
}

void ControladorImagenAudio::realizarAccion(AccionModificarPuntuacion* accion){
	if(id_jugador==accion->getId()){
		modificarPuntuacion(accion->getPunt());
	}
}
void  ControladorImagenAudio::realizarAccion(AccionAsignarJugador* accion){
	asociarACamara(accion->getId());
}
void ControladorImagenAudio::realizarAccion(AccionCargarMapa* accion){
	cargarNuevoMapa(accion->getMapa());
}

void ControladorImagenAudio::realizarAccion(AccionNuevoBomberman* accion){
	agregarDibujable(accion->getId(),fabrica_dibujables->crear(accion));
}

void ControladorImagenAudio::realizarAccion(AccionNuevoExplosion* accion){
	agregarDibujable(accion->getId(),fabrica_dibujables->crear(accion));
}

void ControladorImagenAudio::realizarAccion(AccionNuevoZombieAzul* accion){
	agregarDibujable(accion->getId(),fabrica_dibujables->crear(accion));
}

void ControladorImagenAudio::realizarAccion(AccionNuevoZombieRojo* accion){
	agregarDibujable(accion->getId(),fabrica_dibujables->crear(accion));
}
void ControladorImagenAudio::realizarAccion(AccionNuevoZombieVerde* accion){
	agregarDibujable(accion->getId(),fabrica_dibujables->crear(accion));
}
void ControladorImagenAudio::realizarAccion(AccionNuevoBombaPlanta* accion){
	//agregarBombaPlanta(accion->getId(),accion->getX(),accion->getY());
	agregarDibujable(accion->getId(),fabrica_dibujables->crear(accion));
}
void ControladorImagenAudio::realizarAccion(AccionCambioPosicion* accion){
	cambiarPosicion(accion->getId(),accion->getX(),accion->getY());
}
void ControladorImagenAudio::realizarAccion(AccionNuevoCaja* accion){
	agregarDibujable(accion->getId(),fabrica_dibujables->crear(accion));
}
void ControladorImagenAudio::realizarAccion(AccionNuevoBombaBomberman* accion){
	agregarDibujable(accion->getId(),fabrica_dibujables->crear(accion));
}

void ControladorImagenAudio::realizarAccion(AccionModificarVisibilidad* accion){
	
	modificarVisibilidad(accion->getId(),accion->getVisibilidad());
}
	
void ControladorImagenAudio::actualizarYMostrar(){
	/* desencola acciones , las procesa, actualiza el offset
	 * y dibuja en pantalla*/
	if(!camara_juego.esElJugadorAsociado(id_jugador)) asociarACamara(id_jugador);
	map<int,DibujableReproducible*>::const_iterator iter;
	if(!camara_juego.esElJugadorAsociado(-1)){
		/*si es -1 significa q no hay jugador asociado por lo q no tiene 
		 * q aplicar offset alguno*/
		camara_juego.actualizarOffset();
		camara_juego.aplicarOffset(*terreno_plantas);
		}
	terreno_plantas->dibujar(superficie);
	for (iter=personajes.begin();iter!=personajes.end();++iter){
		camara_juego.aplicarOffset(*(*iter).second);
	}
	for (iter=personajes.begin();iter!=personajes.end();++iter){
		(*iter).second->dibujar(superficie);
	}
	if(datos){
		datos->dibujar(superficie);
	}
	superficie.actualizarSuperficie();
	limpiarMuertos();
	realizarAccionesEncoladas();
	SDL_Delay(25);
}

ControladorImagenAudio::~ControladorImagenAudio(){
	map<int,DibujableReproducible*>::const_iterator iter;
	map<string,Reproductor*> ::const_iterator iterS;
	for (iter=personajes.begin();iter!=personajes.end();++iter){
		delete (*iter).second;
	}
	delete datos;
	delete terreno_plantas;
	delete fabrica_dibujables;
}
	

void ControladorImagenAudio::modificarVisibilidad(int id,bool vis){
	if (hayDibujable(id)){
		personajes[id]->setVisibilidad(vis);
		/*if((id==id_jugador)&&(vis=true)){
			camara_juego.primerOffset();
		}*/
		
		}
}

void ControladorImagenAudio::modificarCantVidas(int cant){
	if(datos&&cant>=0){
		datos->setCantVidas(cant);
	}
}
	
void ControladorImagenAudio::agregarDibujable(int id,DibujableReproducible* dibuj){
	personajes[id]=dibuj;
}



void ControladorImagenAudio::modificarPuntuacion(int puntaje){
	datos->setPuntuacion(puntaje);
}
	
void ControladorImagenAudio::encolarAccion(Accion* accion){
	mutex_cola.lock();
	cola_acciones.push(accion);
	mutex_cola.unlock();
}
void ControladorImagenAudio::cambiarPosicion(int id,int x,int y){
	if (hayDibujable(id)){
		personajes[id]->setPos(x,y);
	}
}

void ControladorImagenAudio::realizarAccionesEncoladas(){
	Accion* accion_actual=NULL;
	mutex_cola.lock();
	while (!cola_acciones.empty()){
		accion_actual=cola_acciones.front();
		this->realizarAccion(accion_actual);
		cola_acciones.pop();
		delete accion_actual;
	}
	mutex_cola.unlock();
}
bool ControladorImagenAudio::hayDibujable(int id){
	if(personajes.find(id)==personajes.end()){
		return false;
	}
	return true;
}
	
void ControladorImagenAudio::realizarAccion(Accion *accion){
	accion->realizarAccion(*this);
}

void ControladorImagenAudio::limpiarMuertos(){
/*elimina del map de dibujablesReproducibles
 *los elementos con visibilidad false*/
	map<int,DibujableReproducible*>::iterator iter;
	vector<map<int,DibujableReproducible*>::iterator> vec;
	for (iter=personajes.begin();iter!=personajes.end();++iter){
		if(!((*iter).second->esVisible())){
			if((*iter).second->esBorrable()){
				vec.push_back(iter);
			}
		}
	}
	vector<map<int,DibujableReproducible*>::iterator>::iterator iter_borrados;
	for(iter_borrados=vec.begin();iter_borrados!=vec.end();++iter_borrados){
		delete (*(*iter_borrados)).second; //deleteo el dibujableReproducible
		personajes.erase((*iter_borrados));
	}
}
