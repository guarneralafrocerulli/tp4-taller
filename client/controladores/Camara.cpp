#include "Camara.h"

Camara::Camara(){
	id_jugador_asociado=-1;
	jugador=NULL;
	tiles_x=0;
	tiles_y=0;
	offset_x=0;
	offset_y=0;
}
bool Camara::esElJugadorAsociado(int id){
	/*devuelve true si el jugador asociado tiene el mismo id*/
	if(id==id_jugador_asociado) return true;
	return false;
}
bool Camara::asociarJugador(int id,DibujableReproducible*jug){
	/*la camara va a seguri al jugador asociado*/
		id_jugador_asociado=id;
		jugador=jug;
		return true;
}
bool Camara::actualizarOffset(){
	/*actualiza el offset en vase a la pos del jguador*/
	if (!jugador) return false;
	//if ((jugador->getX()<(tiles_x*64)-ancho_p/2-32)&&(jugador->getX()>ancho_p/2)){
		offset_x=jugador->getX()-(ancho_p/2);
	//}
	//if ((jugador->getY()<(tiles_y*64)-largo_p/2-32)&&(jugador->getY()>largo_p/2)){
		offset_y=jugador->getY()-(largo_p/2);
	//}
	return true;
}
/*
bool Camara::primerOffset(){
	if (!jugador) return false;
	offset_x=jugador->getX()-(ancho_p/2);
	offset_y=jugador->getY()-(largo_p/2);
}*/
bool Camara::establecerAnchoLargo(int ancho,int largo){
	/*establece el ancho y largo de la pantalla*/
	ancho_p=ancho;
	largo_p=largo;
	return true;
}

bool Camara::aplicarOffset(DibujableReproducible &dibuj){
	dibuj.agregarOffset(offset_x,offset_y);
	return true;
}

void Camara::setCantTiles(int x,int y){
	/*setea las dimensiones en tiles del mapa */
	tiles_x=x;
	tiles_y=y;
}
