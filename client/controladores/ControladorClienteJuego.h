#ifndef CONTROLADOR_CLIENTE_JUEGO
#define CONTROLADOR_CLIENTE_JUEGO
#include "ControladorImagenAudio.h"
#include "ControladorTeclado.h"
//#include "Configuracion.h"
#include "EscuchadorAcciones.h"
class ControladorClienteJuego{
	private:
		int* id_jugador;
		int termino;
		Configuracion* configuracion;
		ControladorImagenAudio* imagen_audio;
		ThreadManejoTelclado* control_teclado;
		ThreadEscucharAcciones escuchador;
		ClientSocket* socket;
	public:
		ControladorClienteJuego(Configuracion &config);
		~ControladorClienteJuego();
		void comenzarJuego();
		void asignarSocket(ClientSocket& sock){socket=&sock;}
		void encolarAccion(Accion* accion);
};

#endif
