#ifndef CONTROLADOR_TECLADO
#define CONTROLADOR_TECLADO
#include "common/thread.h"
#include "ControladorImagenAudio.h"
#include "common/clientSocket.h"

struct Tecla{
	Tecla();
	bool arriba;
	bool abajo;
	bool izquierda;
	bool derecha;
	bool bomba;
	bool salir;
};


class ThreadManejoTelclado:public Thread{
	/*se encarga de escuchar las teclas presionadas
	 * por el jugador y enviar al servidor los eventos
	 * correspondientes*/
	private:
		int* id_jugador;	 
		ClientSocket* socket;
		bool termino;	
	public:
		bool yaTermino(){return termino;}
		void terminar(){termino=true;}
		void asignarSocket(ClientSocket& sock){socket=&sock;}
		void asignarJugador(int* id);
		ThreadManejoTelclado();
		void escucharTeclas();
		void runFunction();
};
#endif
