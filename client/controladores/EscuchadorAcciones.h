#ifndef ESCUCHADOR
#define ESCUCHADOR
#include "common/thread.h"
#include "common/clientSocket.h"
#include "common/mutex.h"
#include <queue>
#include "Accion.h"
#include "ControladorImagenAudio.h"
using std::queue;

class ThreadEscucharAcciones:public Thread{
	/*se encarga de escuchar los eventos q envia 
	 * el server, generar las acciones q corresponda
	 * y encolarlas en la cola de acciones
	 * del controlador imagen audio*/
	private:
		ClientSocket* socket;
		bool termino;
		ControladorImagenAudio *control;	
	public:
		bool verificarTerminoElJuego(string event);
		void asignarControladorImagenAudio(ControladorImagenAudio *ctrl);
		ThreadEscucharAcciones();
		bool yaTermino(){return termino;}
		void terminar(){termino=true;}
		void asignarSocket(ClientSocket& sock){socket=&sock;}
		void escucharAcciones();
		void runFunction();
};

#endif
