#ifndef PANTALLA_JUEGO
#define PANTALLA_JUEGO
#include <map>
#include <queue>
#include <client/clases_sdl/DibujableReproducible.h>
#include "Camara.h"
#include <client/clases_sdl/Sonido.h>
#include "Accion.h"
#include "common/mutex.h"
#include <client/factory/FactoryDibRep.h>
#include "Configuracion.h"
using std::map;
using std::queue;

class ControladorImagenAudio:public Accionable{
	/*es le clase encargada de mostrar en pantalla
	 * el juego y reproducir los sonidos cuando corresponda
	 * cada vez q se actualiza la pantalla se desencolan las 
	 * acciones y se ejecutan*/
		private:
			FactoryDibRep* fabrica_dibujables;//// esto puede traer problemas si no es puntero
			Mutex mutex_cola;
			queue<Accion*> cola_acciones;
			Superficie superficie;
			void realizarAccionesEncoladas();
			Camara camara_juego;
			int id_jugador;
			map<int,DibujableReproducible*> personajes;
			DatosJugador* datos;
			DibujablesFijos* terreno_plantas;
			void limpiarMuertos();
		public:
			void realizarAccion(Accion * accion);
			void realizarAccion(AccionNuevoExplosion* accion);
			void realizarAccion(AccionNuevoBomberman* accion);
			void realizarAccion(AccionNuevoCaja* accion);
			void realizarAccion(AccionNuevoBombaBomberman* accion);
			void realizarAccion(AccionNuevoZombieVerde* accion);
			void realizarAccion(AccionNuevoZombieAzul* accion);
			void realizarAccion(AccionNuevoZombieRojo* accion);
			void realizarAccion(AccionNuevoBombaPlanta* accion);
			void realizarAccion(AccionCambioPosicion* accion);
			void realizarAccion(AccionModificarCantVidas* accion);
			void realizarAccion(AccionModificarVisibilidad* accion);
			void realizarAccion(AccionModificarPuntuacion* accion);
			void realizarAccion(AccionCargarMapa* accion);
			void realizarAccion(AccionAsignarJugador* accion);
			ControladorImagenAudio(Configuracion& config);
			void actualizarYMostrar();
			~ControladorImagenAudio();
			bool asociarACamara(int id);
			void cambiarPosicion(int id,int x,int y);
			void modificarVisibilidad(int id,bool vis);
			void encolarAccion(Accion* accion);
			void agregarBomberman(int id,int x,int y);
			void agregarBombaBomberman(int id,int x,int y);
			void agregarZombieVerde(int id,int x,int y);
			void agregarBombaPlanta(int id,int x,int y);
			void agregarZombieAzul(int id,int x,int y);
			void agregarZombieRojo(int id,int x,int y);
			void agregarDibujable(int id,DibujableReproducible* dibuj);
			void agregarDibujable(int id,DibujablesFijos* dibuj);
			bool hayDibujable(int id);
			void modificarCantVidas(int cant);
			void modificarPuntuacion(int puntaje);
			int* obtenerIdJugador(){return &id_jugador;}
			void cargarNuevoMapa(vector< vector<int> > mapa);
};

#endif 
