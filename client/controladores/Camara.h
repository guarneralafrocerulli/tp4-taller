#ifndef CAMARA
#define CAMARA
#include <client/clases_sdl/DibujableReproducible.h>
class Camara{
	/* es la encargada de hacer el scroll
	 * sigue al jugador asociado*/
	private:
		int id_jugador_asociado;
		DibujableReproducible* jugador;
		int largo_p;///largo pantalla
		int ancho_p;///ancho pantalla
		int tiles_x;///cant tiles horizontal
		int tiles_y;///cant tiles vertical
		int offset_x;
		int offset_y;
	public:
		Camara();
		//bool primerOffset();
		bool esElJugadorAsociado(int id);
		bool establecerAnchoLargo(int ancho,int largo);
		bool asociarJugador(int id,DibujableReproducible* jug);
		bool aplicarOffset(DibujableReproducible &dibuj);
		bool actualizarOffset();
		void setCantTiles(int x,int y);
};

#endif
