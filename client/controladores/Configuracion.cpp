#include "Configuracion.h"
#include <stdlib.h>
Configuracion::Configuracion(){
	/*abre el archivo de configuracion xml si no existe 
	 * lo crea con los valores por defecto*/
	config=new TiXmlDocument("config.xml");
	if(!config->LoadFile()) crearArchivoConfiguracion();
	TiXmlElement* root=config->FirstChildElement();
	pantalla=root->FirstChildElement();
	red=pantalla->NextSiblingElement();
}

int Configuracion::getAnchoResolucion(){
	return atoi(pantalla->Attribute("Ancho"));
}

int Configuracion::getLargoResolucion(){
	return atoi(pantalla->Attribute("Largo"));
}
		
Configuracion::~Configuracion(){
	config->SaveFile("config.xml");
	delete config;
}

string Configuracion::getIp(){
		return red->Attribute("ip");
	}

int Configuracion::getPuerto(){
		return atoi(red->Attribute("puerto"));
	}	
	
void Configuracion::setIp(string ip){
	red->SetAttribute("ip",ip);
}
void Configuracion::setPuerto(int puerto){
	red->SetAttribute("puerto",puerto);
}		
void Configuracion::setPantallaCompleta(bool pant){
	pantalla->SetAttribute("Completa",pant);
}

bool Configuracion::getPantallaCompleta(){
	return atoi(pantalla->Attribute("Completa"));
}
void Configuracion::setResolucion(int ancho,int largo){
	pantalla->SetAttribute("Ancho",ancho);
	pantalla->SetAttribute("Largo",largo);
}	
void Configuracion::crearArchivoConfiguracion(){
	/*Crea el archivo de configuracion por defecto
	 * del juego*/
	TiXmlDeclaration * decl=new TiXmlDeclaration("1.0","","");
	config->LinkEndChild(decl);
	TiXmlElement *root=new TiXmlElement("Configuracion");
	config->LinkEndChild(root);
	TiXmlElement *pantalla=new TiXmlElement("Pantalla");
	pantalla->SetAttribute("Largo",600);
	pantalla->SetAttribute("Ancho",800);
	pantalla->SetAttribute("Completa",false);
	TiXmlElement *red=new TiXmlElement("Red");
	red->SetAttribute("ip","127.0.0.1");
	red->SetAttribute("puerto",8080);
	root->LinkEndChild(pantalla);
	root->LinkEndChild(red);
}
