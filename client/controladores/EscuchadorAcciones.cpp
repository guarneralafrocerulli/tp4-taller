#include "EscuchadorAcciones.h"
#include <string>
#include <stdio.h>
#include <client/factory/factoryAcciones.h>
#include "common/eventos.h"
using std::string;
#include <iostream>
ThreadEscucharAcciones::ThreadEscucharAcciones(){
	socket=NULL;
	control=NULL;
	termino=false;
}
void ThreadEscucharAcciones::runFunction(){
	escucharAcciones();
}

void ThreadEscucharAcciones::asignarControladorImagenAudio(ControladorImagenAudio *ctrl){
	control=ctrl;
}

void ThreadEscucharAcciones::escucharAcciones(){
	try{
		while (!yaTermino()){
			string evento=socket->recvString();
			if(verificarTerminoElJuego(evento)) terminar();
			Accion* accion=FactoryAcciones::crear(evento);
			if(accion&&!yaTermino()){
				if(control){
					control->encolarAccion(accion);
				}
			}
			
		}
	}
	catch(char const* err){
		std::cerr<<err<<std::endl;
		terminar();
	}
}


bool ThreadEscucharAcciones::verificarTerminoElJuego(string evento){
	TiXmlDocument xml("evento");
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	if(!root){
		std::cout<<evento<<std::endl;
		return false;
	}
	if(root->ValueStr()=="EventoFinDelJuego") return true;
	return false;
}