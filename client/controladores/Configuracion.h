#include "tinyxml.h"
#include <string>
using std::string;
class Configuracion{
	/*se encarga de persistir la configuracion del juego
	 * en un archivo config.xml, si el archivo no existe
	 * lo crea con la configuracion por defecto*/
	private:
		TiXmlDocument* config;
		TiXmlElement* pantalla;
		TiXmlElement* red;
		void crearArchivoConfiguracion();
	public:
		void setIp(string ip);
		void setPuerto(int puerto);
		void setPantallaCompleta(bool pant);
		bool getPantallaCompleta();
		void setResolucion(int largo,int ancho);
		int getAnchoResolucion();
		int getLargoResolucion();
		int getPuerto();
		string getIp();
		Configuracion();
		~Configuracion();
	};
