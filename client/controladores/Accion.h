#ifndef ACCION_H
#define ACCION_H
//#include "ControladorImagenAudio.h"
#include <vector>
using std::vector;
class Accion;
class AccionNuevoCaja;
class AccionNuevoBomberman;
class AccionNuevoZombieAzul;
class AccionNuevoZombieRojo;
class AccionNuevoZombieVerde;
class AccionNuevoBombaPlanta;
class AccionNuevoBombaBomberman;
class AccionCambioPosicion;
class AccionModificarVisibilidad;
class AccionModificarCantVidas;
class AccionModificarPuntuacion;
class AccionCargarMapa;
class AccionAsignarJugador;
class AccionNuevoExplosion;

/*todas las acciones q puede realizar el controlador imagen
 * audio*/

class Accionable{
	public:
		virtual void realizarAccion(Accion * accion)=0;
		virtual void realizarAccion(AccionNuevoExplosion* accion)=0;
		virtual void realizarAccion(AccionNuevoBomberman* accion)=0;
		virtual void realizarAccion(AccionNuevoZombieVerde* accion)=0;
		virtual void realizarAccion(AccionNuevoZombieAzul* accion)=0;
		virtual void realizarAccion(AccionNuevoZombieRojo* accion)=0;
		virtual void realizarAccion(AccionNuevoBombaPlanta* accion)=0;
		virtual void realizarAccion(AccionNuevoBombaBomberman* accion)=0;
		virtual void realizarAccion(AccionNuevoCaja* accion)=0;
		virtual void realizarAccion(AccionCambioPosicion* accion)=0;
		virtual void realizarAccion(AccionModificarCantVidas* accion)=0;
		virtual void realizarAccion(AccionModificarPuntuacion*accion)=0;
		virtual void realizarAccion(AccionModificarVisibilidad* accion)=0;
		virtual void realizarAccion(AccionCargarMapa* accion)=0;
		virtual void realizarAccion(AccionAsignarJugador* accion)=0;
		virtual ~Accionable(){};
	};

class Accion{
		protected:
			int id;
		public:
			int getId(){return id;}
			void setId(int id_n){id=id_n;}
		    virtual void realizarAccion(Accionable &ctrl)=0;
			virtual ~Accion(){};
	};
	
class AccionConPosicion:public Accion{
	private:
			int x;
			int y;
	public:
			void setXY(int x, int y);
			int getX(){ return x;}
			int getY(){ return y;}
			void realizarAccion(Accionable &ctrl)=0;
			virtual ~AccionConPosicion(){};
		};
	
class AccionNuevoBomberman:public AccionConPosicion{
		public:	
			void realizarAccion(Accionable &ctrl);
			~AccionNuevoBomberman(){}
		};
		
class AccionNuevoCaja:public AccionConPosicion{
		public:	
			void realizarAccion(Accionable &ctrl);
			~AccionNuevoCaja(){}
		};		
		
class AccionNuevoZombieVerde:public AccionConPosicion{
		public:
			void realizarAccion(Accionable &ctrl);
			~ AccionNuevoZombieVerde(){}
		};	
			
class AccionNuevoZombieAzul:public AccionConPosicion{
		public:
			void realizarAccion(Accionable &ctrl);
			~AccionNuevoZombieAzul(){}
		};				
class AccionNuevoZombieRojo:public AccionConPosicion{
		public:
			void realizarAccion(Accionable &ctrl);
			~AccionNuevoZombieRojo(){}
		};	
		
class AccionNuevoExplosion:public AccionConPosicion{
public:
	void realizarAccion(Accionable &ctrl);
	~AccionNuevoExplosion(){}
};
	
class AccionNuevoBombaBomberman:public AccionConPosicion{
		public:
			void realizarAccion(Accionable &ctrl);
			~AccionNuevoBombaBomberman(){}
		};
		
class AccionNuevoBombaPlanta:public AccionConPosicion{
		public:
			void realizarAccion(Accionable &ctrl);
			~AccionNuevoBombaPlanta(){}
		};					
			
class AccionCambioPosicion:public AccionConPosicion{
	public:
		void realizarAccion(Accionable &ctrl);
	};

class AccionModificarCantVidas:public Accion{
	/*modifica la cantidad de vidas restantes
	 * q va a mostrar el juego*/
	private:
		int cant;
	public:
		int getCant(){return cant;}
		void realizarAccion(Accionable &ctrl);
		void setCant(int cant_vidas){cant=cant_vidas;}
		~AccionModificarCantVidas(){}
	};

class AccionModificarPuntuacion:public Accion{
	/*modifica la puntuacion q muestra la pantalla*/
	private:
		int punt;
	public:
		void realizarAccion(Accionable &ctrl);
		int getPunt(){return punt;}
		void setPunt(int puntuacion){punt=puntuacion;}
		~AccionModificarPuntuacion(){}
	};
	
class AccionAsignarJugador:public Accion{
	/*asigna un jugador al juego
	 * esto se usa para cuando se envia una tecla
	 * se pueda saber q jugador la esta enviando
	 * y para q el scoll siga a un dibujable reproducible
	 * determinado*/
	public:
		 void realizarAccion(Accionable &ctrl);
		~AccionAsignarJugador(){}
	 };	
	 
class AccionModificarVisibilidad:public Accion{
	/*modifica la visibilidad de un dibujable 
	 * reproducible*/
private:
	bool visibilidad;
public:
	void setVisibilidad(bool vis){visibilidad=vis;}
	bool getVisibilidad(){return visibilidad;}
	void realizarAccion(Accionable &ctrl);
	 ~AccionModificarVisibilidad(){}
	 };
	 
class AccionCargarMapa:public Accion{
	/*perminte cargar un nuevo mapa
	 * en el juego*/
	private:
		vector <vector< int > > mapa;
	public:
		 vector <vector< int > > getMapa(){return mapa;}
		 AccionCargarMapa(vector <vector< int > > map){mapa=map;}
		 void realizarAccion(Accionable &ctrl);
	 };	 
	 
#endif	 
	 
