#include "ventanaCrear.h"
#include <sstream>
#include <glib.h>
using std::stringstream;
#define MAX_PLAYERS 4
#include <client/ImagenBase64.h>

VentanaCrear::VentanaCrear(RefPtr<Builder> refBuilder,int *actual,ClientSocket* socket):Ventana(actual,socket){
	std::cerr<<"ventana crear"<<std::endl;
	refBuilder->get_widget("crearError",lError);
	refBuilder->get_widget("eNombreJugador",eNombreJugador);
	refBuilder->get_widget("wCrear",ventana);
	refBuilder->get_widget("bVolver",bVolver);
	refBuilder->get_widget("bCrearPartida",bCrear);
	refBuilder->get_widget("eNombre",eNombre);
	refBuilder->get_widget("treeLista",tree);
	refBuilder->get_widget("sJugadores",sJugadores);
	refBuilder->get_widget("iCampania",iCampania);
	tree->remove_all_columns();
	listaCampanias=Gtk::ListStore::create(col);
	tree->set_model(listaCampanias);
	tree->append_column("-",col.id);
	tree->append_column("Nombre",col.nombre);
	tree->append_column("Dificultad",col.dificultad);
	eNombre->set_can_focus(true);
	eNombreJugador->set_can_focus(true);
	sJugadores->set_can_focus(true);
	sJugadores->set_increments(1,-1);
	sJugadores->set_range(1,MAX_PLAYERS);
	bVolver->signal_clicked().connect(sigc::mem_fun(*this,&VentanaCrear::volver));
	bCrear->signal_clicked().connect(sigc::mem_fun(*this,&VentanaCrear::crear));
	tree->signal_row_activated().connect(sigc::mem_fun(*this,&VentanaCrear::mostrarImagen));
	refBuilder->get_widget("Derror",mensaje);
	ventana->show_all();
	pedirLista();
}
VentanaCrear::~VentanaCrear(){
	//delete tree;
	delete iCampania;
	delete lError;
	delete bVolver;
	delete bCrear;
	delete nValidar;
	delete eNombre;
	delete sJugadores;
}

void VentanaCrear::mostrarImagen(const Gtk::TreeModel::Path& path,Gtk::TreeViewColumn* column){
	try{
		RefPtr<TreeSelection> ref=tree->get_selection();
		TreeModel::iterator iter=ref->get_selected();
		std::cout<<(*iter).get_value(col.id)<<std::endl;
		EventoClientePedirImagen pedir((*iter).get_value(col.id));
		SerializadorEvento ser;
		socket->sendString(ser.serializar(&pedir));
		string sImagen=socket->recvString();
		EventoServerImagen* campania=ser.deserializarImagen(sImagen);
		if(!campania) {
			std::cerr<<"IMAGEN CORRUPTA"<<std::endl;
			return;
		}
		ImagenBase64 decoder;
		guint8* pixels=decoder.decodeImage(campania->getImagen()->getImagenBase64());
		int alto=campania->getImagen()->getAlto();
		int ancho=campania->getImagen()->getAncho();
		bool has_alpha=campania->getImagen()->gethasalpha();
		int bits_per_sample=campania->getImagen()->getBitspersample();
		int rowstride=campania->getImagen()->getRowstride();
		Glib::RefPtr<Gdk::Pixbuf>buf= Gdk::Pixbuf::create_from_data(pixels,Gdk::COLORSPACE_RGB,has_alpha,bits_per_sample,ancho,alto,rowstride);
		iCampania->set(buf);
		delete campania;
	}catch(char const * error){}
}

void VentanaCrear::agregarCampania(int ids,string nombre, int dificultad){
	/*RefPtr<TreeSelection> ref=tree->get_selection();
	TreeModel::iterator iter=ref->get_selected();
	if(iter) std::cout<<(*iter).get_value(col.id)<<std::endl;*/
	Gtk::TreeModel::Row fila=*(listaCampanias->append());
	fila[col.id]=ids;
	fila[col.nombre]=nombre;
	string dif="";
	switch(dificultad){
		case 0:
			dif="Facil";
			break;
		case 1:
			dif="Medio";
			break;
		case 2:
			dif="Dificil";
			break;
	}
	fila[col.dificultad]=dif;
}

void VentanaCrear::volver(){
	socket->shutdownSocket();
	*actual=PRINCIPAL;
	esconder();
}
void VentanaCrear::crear(){
	/*envio eventoCrearPartida con id campania seleccionada
	 * nombre para la partida y la cantidad de jugadores*/
	string error="";
	stringstream sCant;
	//sCant<<eCant->get_text();
	sCant<<sJugadores->get_value_as_int();
	int cant_jugadores;
	sCant>>cant_jugadores;
	if(cant_jugadores==0) error="Cantidad Jugadores Invalida\n";
	string nombre_partida=eNombre->get_text();
	if(nombre_partida=="") error=error+"Nombre Partida Invalido\n";
	string nombre_jugador=eNombreJugador->get_text();
	if(nombre_jugador=="") error=error+"Nombre Jugador Invalido\n";
	SerializadorEvento ser;
	RefPtr<TreeSelection> ref=tree->get_selection();
	TreeModel::iterator iter=ref->get_selected();
	if(!iter) error=error+"Seleccion Invalida\n";
	if(error!=""){
		mensaje->set_secondary_text(error);
		mensaje->show();
		return;
	}
	EventoClienteCrearPartida evento((*iter).get_value(col.id),nombre_partida,nombre_jugador,cant_jugadores);
	try{
		socket->sendString(ser.serializar(&evento));
	}catch(char const * error){
		*actual=PRINCIPAL;
		esconder();
		return ;
	}
	*actual=ESPERA;
	esconder();
}

///agregar verificiacion de socket////////////////////////////////////////////////
void VentanaCrear::pedirLista(){
	/* envio al server la peticion de lista de campanias
	 * recibo la lista , deserializo y las almaceno en el
	 * listStore*/
	SerializadorEvento ser;
	EventoClientePedirLista plista(CAMPANIA);
	////envia el evento serializador/////
	try{
		socket->sendString(ser.serializar(&plista));
		///recibo la resupesta del server (listas de campanias
		string listasSerializadas=socket->recvString();
		std::cout<<"lista:"<<listasSerializadas<<std::endl;
		EventoServerLista* listas=ser.deserializarListas(listasSerializadas);
		vector<PartidaCampania*> campanias=listas->getLista();
		delete listas;
		vector<PartidaCampania*>::const_iterator iterador;
		for (iterador=campanias.begin(); iterador!=campanias.end();++iterador){
			agregarCampania((*iterador)->getId(),(*iterador)->getNombre(),(*iterador)->getDificultad());
			//lcampanias.push_back(*iterador);
			delete (*iterador);
		}
	}catch(char const * error){
		*actual=PRINCIPAL;
		esconder();
		return ;
	}
}
