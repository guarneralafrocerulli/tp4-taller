#ifndef LAUNCHER_H
#define LAUNCHER_H
#include "ventana.h"
#include "ventanaCrear.h"
#include "ventanaUnirse.h"
#include "ventanaEspera.h"
#include "ventanaPrincipal.h"
#include "ventanaConfig.h"
#include <map>
using std::map;


class Launcher{
private:
	ClientSocket* socket;
	RefPtr<Builder> refBuilder;
	Ventana* ventana;
	int x;
	
public:
	vector<ClientSocket*>sockets_usados;
	vector<Ventana*> ventanas_usadas;
	int actual;
	Launcher();
	~Launcher();
	Window * getVentana();
};

#endif