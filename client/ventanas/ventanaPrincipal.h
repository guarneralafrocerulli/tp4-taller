#ifndef VENTANA_PRINCIPAL
#define VENTANA_PRINCIPAL
#include "ventana.h"

class VentanaPrincipal:public Ventana{
	/*Ventana principal del juego*/
private:
	Configuracion config;
	Button* bCrear;
	Button* bUnirse;
	Button* bSalir;
	Button* bConfig;
	Button* bDError;
	MessageDialog* mensaje;
public:
	~VentanaPrincipal();
	VentanaPrincipal(RefPtr<Builder> refBuilder,int *actual,ClientSocket* socket);
	void salir();
	void aceptar(){mensaje->hide();}
	bool conectar();
	void crearPartida();
	void unirseAPartida();
	void configurar();
};

#endif