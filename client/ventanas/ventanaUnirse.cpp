#include "ventanaUnirse.h"

VentanaUnirse::VentanaUnirse(RefPtr< Builder > refBuilder, int* actual, ClientSocket* socket): Ventana(actual,socket){
	refBuilder->get_widget("wUnirse",ventana);
	refBuilder->get_widget("lUError",lError);
	refBuilder->get_widget("bUVolver",bUVolver);
	refBuilder->get_widget("bUActualizar",bActualizar);
	refBuilder->get_widget("bUnirsePartida",bUnirse);
	refBuilder->get_widget("lPartidas",tree);
	refBuilder->get_widget("eUNombreJugador",eUNombreJugador);
	refBuilder->get_widget("Derror",error);
	eUNombreJugador->set_can_focus(true);
	listaPartidas=Gtk::ListStore::create(columnas);
	tree->remove_all_columns();
	tree->set_model(listaPartidas);
	tree->append_column("ID",columnas.id);
	tree->append_column("Nombre",columnas.nombre);
	tree->append_column("Cantidad jugadores",columnas.cant_jugadores);
	bUVolver->signal_clicked().connect(sigc::mem_fun(*this,&VentanaUnirse::volver));
	bUnirse->signal_clicked().connect(sigc::mem_fun(*this,&VentanaUnirse::unirse));
	bActualizar->signal_clicked().connect(sigc::mem_fun(*this,&VentanaUnirse::actualizar));
	ventana->show_all();
	actualizar();
}
void VentanaUnirse::volver(){
	socket->shutdownSocket();
	*actual=PRINCIPAL;
	esconder();
}
void VentanaUnirse::unirse(){///habria q agregar un mensaje de respuesta del servidor por si no hay lugar
	/*envio eventoUnirsePartida , si no selecciono
	 * una partida avisa por pantalla*/
	SerializadorEvento ser;
	string error="";
	string nombre_jugador=eUNombreJugador->get_text();
	if(nombre_jugador=="") error=error+"Nombre Jugador Invalido\n";
	RefPtr<TreeSelection> ref=tree->get_selection();
	TreeModel::iterator iter=ref->get_selected();
	if(!iter) error=error+"Seleccion Invalida\n";
	if(error!=""){
		this->error->set_secondary_text(error);
		this->error->show();
	}
	EventoClienteUnirsePartida evento((*iter).get_value(col.id),nombre_jugador);
	EventoServerUnirPartida* unirse=NULL;
	try{
		socket->sendString(ser.serializar(&evento));
		string aceptado=socket->recvString();
		unirse=ser.deserializarServerUnirPartida(aceptado);
	}catch(char const * err){
		*actual=PRINCIPAL;
		esconder();
		return ;
	}
	if(unirse&&unirse->getUnir()){
		*actual=ESPERA;
		esconder();
	}else{
		this->error->set_secondary_text("Imposible Unirse a la Partida");
		this->error->show();
	}
}



void VentanaUnirse::agregarPartida(int id,string nombre,int cant_jug){
	RefPtr<TreeSelection> ref=tree->get_selection();
	/*TreeModel::iterator iter=ref->get_selected();
	if(iter) std::cout<<(*iter).get_value(col.id)<<std::endl;*/
	Gtk::TreeModel::Row fila=*(listaPartidas->append());
	fila[columnas.id]=id;
	fila[columnas.nombre]=nombre;
	fila[columnas.cant_jugadores]=cant_jug;
}

void VentanaUnirse::actualizar(){
	/* envio al server la peticion de lista de partidas
	 * recibo la lista , deserializo y las almaceno en el
	 * listStore*/
	listaPartidas.clear();
	listaPartidas=Gtk::ListStore::create(columnas);
	tree->remove_all_columns();
	tree->set_model(listaPartidas);
	tree->set_model(listaPartidas);
	tree->append_column("ID",columnas.id);
	tree->append_column("Nombre",columnas.nombre);
	tree->append_column("Cantidad jugadores",columnas.cant_jugadores);
	SerializadorEvento ser;
	EventoClientePedirLista plista(PARTIDA);
	////envia el evento serializador/////
	try{
		socket->sendString(ser.serializar(&plista));
		///recibo la resupesta del server (listas de partidas
		//serializadas)
		string listasSerializadas=socket->recvString();////por ahora no implementado en el server
		EventoServerLista* listas=ser.deserializarListas(listasSerializadas);
		vector<PartidaCampania*> partidas=listas->getLista();
		delete listas;
		vector<PartidaCampania*>::const_iterator iterador;
		for (iterador=partidas.begin(); iterador!=partidas.end();++iterador){
			agregarPartida((*iterador)->getId(),(*iterador)->getNombre(),(*iterador)->getCantJugadores());
			delete (*iterador);
		}
	}catch(char const * err){
		*actual=PRINCIPAL;
		esconder();
		return ;
	}
}