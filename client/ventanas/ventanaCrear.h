#ifndef VENTANA_CREAR
#define VENTANA_CREAR
#include "ventana.h"
class VentanaCrear:public Ventana{
private:
	ColumnasCampanias col;
	SpinButton* sJugadores;
	Label* lError;
	TreeView* tree;
	Image* iCampania;
	Button* bVolver;
	Button* bCrear;
	Button* nValidar;
	Entry* eNombreJugador;
	Entry* eNombre;
	MessageDialog* mensaje;
	vector<PartidaCampania*> lcampanias;
	//RefPtr<Gtk::TreeModel> lCampanias;
	RefPtr<Gtk::ListStore> listaCampanias;
public:
	~VentanaCrear();
	void mostrarImagen(const Gtk::TreeModel::Path& path,Gtk::TreeViewColumn* column);
	void agregarCampania(int id,string nombre, int dificultad);
	void pedirLista();
	VentanaCrear(RefPtr<Builder> refBuilder,int *actual,ClientSocket* socket);
	void volver();
	void crear();
};
#endif