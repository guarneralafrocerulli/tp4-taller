#ifndef VENTANA_CONFIG
#define VENTANA_CONFIG
#include "ventana.h"


class VentanaConfiguracion:public Ventana{
private:
	Button* bVolver;
	Button* bAplicar;
	Entry* eIp;
	Label* lError;
	Entry* ePuerto;
	RadioButton* res0;
	RadioButton* res1;
	RadioButton* res2;
	RadioButton* res3;
	CheckButton* pantallaCompleta;
	Gtk::RadioButton::Group grupo;
	void setearBotones(Configuracion &config);
public:
	VentanaConfiguracion(RefPtr<Builder> refBuilder,int *actual);
	~VentanaConfiguracion();
	void aplicar();
	void volver();
};


#endif