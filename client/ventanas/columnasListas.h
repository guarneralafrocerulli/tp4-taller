#include <gtkmm.h>

class ColumnasCampanias : public Gtk::TreeModelColumnRecord{
public:

  ColumnasCampanias()
    { add(id); add(nombre); add(dificultad);}
  Gtk::TreeModelColumn<int> id;
  Gtk::TreeModelColumn<Glib::ustring> nombre ;
  Gtk::TreeModelColumn<Glib::ustring> dificultad ;
};




class ColumnasPartidas : public Gtk::TreeModelColumnRecord{
public:

  ColumnasPartidas()
    { add(id); add(nombre); add(cant_jugadores);}
  Gtk::TreeModelColumn<int> id;
  Gtk::TreeModelColumn<Glib::ustring> nombre ;
  Gtk::TreeModelColumn<int> cant_jugadores ;
};



class ColumnasJugadores : public Gtk::TreeModelColumnRecord{
public:

  ColumnasJugadores()
    { add(posicion); add(nombre); add(puntuacion);}
  Gtk::TreeModelColumn<int> posicion;
  Gtk::TreeModelColumn<Glib::ustring> nombre ;
  Gtk::TreeModelColumn<int> puntuacion ;
};