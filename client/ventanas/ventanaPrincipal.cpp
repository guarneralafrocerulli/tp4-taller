#include "ventanaPrincipal.h"


VentanaPrincipal::VentanaPrincipal(RefPtr<Builder> refBuilder,int *actual,ClientSocket* socket):Ventana(actual,socket){
	std::cerr<<"ventana principal se crea"<<std::endl;
	refBuilder->get_widget("wPrincipal",ventana);
	refBuilder->get_widget("bCrear",bCrear);
	refBuilder->get_widget("bUnirse",bUnirse);
	refBuilder->get_widget("bSalir",bSalir);
	refBuilder->get_widget("bConfig",bConfig);
	refBuilder->get_widget("bDError",bDError);
	refBuilder->get_widget("Derror",mensaje);
	bDError->signal_clicked().connect(sigc::mem_fun(*this,&VentanaPrincipal::aceptar));
	bSalir->signal_clicked().connect(sigc::mem_fun(*this,&VentanaPrincipal::salir));
	bCrear->signal_clicked().connect(sigc::mem_fun(*this,&VentanaPrincipal::crearPartida));
	bUnirse->signal_clicked().connect(sigc::mem_fun(*this,&VentanaPrincipal::unirseAPartida));
	bConfig->signal_clicked().connect(sigc::mem_fun(*this,&VentanaPrincipal::configurar));
	ventana->show_all();
}

void VentanaPrincipal::salir(){
	*actual=TERMINO;
	esconder();
}
bool VentanaPrincipal::conectar(){
		try{
			if (!socket->isConnected()){
				socket->connectSocket();
			}
			return true;
		}
		catch(char const* err){
			mensaje->show();
			return false;
		}
}
void VentanaPrincipal::crearPartida(){
	//std::cerr<<"crear partida ventana pricipal"<<std::endl;
	if(conectar()){
		mensaje->hide();
		*actual=CREAR;
		esconder();
	}
}

void VentanaPrincipal::unirseAPartida(){
	//std::cerr<<"unirse partida ventana pricipal"<<std::endl;
	if(conectar()){
		mensaje->hide();
		*actual=UNIRSE;
		esconder();
	}
}
void VentanaPrincipal::configurar(){
	*actual=CONFIGURACION;
	esconder();
}

VentanaPrincipal::~VentanaPrincipal(){
	delete bCrear;
	delete bUnirse;
	delete bSalir;
	delete bConfig;
	delete bDError;
	delete mensaje;
}