#include "ventanaEspera.h"
#include "common/jugador.h"
VentanaEspera::VentanaEspera(RefPtr<Builder> refBuilder,int *actual,ClientSocket* socket):Ventana(actual,socket){
	refBuilder->get_widget("wEspera",ventana);
	refBuilder->get_widget("bEVolver",bEVolver);
	refBuilder->get_widget("bComenzar",bComenzar);
	refBuilder->get_widget("tJugadores",tJugadores);
	refBuilder->get_widget("scrolledwindow3",scroll);
	refBuilder->get_widget("imagen",imagen);
	Glib::RefPtr<Gdk::Pixbuf>buf= Gdk::Pixbuf::create_from_file("imagen.jpg",500,300);
	imagen->set(buf);
	tJugadores->remove_all_columns();
	listaJugadores=Gtk::ListStore::create(columnas);
	tJugadores->set_model(listaJugadores);
	tJugadores->append_column("Posicion",columnas.posicion);
	tJugadores->append_column("Nombre",columnas.nombre);
	tJugadores->append_column("Puntuacion",columnas.puntuacion);
	bEVolver->signal_clicked().connect(sigc::mem_fun(*this,&VentanaEspera::volver));
	bComenzar->signal_clicked().connect(sigc::mem_fun(*this,&VentanaEspera::comenzar));
	bEVolver->set_sensitive(false);
	bComenzar->set_sensitive(true);
	ventana->show_all();
	scroll->hide();
}

void VentanaEspera::volver(){
	socket->shutdownSocket();
	//socket->closeSocket();
	*actual=PRINCIPAL;
	esconder();
}

VentanaEspera::~VentanaEspera(){
	delete bEVolver;
	delete bComenzar;
	delete imagen;
	delete puntuaciones;
	delete scroll;
}
void VentanaEspera::comenzar(){
	/* envio eventoClienteEmpezar partida
	 * espero respuesta del servidor
	 * si es true significa q esta todo listo para
	 * q la partida pueda comenzar , lanzo el juego*/
	bComenzar->set_sensitive(false);
	SerializadorEvento ser;
	EventoClienteEmpezarPartida even;
	try{
		socket->sendString(ser.serializar(&even));
		EventoServerEmpezarPartida* eventoEmpezar=NULL;
		bool juegoEmpieza = false;
		while(!juegoEmpieza){
			string comenzar=socket->recvString();
			/*esepero respuesta del servidor*/
			std::cout<<"RECIBIDO::"<<comenzar<<std::endl;
			eventoEmpezar= ser.deserializarServerEmpezarPartida(comenzar);
			juegoEmpieza = eventoEmpezar->getEmpezar();
			delete eventoEmpezar;
			if(juegoEmpieza){ //si empezar es true empieza el juego!
				Configuracion config;
				ControladorClienteJuego cliente(config);
				cliente.asignarSocket(*socket);
				cliente.comenzarJuego();
				imagen->hide();
				scroll->show();
				/*una vez q termino el juego 
				* si se acabaron los niveles
				* o algun jugador pidio desconexion*/
				cargarPuntuaciones();
				bEVolver->set_sensitive(true);
			}
		}
	}catch(char const * error){
		bEVolver->set_sensitive(true);
		/*actual=PRINCIPAL;
		esconder();
		return ;*/
	}
}

void VentanaEspera::cargarPuntuaciones(){
	/* espera recibir la lista de puntuaciones y las 
	 * carga en el listStore*/
	SerializadorEvento ser;
	EventoListaPuntuacion* listaPunt=NULL;
	while(!listaPunt){
		string sPuntuaciones=socket->recvString();
		std::cout<<"PUNTUACIONES:"<<sPuntuaciones<<std::endl;
		listaPunt=ser.deserializarListaPuntuacion(sPuntuaciones);
	}
	if(listaPunt){
		vector<Jugador> jugadores=listaPunt->getJugadores();
		vector<Jugador>::iterator iterador;
		int pos=1;
		for (iterador=jugadores.begin();iterador!=jugadores.end();++iterador){
			agregarJugador(pos,(*iterador).getNombre(),(*iterador).getPuntuacion());
			pos++;
		}
		delete listaPunt;
	}
}

void VentanaEspera::agregarJugador(int posicion,string nombre, int puntuacion){
	Gtk::TreeModel::Row fila=*(listaJugadores->append());
	fila[columnas.posicion]=posicion;
	fila[columnas.nombre]=nombre;
	fila[columnas.puntuacion]=puntuacion;
}