#ifndef VENTANA
#define VENTANA
#include <client/controladores/ControladorClienteJuego.h>
#include <common/Serializadores.h>
#include <gtkmm.h>
#include "columnasListas.h"
using namespace Gtk;
//using namespace Glib;
using Glib::RefPtr;
#define TERMINO -1
#define PRINCIPAL 0
#define CREAR 1
#define UNIRSE 2
#define CONFIGURACION 3
#define ESPERA 4
/*#define ESPERAC 4
#define ESPERAU 5*/


class Ventana{
protected:
	int* actual;
	Window* ventana;
	ClientSocket* socket;
public:
	Ventana(int *actual,ClientSocket* socket);
	Ventana(int *actual);
	Window *getVentana();//{return ventana;}
	void esconder(){ventana->hide();}
	//void mostrar(){ventana->show_all();}
};

#endif