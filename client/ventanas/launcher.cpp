#include "launcher.h"
#include <iostream>

Launcher::Launcher(){
	refBuilder=Builder::create();
	refBuilder->add_from_file("launcher.glade");
	actual=PRINCIPAL;
	ventana=NULL;
	x=0;
	socket=NULL;
}

Launcher::~Launcher(){
	vector<ClientSocket*>::iterator iter;
	vector<Ventana*>::iterator ventanam;
	for (iter=sockets_usados.begin();iter!=sockets_usados.end();++iter){
		delete (*iter);
	}
	for (ventanam=ventanas_usadas.begin();ventanam!=ventanas_usadas.end();++ventanam){
		if(*ventanam) delete (*ventanam);
	}
}

Window* Launcher::getVentana(){
	/*if (ventana) ventana->esconder();
	std::cout<<actual<<std::endl;*/
	Configuracion  conf;
	string ip="";
	x++;
	std::cerr<<x<<std::endl;
	Window* wind;
	switch (actual){
		case PRINCIPAL:{
			ip=conf.getIp();
			printf("Conectando un nuevo socket: %s:%d\n", ip.c_str(), conf.getPuerto());
			socket=new ClientSocket(ip,conf.getPuerto());
			sockets_usados.push_back(socket);
			ventana=new VentanaPrincipal(refBuilder,&actual,socket);
			wind=ventana->getVentana();
			ventanas_usadas.push_back(ventana);
			break;}
		case CREAR:
			ventana=new VentanaCrear(refBuilder,&actual,socket);
			wind= ventana->getVentana();
			ventanas_usadas.push_back(ventana);
			break;
		case UNIRSE:
			ventana=new VentanaUnirse(refBuilder,&actual,socket);
			wind= ventana->getVentana();
			ventanas_usadas.push_back(ventana);
			break;
		case ESPERA:
			ventana=new VentanaEspera(refBuilder,&actual,socket);
			wind= ventana->getVentana();
			break;
		case CONFIGURACION:
			ventana=new VentanaConfiguracion(refBuilder,&actual);
			wind=ventana->getVentana();
			ventanas_usadas.push_back(ventana);
			break;
		case TERMINO:
			wind=NULL;
			break;
	}
	return wind;
}

