#ifndef VENTANA_UNIRSE
#define VENTANA_UNIRSE
#include "ventana.h"

class VentanaUnirse:public Ventana{
private:
	Label* lError;
	Entry* eUNombreJugador;
	Button* bUnirse;
	Button* bUVolver;
	Button* bActualizar;
	MessageDialog* error;
	ColumnasCampanias col;
	TreeView* tree;
	ColumnasPartidas columnas;
	RefPtr<Gtk::ListStore> listaPartidas;
public:
	VentanaUnirse(RefPtr<Builder> refBuilder,int *actual,ClientSocket* socket);
	void agregarPartida(int id, string nombre, int cant);
	void actualizar();//{agregarPartida(4,"sfsdfsd",55);}
	void volver();
	void unirse();
};

#endif