#include "ventanaConfig.h"
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sstream>
using std::stringstream;
VentanaConfiguracion::VentanaConfiguracion(RefPtr<Builder> refBuilder,int *actual):Ventana(actual){
	Configuracion config;
	refBuilder->get_widget("wConfig",ventana);
	refBuilder->get_widget("bAplicar",bAplicar);
	refBuilder->get_widget("bCVolver",bVolver);
	refBuilder->get_widget("lError",lError);
	refBuilder->get_widget("eIp",eIp);
	refBuilder->get_widget("ePuerto",ePuerto);
	refBuilder->get_widget("bPantalla",pantallaCompleta);
	refBuilder->get_widget("res0",res0);
	refBuilder->get_widget("res1",res1);
	refBuilder->get_widget("res2",res2);
	refBuilder->get_widget("res3",res3);
	eIp->set_can_focus();
	ePuerto->set_can_focus();
	eIp->set_text(config.getIp());
	stringstream ss;
	ss<<config.getPuerto();
	ePuerto->set_text(ss.str());
	bAplicar->signal_clicked().connect(sigc::mem_fun(*this,&VentanaConfiguracion::aplicar));
	bVolver->signal_clicked().connect(sigc::mem_fun(*this,&VentanaConfiguracion::volver));
	setearBotones(config);
	
}
VentanaConfiguracion::~VentanaConfiguracion(){
	delete bVolver;
	delete bAplicar;
	delete eIp;
	delete lError;
	delete ePuerto;
	delete res0;
	delete res1;
	delete res2;
	delete res3;
	delete pantallaCompleta;
}
void VentanaConfiguracion::setearBotones(Configuracion &config){
	/*establece q boton esta activado dependiendo de lo q devuelve
	 * configuracion*/
	res0->set_group(grupo);
	res1->set_group(grupo);
	res2->set_group(grupo);
	res3->set_group(grupo);
	pantallaCompleta->set_active(config.getPantallaCompleta());
	int ancho=config.getAnchoResolucion();
	switch(ancho){
		case 800:
			res0->set_active();
			break;
		case 1024:
			res1->set_active();
			break;
		case 1366:
			res2->set_active();
			break;
		case 1920:
			res3->set_active();
			break;
		default:
			res0->set_active();
			break;
	}
	
}

void VentanaConfiguracion::volver(){
	*actual=PRINCIPAL;
	ventana->hide();
}

void VentanaConfiguracion::aplicar(){
	/*verifica puerto valido ip valido
	 * y almacena la nueva configuracion el xml*/
	Configuracion config;
	string ip=eIp->get_text();
	string puerto=ePuerto->get_text();
	int intpuerto;
	stringstream ss;
	ss<<puerto;
	ss>>intpuerto;
	if((intpuerto>=1024)&&(intpuerto<=49151)){
		config.setPuerto(intpuerto);
	}else{
		lError->set_label("PUERTO INVALIDO");
		return;
	}
	if(inet_addr(ip.c_str())!=-1){
		config.setIp(eIp->get_text());
	}else{
		lError->set_label("IP INVALIDO");
		return ;
	}
	if (res0->get_active()){
		config.setResolucion(800,600);
	}
	if (res1->get_active()){
		config.setResolucion(1024,768);
	}
	if(res2->get_active()){
		config.setResolucion(1366,768);
	}
	if(res3->get_active()){
		config.setResolucion(1920,1080);
	}
	if(pantallaCompleta->get_active()){
		config.setPantallaCompleta(true);
	}else{
		config.setPantallaCompleta(false);
	}
	lError->set_label("CAMBIOS GUARDADOS");
}