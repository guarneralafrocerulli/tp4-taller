#ifndef VENTANA_ESPERA
#define VENTANA_ESPERA
#include "ventana.h"

class VentanaEspera:public Ventana{
protected:
	Image* imagen;
	ColumnasJugadores columnas;
	RefPtr<Gtk::ListStore> listaJugadores;
	Button* bEVolver;
	Button* bComenzar;
	TreeView* tJugadores;
	Dialog* puntuaciones;
	ScrolledWindow* scroll;
public:
	~VentanaEspera();
	void cargarPuntuaciones();
	void agregarJugador(int posicion,string nombre,int puntuacion);
	VentanaEspera(RefPtr<Builder> refBuilder,int *actual,ClientSocket* socket);
	void volver();
	virtual void comenzar();
};

#endif