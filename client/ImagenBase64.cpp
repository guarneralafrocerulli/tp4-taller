#include "ImagenBase64.h"

std::string ImagenBase64::encodeImage(guint8* pixels,size_t size)
{ 
std::string encoded=g_base64_encode((const guchar*)pixels,size);
 return encoded ;
}
guchar* ImagenBase64::decodeImage(std::string image64)
{ 
  size_t size=this->decoded_size(image64.size());
  guchar* decoded=g_base64_decode(image64.c_str(),&size);
  return decoded; 
}

size_t ImagenBase64::decoded_size(size_t encoded_size)
{         
   return ((encoded_size+3)/4)*3;
    
}