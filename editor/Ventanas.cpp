#include "Ventanas.h"

Gtk::Window* Ventana::get_window()
{
 return this->ventana;
}

int Ventana::siguiente()
{
return this->sig;
}
Ventana::~Ventana()
{  if(this->sig==SALIR){
  if (this->campania){
    delete this->campania;
  }
 }
   if(this->ventana){
     this->ventana->hide();
     
   }
}
Campania* Ventana::getCampania()
{
 return this->campania;
}

////////////////////////////////////////////////////////////////
///++++++++++++++++++++++ventana principal+++++++++++++++++++++///
VentanaPrincipal::VentanaPrincipal()
{   std::string path=PATH_CONFIGURE;
    this-> refBuilder = Gtk::Builder::create();
    this->refBuilder->add_from_file(path + "glade/ventanaprincipal.glade");
    this->refBuilder->get_widget("ventanaprincipal", this->ventana);
    this->refBuilder->get_widget("nueva",this->nueva);
    this->refBuilder->get_widget("editar",this->editar);
    this->refBuilder->get_widget("salir",this->salir);
    this->setSig(SALIR);
    this->campania=NULL;
    this->set_button_signals();
}

void VentanaPrincipal::set_button_signals()
{       
    this->nueva->signal_clicked().connect( sigc::mem_fun(*this,&VentanaPrincipal::on_nueva_clicked) );	
    this->editar->signal_clicked().connect( sigc::mem_fun(*this,&VentanaPrincipal::on_editar_clicked) );	
    this->salir->signal_clicked().connect( sigc::mem_fun(*this,&VentanaPrincipal::on_salir_clicked) );
}

void VentanaPrincipal::on_editar_clicked()
{  
   this->setSig(VEDITARCAMPANIA);
   this->ventana->hide();
}

void VentanaPrincipal::on_nueva_clicked()
{
  this->setSig(VNUEVACAMPANIA);
  this->ventana->hide();  
}

void VentanaPrincipal::on_salir_clicked()
{ this->setSig(SALIR);
  this->ventana->hide();
}

/*VentanaPrincipal::~VentanaPrincipal()
{ this->setSig(SALIR);
  this->ventana->hide();
}*/
///++++++++++++++++++++++ventana nueva campaña++++++++++++++++++++++++++///
//////////////////////////////////////////////////////////////////
VentanaNueva::VentanaNueva()
{    std::string path=PATH_CONFIGURE;
     this-> refBuilder = Gtk::Builder::create();
     this->refBuilder->add_from_file(path + "glade/ventananuevacampaña.glade");
     this->refBuilder->get_widget("campanianueva",this->ventana);
     this->refBuilder->get_widget("volver",this->volver);
     this->refBuilder->get_widget("aceptar",this->aceptar);
     this->refBuilder->get_widget("salir",this->salir);
     this->refBuilder->get_widget("facil",this->facil);
     this->refBuilder->get_widget("dificil",this->dificil);
     this->refBuilder->get_widget("intermedio",this->intermedio);
     this->refBuilder->get_widget("botonnombre",this->elegircampania);
     this->refBuilder->get_widget("botonimagen",this->elegirimagen);
     this->refBuilder->get_widget("archivoimagen",this->imagencamp);
     this->refBuilder->get_widget("archivonombre",this->nombrecamp);
     this->refBuilder->get_widget("aceptarimagen",this->aceptarimagen);
     this->refBuilder->get_widget("cancelarimagen",this->cancelarimagen);
     this->refBuilder->get_widget("aceptarnombre",this->aceptarnombre);
     this->refBuilder->get_widget("cancelarnombre",this->cancelarnombre);
     this->refBuilder->get_widget("imagencampania",this->imagen);
     this->refBuilder->get_widget("msgimagen",this->msgimagen);
     this->refBuilder->get_widget("msgnombre",this->msgnombre);
     this->refBuilder->get_widget("aceptarmsg",this->aceptarmsg);
     this->refBuilder->get_widget("aceptarmsg2",this->aceptarmsg2);
     Glib::RefPtr< Gtk::FileFilter > filter= Gtk::FileFilter::create();
     filter->add_pattern("*.png");
     filter->set_name(Glib::ustring("PNG"));
     this->imagencamp->add_filter(filter);
     this->setSig(VENTANAPRINCIPAL);
     this->dificultad=0;
     this->campania=NULL;
     this->set_button_signals();
}

void VentanaNueva::set_button_signals()
{
  this->volver->signal_clicked().connect( sigc::mem_fun(*this,&VentanaNueva::on_volver_clicked) );
  this->aceptar->signal_clicked().connect( sigc::mem_fun(*this,&VentanaNueva::on_aceptar_clicked) );
  this->salir->signal_clicked().connect( sigc::mem_fun(*this,&VentanaNueva::on_salir_clicked) );
  Gtk::RadioButton::Group grupodificultad=this->facil->get_group();
  this->intermedio->set_group(grupodificultad);
  this->dificil->set_group(grupodificultad);
  this->facil->signal_clicked().connect( sigc::mem_fun(*this,&VentanaNueva::on_dificultad_clicked) );
  this->dificil->signal_clicked().connect( sigc::mem_fun(*this,&VentanaNueva::on_dificultad_clicked) );
  this->intermedio->signal_clicked().connect( sigc::mem_fun(*this,&VentanaNueva::on_dificultad_clicked) );
  this->elegircampania->signal_clicked().connect( sigc::mem_fun(*this,&VentanaNueva::on_elegircampania_clicked) );
  this->elegirimagen->signal_clicked().connect( sigc::mem_fun(*this,&VentanaNueva::on_elegirimagen_clicked) );
  this->aceptarimagen->signal_clicked().connect( sigc::mem_fun(*this,&VentanaNueva::on_aceptarimagen_clicked) );
  this->cancelarimagen->signal_clicked().connect( sigc::mem_fun(*this,&VentanaNueva::on_cancelarimagen_clicked) );
  this->aceptarmsg->signal_clicked().connect(sigc::mem_fun(*this,&VentanaNueva::on_aceptar_cancelar_msg_clicked) );
  this->aceptarmsg2->signal_clicked().connect(sigc::mem_fun(*this,&VentanaNueva::on_aceptar_cancelar_msg2_clicked) );
  this->cancelarnombre->signal_clicked().connect(sigc::mem_fun(*this,&VentanaNueva::on_cancelarnombre_clicked));
  this->aceptarnombre->signal_clicked().connect(sigc::mem_fun(*this,&VentanaNueva::on_aceptarnombre_clicked));
}

void VentanaNueva::on_dificultad_clicked()
{  if(this->facil->get_active()==true){
   this->dificultad=0;
}else{
    if(this->intermedio->get_active()==true){
      this->dificultad=1;
    }else{
      if(this->dificil->get_active()==true){
	this->dificultad=2;
      }
    }
}
}
////señales top level////
void VentanaNueva::on_aceptar_clicked()
{Gtk::Entry *nombre;
refBuilder->get_widget("nombr",nombre);
Glib::RefPtr<Gtk::EntryBuffer > bufnombre;
bufnombre=nombre->get_buffer();
Glib::ustring textnombre;
textnombre=bufnombre->get_text();
std::string strings= textnombre.c_str();
if(strings.compare("")==0){
  this->ventana->freeze_notify();
  this->msgnombre->show();
}else{
  std::string nombrearch;
  if(!this->folder.empty()){
    this->folder.push_back('/');
  }
  
  this->folder.append(strings);
this->campania=new Campania(this->folder,0);
this->campania->setDificultad(this->dificultad);
this->campania->setNombre(strings);

if((this->imagen->get_pixbuf())==NULL){
  this->ventana->freeze_notify();
  this->msgimagen->show();
 
}else{
Glib::RefPtr<Gdk::Pixbuf> pixbuf=this->imagen->get_pixbuf()->copy();
Gdk::Pixbuf *pix=pixbuf.operator->();
GdkPixbuf *pixbufRef =pix->gobj();
ImagenCampania imagendelacamp;
imagendelacamp.setAlto(pixbuf->get_height());
imagendelacamp.setAncho(pixbuf->get_width());
imagendelacamp.setAlpha(pixbuf->get_has_alpha());
imagendelacamp.setBitspersample(pixbuf->get_bits_per_sample());
imagendelacamp.setRowstride(pixbuf->get_rowstride());
imagendelacamp.setPixels(pixbuf->get_pixels());
imagendelacamp.setImagenBase64(this->codificadorimagen.encodeImage(pixbuf->get_pixels(),gdk_pixbuf_get_byte_length(pixbufRef)));
this->campania->setImagen(&imagendelacamp);
this->setSig(VENTANACAMPANIA);
this->ventana->hide();
}
}
}
void VentanaNueva::on_salir_clicked()
{ this->setSig(SALIR);
  this->ventana->hide();
}

void VentanaNueva::on_volver_clicked()
{ this->setSig(VENTANAPRINCIPAL);
  this->ventana->hide();   
}

void VentanaNueva::on_elegircampania_clicked()
{ 
 this->nombrecamp->show();
 this->ventana->freeze_child_notify();
 
}
void VentanaNueva::on_elegirimagen_clicked()
{
 this->imagencamp->show();
 this->ventana->freeze_child_notify();
 
}
//////extras////////////////////////////////
bool VentanaNueva::verificarPNG(std::string file)
{int ocurrence;
 ocurrence=file.rfind(".png");
 if(ocurrence==-1){
   return false;
 }else{return true;}

}


////señales file choosers//////////////////
void VentanaNueva::on_aceptarimagen_clicked()
{
std::string filename;
filename=this->imagencamp->get_filename();
if(!filename.empty()){
if (this->verificarPNG(filename)){
Glib::RefPtr<Gdk::Pixbuf> pix=Gdk::Pixbuf::create_from_file(filename,150,100);
this->imagen->set(pix);
Gtk::Entry *imagenentry;
refBuilder->get_widget("entry",imagenentry);
Glib::RefPtr<Gtk::EntryBuffer > bufimagen;
bufimagen=imagenentry->get_buffer();
Glib::ustring textnombre=Glib::ustring(filename);
bufimagen->set_text(textnombre);
}
}
this->imagencamp->hide();
this->ventana->activate();
}

void VentanaNueva::on_cancelarimagen_clicked()
{
this->imagencamp->hide();
this->ventana->activate();
}
void VentanaNueva::on_aceptarnombre_clicked()
{
std::string foldername;
foldername=this->nombrecamp->get_filename();
if(!foldername.empty()){
this->folder=foldername;
Gtk::Label *label;
this->refBuilder->get_widget("path",label);
label->set_text(Glib::ustring(this->folder));
}
 this->nombrecamp->hide();
 this->ventana->activate();
}
void VentanaNueva::on_cancelarnombre_clicked()
{
 this->nombrecamp->hide();
 this->ventana->activate();
}

///////////////////señales msg de aviso////////////////////////
void VentanaNueva::on_aceptar_cancelar_msg_clicked()
{
this->msgimagen->hide();
this->ventana->activate();
}
void VentanaNueva::on_aceptar_cancelar_msg2_clicked()
{
this->msgnombre->hide();
this->ventana->activate();
}

////////////////////////////////////////////////////////////////
///++++++++++++++++++++++ventana campaña+++++++++++++++++++++///
VentanaCampania::VentanaCampania(Campania* camp)
{   std::string path=PATH_CONFIGURE;
    this-> refBuilder = Gtk::Builder::create();
    this->refBuilder->add_from_file(path + "glade/ventanacampaña.glade");
    this->refBuilder->get_widget("campania",this->ventana);
    this->refBuilder->get_widget("volver",this->volver);
    this->refBuilder->get_widget("aplicar",this->aplicar);
    this->refBuilder->get_widget("agregar",this->agregar);
    this->refBuilder->get_widget("aceptar",this->aceptar);
    this->refBuilder->get_widget("aceptareliminar",this->aceptareliminar);
    this->refBuilder->get_widget("treeniveles",this->treeniveles);
    this->refBuilder->get_widget("msgCampaniaInvalida",this->msgCampaniaInvalida);
    this->refBuilder->get_widget("aceptarmsgcamp",this->aceptarmsgcamp);
    this->refBuilder->get_widget("cancelarmsgcamp",this->cancelarmsgcamp);
    this->refBuilder->get_widget("imagencampania",this->imagencamp);
    this->refBuilder->get_widget("salir",this->salir);
    this->setSig(VENTANAPRINCIPAL);
    this->campania=camp;
    if(this->campania==NULL){
       this->msgCampaniaInvalida->show();
    }else{
    if (this->campania->getError()){
      this->msgCampaniaInvalida->show();
    }
    this->setNombreCampania();
    this->setDificultadCampania();
    this->setNivelesCampania();
    this->setListaNiveles();
    this->setImagenCampania();
    this->set_button_signals();
    }
}
void VentanaCampania::setNombreCampania()
{ Glib::ustring nombre= Glib::ustring(this->campania->getNombre());
  Gtk::Label *label;
  this->refBuilder->get_widget("nombrecampania",label);
  label->set_text(nombre);
}

void VentanaCampania::setDificultadCampania()
{Glib::ustring dificultad;
 int dif= this->campania->getDificultad();
 if( dif==0){
  dificultad= Glib::ustring("Fácil");
 }else{
   if(dif==1){
     dificultad= Glib::ustring("Intermedio");
   }else{
     dificultad= Glib::ustring("Difícil");
   }
 } 
  Gtk::Label *label;
  this->refBuilder->get_widget("dificultad",label);
  label->set_text(dificultad);
}

void VentanaCampania::setNivelesCampania()
{ std::stringstream ss;
 
  ss<<this->campania->getCantNiveles();
   Glib::ustring cantniveles= Glib::ustring(ss.str());
  Gtk::Label *label;
  this->refBuilder->get_widget("niveles",label);
  label->set_text(cantniveles);

}
void VentanaCampania::setImagenCampania()
{ ImagenCampania *imagen=new ImagenCampania(this->campania->getInfoImagen());
  std::string imagen64=(imagen->getImagenBase64());
  Glib::RefPtr<Gdk::Pixbuf> pixbuf=Gdk::Pixbuf::create_from_data(this->codificadorimagen.decodeImage(imagen->getImagenBase64()),Gdk::COLORSPACE_RGB,imagen->gethasalpha(),
   imagen->getBitspersample(),imagen->getAncho(),imagen->getAlto(),imagen->getRowstride());
 this->imagencamp->set(pixbuf);
 delete imagen;
}
void VentanaCampania::setListaNiveles()
{    this->treeniveles->remove_all_columns();
     this->listaNiveles=Gtk::ListStore::create(this->columna);
     this->treeniveles->set_model(this->listaNiveles);
     this->treeniveles->append_column("Niveles",columna.Nivel);
     for (int i=0; i<(this->campania->getCantNiveles()); i++){
       std::stringstream ss;
       ss<<(i+1);
      Gtk::TreeModel::Row fila=*(this->listaNiveles->append());
	fila[columna.Nivel]="Nivel " + ss.str();
     }
}


void VentanaCampania::set_button_signals()
{
  this->volver->signal_clicked().connect( sigc::mem_fun(*this,&VentanaCampania::on_volver_clicked) );
  this->aplicar->signal_clicked().connect( sigc::mem_fun(*this,&VentanaCampania::on_aplicar_clicked) );
  this->agregar->signal_clicked().connect( sigc::mem_fun(*this,&VentanaCampania::on_agregar_clicked) );
  this->aceptar->signal_clicked().connect( sigc::mem_fun(*this,&VentanaCampania::on_aceptar_clicked) );
  this->salir->signal_clicked().connect( sigc::mem_fun(*this,&VentanaCampania::on_salir_clicked) );
  this->aceptareliminar->signal_clicked().connect(sigc::mem_fun(*this,&VentanaCampania::on_aceptar_eliminar_clicked));
  this->aceptarmsgcamp->signal_clicked().connect(sigc::mem_fun(*this,&VentanaCampania::on_msgcamp_clicked));
  this->cancelarmsgcamp->signal_clicked().connect(sigc::mem_fun(*this,&VentanaCampania::on_msgcamp_clicked)); 
}

void VentanaCampania::on_agregar_clicked()
{
 this->setSig(VENTANAMAPA);
 this->ventana->hide();
}
void VentanaCampania::on_aplicar_clicked()
{this->campania->guardar();
 this->setSig(VENTANAPRINCIPAL);
 this->ventana->hide();
}
void VentanaCampania::on_volver_clicked()
{
  this->setSig(VNUEVACAMPANIA);
  this->ventana->hide();

}
void VentanaCampania::on_salir_clicked()
{
  this->setSig(SALIR);
  this->ventana->hide();
}

void VentanaCampania::on_aceptar_clicked()
{ 
  Glib::RefPtr<Gtk::TreeSelection> ref=this->treeniveles->get_selection();
  if(ref->count_selected_rows()==1){
  Gtk::TreeModel::iterator iter=ref->get_selected();
  std::string nivelelegido=((*iter).get_value(columna.Nivel));
  std::string nivel;
  nivel.push_back(nivelelegido[6]);
  if(this->campania->setNivelEditar(atoi(nivel.c_str())-1)){
  this->setSig(VENTANAMAPA);
  this->ventana->hide();
  } 
  }
}

void VentanaCampania::on_aceptar_eliminar_clicked()
{  
  Glib::RefPtr<Gtk::TreeSelection> ref=this->treeniveles->get_selection();
  if(ref->count_selected_rows()==1){
  Gtk::TreeModel::iterator iter=ref->get_selected();
  std::string nivelelegido=((*iter).get_value(columna.Nivel));
  std::string nivel;
  nivel.push_back(nivelelegido[6]);
   if(this->campania->eliminarNivel(atoi(nivel.c_str()))){
   this->setSig(VENTANACAMPANIA);
   this->ventana->hide();
   }
}
}

void VentanaCampania::on_msgcamp_clicked()
{
 this->msgCampaniaInvalida->hide();
 this->setSig(VENTANAPRINCIPAL);
 this->ventana->hide();
}

////////////////////////////////////////////////////////////////
///++++++++++++++++++++++ventana editar campaña+++++++++++++++++++++///
VentanaEditar::VentanaEditar()
{   std::string path=PATH_CONFIGURE;
    this-> refBuilder = Gtk::Builder::create();
    this->refBuilder->add_from_file(path + "glade/ventanaelegircampania.glade");
    this->refBuilder->get_widget("filechooser",this->filedialog);
    Glib::RefPtr< Gtk::FileFilter > filter= Gtk::FileFilter::create();
    filter->add_pattern("*.xml");
    filter->set_name(Glib::ustring("XML"));
    this->filedialog->add_filter(filter);
    this->ventana=this->filedialog;
    this->refBuilder->get_widget("cancelar",this->cancelar);
    this->refBuilder->get_widget("aceptar",this->aceptar);
    this->setSig(VENTANAPRINCIPAL);
    this->set_button_signals();
    this->campania=NULL;
}

void VentanaEditar::on_cancelar_clicked()
{ this->setSig(VENTANAPRINCIPAL);
 this->ventana->hide();   
}

bool VentanaEditar::verificarFileName(string filename)
{ int ocurrence;
 ocurrence=filename.rfind(".xml");
 if(ocurrence==-1){
   return false;
 }else{return true;}
}

void VentanaEditar::on_aceptar_clicked()
{std::string filename;
filename=this->filedialog->get_filename();
if(!filename.empty()){
if (this->verificarFileName(filename)){
 if (!filename.empty()){
  this->campania=new Campania(filename);
 }
}
}
 if(this->campania!=NULL){
this->setSig(VENTANACAMPANIA);
this->ventana->hide();
 }
}
void VentanaEditar::set_button_signals()
{
 this->cancelar->signal_clicked().connect( sigc::mem_fun(*this,&VentanaEditar::on_cancelar_clicked) );
 this->aceptar->signal_clicked().connect( sigc::mem_fun(*this,&VentanaEditar::on_aceptar_clicked) );
}



/////////////////////////////////////////////////////////////
///++++++++++++++++++++++ventana Mapa+++++++++++++++++++++///
 Mapa::Mapa(Campania* camp){
    this->campania=camp;
    this->guardarImagen=true;
    std::string path=PATH_CONFIGURE;
    this-> refBuilder = Gtk::Builder::create();
    this->refBuilder->add_from_file(path + "glade/editorwindow.glade");
    this->refBuilder->get_widget("window1", this->ventana);
    this->refBuilder->get_widget("msjnohaybomberman", this->msgbomberman);
    this->refBuilder->get_widget("msjnohayzombies", this->msgzombies);
    this->levelview=NULL;
    this->refBuilder->get_widget("toolbox",this->toolbox);
    this->configurarToolbox();
    this->refBuilder->get_widget("viewport1",this->port);
    this->refBuilder->get_widget("listo",this->listo);
    this->refBuilder->get_widget("salir",this->salir);
    this->refBuilder->get_widget("aplicar",this->aplicar);
    this->refBuilder->get_widget("alto",this->alto);
    this->refBuilder->get_widget("ancho",this->ancho);
    this->refBuilder->get_widget("aceptar2",this->aceptarbomber);
    this->refBuilder->get_widget("aceptar3",this->aceptarzombie);

    
    this->set_buttons_signals();
    this->setSig(VENTANACAMPANIA);//es la anterior.
    if (camp->getAEditar()!=-1){
     LevelGrid *levelGrid=camp->getNivel(camp->getAEditar());
     this->levelview=new DibujoMapa(levelGrid);
     this->configurarMapa();
     camp->unsetNivelEditar();
    }else{
      //para que empiece con un mapa precargado de 10x10//
      this->levelview=new DibujoMapa(10,10);
      this->configurarMapa();
    }
}

void  Mapa::agregarToolButtonAToolGroup(std::string imagepath, Glib::ustring label,Gtk::ToolItemGroup *grupo)
{
Gtk::Image *imagen=Gtk::manage(new Gtk::Image(imagepath));
Gtk::ToolButton *boton=Gtk::manage(new Gtk::ToolButton(*imagen,label));
grupo->add(*boton);
}

Gtk::ToolItemGroup* Mapa::agregarToolBoxToolItemGroup(std::string title)
{ if (this->toolbox){
Gtk::ToolItemGroup *nuevogrupo= Gtk::manage(new Gtk::ToolItemGroup(title));
this->toolbox->add(*nuevogrupo);
return nuevogrupo;
}
return NULL;
}

void Mapa::configurarToolbox()
{  std::string path=PATH_CONFIGURE;
  //creo la goma
 Gtk::ToolItemGroup* borrar=this->agregarToolBoxToolItemGroup("Goma");
 this->agregarToolButtonAToolGroup(path + "images/goma.png", Glib::ustring("100"),borrar);
 //creo los spawns
 ////////////////////////////////////////////////////////////////////////
 Gtk::ToolItemGroup* spawns=this->agregarToolBoxToolItemGroup("Spawns");
 this->agregarToolButtonAToolGroup(path + "images/bomber.png",Glib::ustring("1"),spawns);
 this->agregarToolButtonAToolGroup(path + "images/zverde.png",Glib::ustring("2"),spawns);
 this->agregarToolButtonAToolGroup(path + "images/zazul.png",Glib::ustring("7"),spawns);
 this->agregarToolButtonAToolGroup(path + "images/zrojo.png",Glib::ustring("8"),spawns);
//creo grupo obstaculos
 Gtk::ToolItemGroup *obstgroup=this->agregarToolBoxToolItemGroup("Obstáculos");
 this->agregarToolButtonAToolGroup(path + "images/madera.png",Glib::ustring("4"),obstgroup);
 this->agregarToolButtonAToolGroup(path + "images/cajametal.png",Glib::ustring("5"),obstgroup);
//creo plantas
/////plantas lentas///////////////////////////////////////////////////////
Gtk::ToolItemGroup *plantaslentas= this->agregarToolBoxToolItemGroup("Plantas Lentas");
 this->agregarToolButtonAToolGroup(path + "images/plantanorte.png",Glib::ustring("11"),plantaslentas);
 this->agregarToolButtonAToolGroup(path + "images/plantasur.png",Glib::ustring("21"),plantaslentas);
 this->agregarToolButtonAToolGroup(path + "images/plantaizq.png",Glib::ustring("31"),plantaslentas);
 this->agregarToolButtonAToolGroup(path + "images/plantadr.png",Glib::ustring("41"),plantaslentas);
///////////////////////////////////////////////////////////////////////////
//////////////////////7plantas regulares///////////////////////////////////
 Gtk::ToolItemGroup *plantasnormales=this->agregarToolBoxToolItemGroup("Plantas Regulares");
 this->agregarToolButtonAToolGroup(path + "images/plantanorteazul.png",Glib::ustring("12"),plantasnormales);
 this->agregarToolButtonAToolGroup(path +"images/plantasurazul.png",Glib::ustring("22"),plantasnormales);
 this->agregarToolButtonAToolGroup(path + "images/plantaizqazul.png",Glib::ustring("32"),plantasnormales);
 this->agregarToolButtonAToolGroup(path + "images/plantadrazul.png",Glib::ustring("42"),plantasnormales);
////////////////////////////////////////////////////////////////////////////
///////////////////////plantas rapidas//////////////////////////////////////
Gtk::ToolItemGroup *plantasrapidas=this->agregarToolBoxToolItemGroup("Plantas Rapidas");
 this->agregarToolButtonAToolGroup(path + "images/plantanorteroja.png",Glib::ustring("13"),plantasrapidas);
 this->agregarToolButtonAToolGroup(path + "images/plantasurroja.png",Glib::ustring("23"),plantasrapidas);
 this->agregarToolButtonAToolGroup(path + "images/plantaizqroja.png",Glib::ustring("33"),plantasrapidas);
 this->agregarToolButtonAToolGroup(path + "images/plantadrroja.png",Glib::ustring("43"),plantasrapidas);
//////////////////////////////////////////////////////////////////////////
 this->toolbox->show_all();
}

void Mapa::set_buttons_signals(){
 this->listo->signal_clicked().connect( sigc::mem_fun(*this,&Mapa::on_listo_clicked) );
 this->salir->signal_clicked().connect( sigc::mem_fun(*this,&Mapa::on_salir_clicked) );
 this->aplicar->signal_clicked().connect( sigc::mem_fun(*this,&Mapa::on_aplicar_clicked) );
 this->aceptarbomber->signal_clicked().connect( sigc::mem_fun(*this,&Mapa::on_aceptar_cancelar_bomber_clicked) );
 this->aceptarzombie->signal_clicked().connect( sigc::mem_fun(*this,&Mapa::on_aceptar_cancelar_zombie_clicked) );
 this->alto->set_range(10,500);
 this->alto->set_increments(5,5);
 this->ancho->set_range(10,500);
 this->ancho->set_increments(5,5);	
}

void Mapa::on_aplicar_clicked()
{ 
Glib::RefPtr<Gtk::EntryBuffer > buffercol,bufferfil;
buffercol=this->ancho->get_buffer();
bufferfil=this->alto->get_buffer();
Glib::ustring textfil,textcol;
textcol=buffercol->get_text();
textfil=bufferfil->get_text();  
  if(this->levelview==NULL){
    
 this->levelview=new DibujoMapa(atoi(textfil.c_str()),atoi(textcol.c_str()));
 this->configurarMapa(); 
  }else{
    if(!this->matrizdenivel->getEditado()){
      this->port->remove();
      this->levelview=new DibujoMapa(atoi(textfil.c_str()),atoi(textcol.c_str()));
      this->configurarMapa(); 
    }else{
    this->port->remove();
    this->levelview->getMatrizdeNivel()->create(atoi(textcol.c_str()),atoi(textfil.c_str()));
    this->levelview=new DibujoMapa(this->levelview->getMatrizdeNivel());
    this->configurarMapa();
    }
  }

}
void Mapa::configurarMapa()
{
 this->matrizdenivel=this->levelview->getMatrizdeNivel();
 this->toolbox->add_drag_dest(*this->levelview,Gtk::DEST_DEFAULT_HIGHLIGHT, Gtk::TOOL_PALETTE_DRAG_ITEMS, Gdk::ACTION_COPY);
 this->port->add(*levelview);
 this->port->show_all();
}

void Mapa::on_salir_clicked()
{
  this->setSig(VENTANACAMPANIA);
  this->ventana->hide();

}
void Mapa::on_aceptar_cancelar_bomber_clicked()
{
   this->msgbomberman->hide();
}
void Mapa::on_aceptar_cancelar_zombie_clicked()
{
  this->msgzombies->hide();
}

	
bool Mapa::verificarMatrizDeNivel()
{//tengo que verificar si tiene por lo menos un 1 , y un 2,7 o 8
//primero verifico que tenga spawn de bomberman
//luego de zombies
bool bomberman=false;
bool zombies=false;
 for (int i=0; i<this->matrizdenivel->getAlto(); i++){
   for(int j=0; j<this->matrizdenivel->getAncho(); j++){
     int auxvalor=this->matrizdenivel->getData(i,j);
     if(auxvalor==1){
       bomberman=true;
     }
     if((auxvalor==2)||(auxvalor==7)||(auxvalor==8)||(auxvalor==6)||(auxvalor==3)||(auxvalor==9))
       zombies=true;
   }
 }
 if(!bomberman){
   this->msgbomberman->show();
   return false;
 }
 if(!zombies){
   this->msgzombies->show();
   return false;
 }
       
  return true; 
}

void Mapa::on_listo_clicked(){
  if (this->levelview!=NULL){
this->matrizdenivel=this->levelview->getMatrizdeNivel(); 
bool correcta=this->verificarMatrizDeNivel();
this->setSig(VENTANACAMPANIA);
if ((!this->matrizdenivel->getEditado())&&(correcta)){
this->matrizdenivel->setDificultad(this->campania->getDificultad());
this->campania->agregarNivel(this->matrizdenivel);
delete this->levelview;
}

if(correcta){
this->ventana->hide();
}
  }
}

Gtk::Window* Mapa::get_window(){
	return this->ventana;
}

///////////////////////////////////////////////////////////////
