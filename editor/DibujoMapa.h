#ifndef DIBUJOMAPA_H_
#define DIBUJOMAPA_H_
#include <iostream>
#include "common/LevelGrid.h"
#include "Tiles.h"
#define TILESIZE 64
///Clase DibujoMapa , hereda de Gtk::DrawingArea, representa ///
/// el area de dibujo del mapa de nivel.                    ///


class DibujoMapa: public Gtk::DrawingArea { 
 public:
  DibujoMapa(int alto,int ancho);
  DibujoMapa(LevelGrid* nivel);
  void  DibujarNivel(const Cairo::RefPtr< Cairo::Context >& cr);  
 LevelGrid* getMatrizdeNivel();
 ~DibujoMapa();
 /////////////////////////////////////////////////////////////////////
 class MapaItem{
  public:
    MapaItem(std::string pixbufpath,std::string pixname,int x ,int y){
      this->pixbuf=Gdk::Pixbuf::create_from_file(pixbufpath);
      this->name=Glib::ustring(pixname);
      this->x=x;
      this->y=y;
    }
    MapaItem(Glib::RefPtr<Gdk::Pixbuf> pixbufnew,Glib::ustring pixname,int x ,int y){
     this->pixbuf=pixbufnew;
     this->name=pixname;
     this->x=x;
     this->y=y;      
    }
   int getX(){ return this->x;}
   int getY(){ return this->y;}
   std::string get_name(){
     return this->name.c_str();
   }
   void recreate(Glib::RefPtr<Gdk::Pixbuf> pixbufnew,std::string pixname){
     this->pixbuf=pixbufnew;
     this->name=Glib::ustring(pixname);
   }
    Glib::RefPtr<Gdk::Pixbuf> pixbuf;
    Glib::ustring name;
    int x,y;
  };
 /////////////////////////////////////////////////////////////////////////////
private:
  //atributos de la clase
  Glib::RefPtr<Gdk::Pixbuf> imagen;//para la imagen de fondo
  typedef std::vector<MapaItem*> vectorItems_t;
  vectorItems_t vectoritems;
  vectorItems_t itemsdraged;
  int alto;
  int ancho;
  bool drop_data;
  bool nivelaeditar;
  MapaItem *dropItem;
  LevelGrid *matrizdelnivel;
  Tiles fabricadetiles;
protected:
 bool on_draw(const Cairo::RefPtr<Cairo::Context>& cairocontext);
 void on_drag_data_received(const Glib::RefPtr<Gdk::DragContext>& context,int x, int y, const Gtk::SelectionData& selection_data, guint info, guint time);
 bool on_drag_motion(const Glib::RefPtr<Gdk::DragContext>& context, int x, int y, guint time);
 bool on_drag_drop(const Glib::RefPtr<Gdk::DragContext>& context, int x, int y, guint time);
 void dibujarTile( MapaItem *item,const Cairo::RefPtr<Cairo::Context>& cr,bool preview);
 void cargarvectorItems();
 bool cargarMatrizNivel(int posx,int posy,int tipotile);
 bool verificarRepetido(std::string name);
 bool borrarItem(int x,int y);
 int pasarAMultiploDe64(double num);
 bool hayItemEnLaPosicion(MapaItem* item);
 bool esEspecial(MapaItem* itemainsertar,MapaItem* itemanterior);
};
#endif //end DIBUJOMAPA_H_
  
  

