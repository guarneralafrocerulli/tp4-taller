#include "ManejadorVentanas.h"
#define PRINCIPAL 0
#define NUEVACAMPANIA 1
#define EDITARCAMPANIA 2
#define CAMPANIAS 3
#define MAPA 4
Ventana* ManejadorVentanas::ObtenerSiguiente(int cod)
{ Ventana* ventana=NULL;
  if (cod==PRINCIPAL){
    ventana= new VentanaPrincipal();
    }else{
      if (cod==NUEVACAMPANIA){
	ventana= new VentanaNueva();
      }else{
	if(cod==EDITARCAMPANIA){
	  ventana= new VentanaEditar();
	}
      }
    }
 return ventana;
      
}
Ventana* ManejadorVentanas::ObtenerSiguiente(int cod, Campania* camp)
{
    Ventana* ventana=NULL;
    
    if(cod==CAMPANIAS){
      ventana=new VentanaCampania(camp);
    }else{
	  if(cod==MAPA){
	 ventana= new Mapa(camp);
	  }
      }
 return ventana;
}
