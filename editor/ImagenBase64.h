#ifndef IMAGEN_H_
#define IMAGEN_H_
#include <gtkmm.h>
#include <iostream>
class ImagenBase64{
public:
  
  //codifica a base 64
  std::string encodeImage(guint8 * pixels,size_t size);
  size_t decoded_size(size_t encoded_size);
  //decodifica de base 64
  guint8* decodeImage(std::string image64);

};

#endif 