#include "DibujoMapa.h" 

 
DibujoMapa::DibujoMapa(int alto,int ancho)
{
  this->set_size_request(ancho*TILESIZE,alto*TILESIZE);
  this->alto=alto*TILESIZE;
  this->ancho=ancho*TILESIZE;
  this->matrizdelnivel=new LevelGrid();
  this->matrizdelnivel->create(ancho,alto);
  this->matrizdelnivel->setEditado(false);
  bool aux;
  this->imagen=this->fabricadetiles.crearTileTipo(0,aux);
  this->dropItem=0;
  this->drop_data=false;
   set_app_paintable();
}

DibujoMapa::DibujoMapa(LevelGrid* nivel)
{ if(nivel!=NULL){
  nivel->setEditado(true);//seteo que voy a editar el nivel;
  this->set_size_request(nivel->getAncho()*TILESIZE,nivel->getAlto()*TILESIZE);
  this->alto=nivel->getAlto()*TILESIZE;
  this->ancho=nivel->getAncho()*TILESIZE;
  this->matrizdelnivel=nivel;
  bool aux;
  this->imagen=this->fabricadetiles.crearTileTipo(0,aux);
  this->dropItem=0;
  this->drop_data=false;
  bool encontrado=true;
  for(int i=0; i<nivel->getAlto(); i++){
    for(int j=0; j<nivel->getAncho();j++){
  std::stringstream ss;
     ss<<nivel->getData(i,j);
     if(nivel->getData(i,j)!=0){
  Glib::RefPtr< Gdk::Pixbuf >  pixbuf=this->fabricadetiles.crearTileTipo(nivel->getData(i,j),encontrado);
   MapaItem *Item=new MapaItem(pixbuf,ss.str(),j*TILESIZE,i*TILESIZE);  
   this->itemsdraged.push_back(Item);
     }
    }
  }
  this->matrizdelnivel->setEditado(true);
   set_app_paintable();
}
}

bool DibujoMapa::on_draw(const Cairo::RefPtr< Cairo::Context >& cairocontext)
{ 
  for (int i=0; i<this->ancho; i+=TILESIZE){
    for (int j=0; j<this->alto; j+=TILESIZE){
  Gdk::Cairo::set_source_pixbuf(cairocontext,this->imagen,i,j);
  cairocontext->paint();
    }
  }
  if (itemsdraged.size()>0){
  for (int k=0; k<itemsdraged.size(); k++){
    dibujarTile(itemsdraged[k],cairocontext,false);
  } 
  }
   if(dropItem!=NULL){
    dibujarTile(dropItem,cairocontext,true);
   }
  return true;
}

void DibujoMapa::dibujarTile(DibujoMapa::MapaItem* item, const Cairo::RefPtr< Cairo::Context >& cr, bool preview)
{
  Gdk::Cairo::set_source_pixbuf(cr, item->pixbuf,item->x,item->y);
  if(preview){
    cr->paint_with_alpha(0.6);
  }else{
    if((item->x/TILESIZE<= this->matrizdelnivel->getAncho())&&(item->x/TILESIZE<= this->matrizdelnivel->getAlto()))
    this->matrizdelnivel->setXY(item->x/TILESIZE,item->y/TILESIZE,atoi(item->name.c_str()));
    cr->paint();
    }
}

void DibujoMapa::on_drag_data_received(const Glib::RefPtr< Gdk::DragContext >& context, int x, int y, const Gtk::SelectionData& selection_data, guint info, guint time)
{
Gtk::Widget* widget = drag_get_source_widget(context);
  Gtk::ToolPalette* drag_palette = dynamic_cast<Gtk::ToolPalette*>(widget);
  while(widget && !drag_palette)
  {
    widget = widget->get_parent();
    drag_palette = dynamic_cast<Gtk::ToolPalette*>(widget);
  }
  Gtk::ToolItem* drag_item = 0;
  if(drag_palette)
    drag_item = drag_palette->get_drag_item(selection_data);
  Gtk::ToolButton* button = dynamic_cast<Gtk::ToolButton*>(drag_item);
  Glib::ustring nombreboton=button->get_label();
  delete this->dropItem;
  this->dropItem=0;
  bool error=false;
  bool borrar=false;
  MapaItem *Item=NULL;
  std::string aux=nombreboton.c_str();
  if (atoi(nombreboton.c_str())==100){
    borrar=true;
  }
  Glib::RefPtr< Gdk::Pixbuf >  pixbuf=this->fabricadetiles.crearTileTipo(atoi(nombreboton.c_str()),error);
  if (!error){ 
      Item=new MapaItem(pixbuf,nombreboton,this->pasarAMultiploDe64(x),this->pasarAMultiploDe64(y));
    }
  if (Item!=NULL){
  if (this->drop_data){
    if (!borrar){
      if (!this->hayItemEnLaPosicion(Item)){
      itemsdraged.push_back(Item);
      }
    }else{  
     if( this->borrarItem(Item->getX(),Item->getY())){
      this->matrizdelnivel->setXY(Item->getX()/TILESIZE,Item->getY()/TILESIZE,0);  
     }
    }
      context->drag_finish(true,false,time);
    }else{ 
    this->dropItem=Item;
    context->drag_status(Gdk::ACTION_COPY, time);   
  }
  }
 this->queue_draw();
  Gtk::DrawingArea::on_drag_data_received(context, x, y, selection_data, info, time);
}

bool DibujoMapa::on_drag_drop(const Glib::RefPtr< Gdk::DragContext >& context, int x, int y, guint time)
{
  const Glib::ustring target = drag_dest_find_target(context);
  if (target.empty())
    return false;
  this->drop_data= true;
  drag_get_data(context, target, time);
  return true;
}

bool DibujoMapa::on_drag_motion(const Glib::RefPtr< Gdk::DragContext >& context, int x, int y, guint time)
{
  this->drop_data = false; 
 if(dropItem)
  {    
    dropItem->x =this->pasarAMultiploDe64(x);
    dropItem->y =this->pasarAMultiploDe64(y);
    queue_draw();
    context->drag_status(Gdk::ACTION_COPY, time);
  }
  else
  { 
    const Glib::ustring target = drag_dest_find_target(context);
    if (target.empty())
      return false;
    drag_get_data(context, target, time);
}
Gtk::DrawingArea::on_drag_motion(context, x, y, time);
  return true;
}

bool DibujoMapa::borrarItem(int x, int y)
{ int nx=this->pasarAMultiploDe64(x);
  int ny=this->pasarAMultiploDe64(y);
  bool borrado=false;
  for(int i =0; i<itemsdraged.size();i++){
    if((itemsdraged[i]->x==nx)&&(itemsdraged[i]->y==ny)){
      itemsdraged.erase(itemsdraged.begin()+i);
      borrado=true;
    }
  }
return borrado;
}

int DibujoMapa::pasarAMultiploDe64(double num)
{
  int realx=(num/TILESIZE);
  realx=realx*TILESIZE;
  return realx;
}
bool DibujoMapa::esEspecial(MapaItem* itemainsertar,MapaItem* itemanterior)
{//son especiales si el item es una cja de madera y el encontrado es un zombie.
 //primero me fijo si cumplen los requisitos
 bool error;
 if (itemanterior->get_name().compare("4")==0){
  if (itemainsertar->get_name().compare("2")==0){
     Glib::RefPtr< Gdk::Pixbuf >  pixbuf=this->fabricadetiles.crearTileTipo(6,error);
     itemainsertar->recreate(pixbuf,"6");
     return true;
   }else{
     if (itemainsertar->get_name().compare("7")==0){
        Glib::RefPtr< Gdk::Pixbuf >  pixbuf=this->fabricadetiles.crearTileTipo(9,error);
        itemainsertar->recreate(pixbuf,"9");
        return true;
       }else{
	  if (itemainsertar->get_name().compare("8")==0){
        Glib::RefPtr< Gdk::Pixbuf >  pixbuf=this->fabricadetiles.crearTileTipo(3,error);
        itemainsertar->recreate(pixbuf,"3");
        return true;
	  }else{
	    return false;
	  }
       }
   }
 } 
 if (itemainsertar->get_name().compare("4")!=0)
 { return false;   
 }else{
   if (itemanterior->get_name().compare("2")==0){
     Glib::RefPtr< Gdk::Pixbuf >  pixbuf=this->fabricadetiles.crearTileTipo(6,error);
     itemainsertar->recreate(pixbuf,"6");
     return true;
   }else{
     if (itemanterior->get_name().compare("7")==0){
        Glib::RefPtr< Gdk::Pixbuf >  pixbuf=this->fabricadetiles.crearTileTipo(9,error);
        itemainsertar->recreate(pixbuf,"9");
        return true;
       }else{
	  if (itemanterior->get_name().compare("8")==0){
        Glib::RefPtr< Gdk::Pixbuf >  pixbuf=this->fabricadetiles.crearTileTipo(3,error);
        itemainsertar->recreate(pixbuf,"3");
        return true;
	  }else{
	    return false;
	  }
       }
   }
 }
 
}

bool DibujoMapa::hayItemEnLaPosicion(MapaItem* item)
{ bool encontrado=false;
  int i=0;
    while ((!encontrado)&&(i<itemsdraged.size())){
    if((itemsdraged[i]->x==item->x)&&(itemsdraged[i]->y==item->y)){
      encontrado=true;
    }
    i++;
    }
     if (encontrado){
       if (this->esEspecial(item,itemsdraged[i-1])){
	 encontrado=false;
	 this->itemsdraged.erase(itemsdraged.begin()+i-1);
       }
     }
return encontrado;
}

LevelGrid* DibujoMapa::getMatrizdeNivel()
{
return  this->matrizdelnivel;
}

DibujoMapa::~DibujoMapa()
{

}
