#include "Tiles.h"


Glib::RefPtr< Gdk::Pixbuf > Tiles::crearTileTipo(int cod,bool &error)
{ std::string path=PATH_CONFIGURE;
  switch(cod){
  case GOMA :
    this->pixbuf=Gdk::Pixbuf::create_from_file(path + "images/goma.png");  
    break;
  case COD0 :
    this->pixbuf=Gdk::Pixbuf::create_from_file(path + "images/grass.png");    
    break;
  case COD1 :
    this->pixbuf=Gdk::Pixbuf::create_from_file(path + "images/bomber.png"); 
    break;
  case COD2 :
    this->pixbuf= Gdk::Pixbuf::create_from_file(path + "images/zverde.png"); 
    break;
  case COD3 :
    this-> pixbuf= Gdk::Pixbuf::create_from_file(path +"images/maderazrojo.png");
    break;
  case COD4 :
    this->pixbuf= Gdk::Pixbuf::create_from_file(path + "images/madera.png");
    break;
  case COD5 :
    this->pixbuf= Gdk::Pixbuf::create_from_file(path + "images/cajametal.png");
    break;
  case COD6 :
    this->pixbuf= Gdk::Pixbuf::create_from_file(path + "images/maderazverde.png");
    break;
  case COD7 :
    this-> pixbuf=Gdk::Pixbuf::create_from_file(path + "images/zazul.png");
    break;
  case COD8 :
    this-> pixbuf=Gdk::Pixbuf::create_from_file(path + "images/zrojo.png");
    break;
  case COD9 :
    this-> pixbuf=Gdk::Pixbuf::create_from_file(path + "images/maderazazul.png");
    break;
  case COD11:
    this-> pixbuf=Gdk::Pixbuf::create_from_file(path + "images/plantanorte.png");
    break;
  case COD12 :
    this->pixbuf=Gdk::Pixbuf::create_from_file(path + "images/plantanorteazul.png");
    break;
  case COD13 :
    this->pixbuf=Gdk::Pixbuf::create_from_file(path + "images/plantanorteroja.png");
    break;
  case COD21 :
    this-> pixbuf=Gdk::Pixbuf::create_from_file(path + "images/plantasur.png");
    break;
  case COD22 :
    this->pixbuf=Gdk::Pixbuf::create_from_file(path + "images/plantasurazul.png");
    break;
  case COD23 :
    this->pixbuf=Gdk::Pixbuf::create_from_file(path + "images/plantasurroja.png");
    break;
  case COD31 :
    this-> pixbuf=Gdk::Pixbuf::create_from_file(path + "images/plantaizq.png");
    break;
  case COD32 :
    this->pixbuf=Gdk::Pixbuf::create_from_file(path + "images/plantaizqazul.png");
    break;
  case COD33 :
    this-> pixbuf=Gdk::Pixbuf::create_from_file(path + "images/plantaizqroja.png");
    break;
  case COD41 :
    this->pixbuf=Gdk::Pixbuf::create_from_file(path + "images/plantadr.png");
    break;
  case COD42 :
    this->pixbuf=Gdk::Pixbuf::create_from_file(path + "images/plantadrazul.png");
    break;
  case COD43 :
    this->pixbuf=Gdk::Pixbuf::create_from_file(path + "images/plantadrroja.png");
    break;
  default :
    error=true;
}

 return this->pixbuf;

}
