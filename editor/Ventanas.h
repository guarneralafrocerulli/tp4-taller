#ifndef VENTANAS_H_
#define VENTANAS_H_
#include <gtkmm.h>
#include "DibujoMapa.h"
#include "common/Campania.h"
#include "ImagenBase64.h"
#include "ColumnaCampania.h"
#include "common/ImagenCampania.h"
#include "config.h"
#define SALIR -1
#define VENTANAPRINCIPAL 0
#define VNUEVACAMPANIA 1
#define VEDITARCAMPANIA 2
#define VENTANACAMPANIA 3
#define VENTANAMAPA 4
class Ventana{
  
private: 
  Glib::RefPtr<Gtk::Builder> refBuilder;
  ImagenBase64 codificadorimagen;
  int sig;
public:
  int siguiente();
  void setSig(int siguiente){ this->sig=siguiente;}
  void setcamp(Campania* camp){this->campania=camp;}
  void setVentana(Gtk::Window *vent){this->ventana=vent;}
  Gtk::Window * get_window();
  Campania* getCampania();
 ~Ventana();
  Gtk::Window *ventana;
  Campania *campania;
};

//////////////////////////////////////////////////
///////////////////////////////////////////////
class VentanaPrincipal: public Ventana{
public:
  VentanaPrincipal();
  void on_editar_clicked();
  void on_nueva_clicked();
  void on_salir_clicked();
  void set_button_signals();
  
private:
  Glib::RefPtr<Gtk::Builder> refBuilder;
  Gtk::Button *nueva;
  Gtk::Button *editar;
  Gtk::Button *salir;
  
  
};
//////////////////////////////////////////////////
class VentanaNueva: public Ventana{

public:
  VentanaNueva();
 ////handlers de señales ventana toplevel///
  void on_volver_clicked();
  void set_button_signals();
  void on_aceptar_clicked();
  void on_salir_clicked();
  void on_elegircampania_clicked();
  void on_elegirimagen_clicked();
  void on_dificultad_clicked();
 ////handlers de señales ventanas filechooser y alertas///
  void on_aceptarnombre_clicked();
  void on_aceptarimagen_clicked();
  void on_cancelarnombre_clicked();
  void on_cancelarimagen_clicked();
  void on_aceptar_cancelar_msg_clicked();
   void on_aceptar_cancelar_msg2_clicked();
  /////extras//////////////////////////////////
  bool verificarPNG(std::string file);
private:
  int dificultad;
  ////botones ventana toplevel///
  Gtk::RadioButton *facil;
  Gtk::RadioButton *dificil;
  Gtk::RadioButton *intermedio;
  Gtk::Button *aceptar;
  Gtk::Button *salir;
  Gtk::Button *volver;
  Gtk::Button *elegircampania;
  Gtk::Button *elegirimagen;
  ////imagen campania///
  Gtk::Image *imagen;
  ////ventanas filechooser y alertas///
  Gtk::FileChooserDialog *imagencamp;
  Gtk::FileChooserDialog *nombrecamp;
  Gtk::MessageDialog *msgimagen;
  Gtk::MessageDialog *msgnombre;
  ////botones filechooser's///
  Gtk::Button *aceptarimagen;
  Gtk::Button *cancelarimagen;
  Gtk::Button *aceptarnombre;
  Gtk::Button *cancelarnombre;
  Gtk::Button *aceptarmsg;
  Gtk::Button *aceptarmsg2;

  ////carpeta donde se va a guardar la campania////
  std::string folder;
  Glib::RefPtr<Gtk::Builder> refBuilder;
  ImagenBase64 codificadorimagen;
};
//////////////////////////////////////////////////
class VentanaCampania:public Ventana{
public:
  VentanaCampania(Campania* camp);
  void on_volver_clicked();
  void set_button_signals();
  void on_aplicar_clicked();
  void on_agregar_clicked();
  void on_aceptar_clicked();
  void on_salir_clicked();
  void on_aceptar_eliminar_clicked();
  void on_msgcamp_clicked();
  void setNombreCampania();
  void setDificultadCampania();
  void setNivelesCampania();
  void setImagenCampania();
  void setListaNiveles();
private:
  Gtk::Button *aplicar;
  Gtk::Button *agregar;
  Gtk::Button *volver;
  Gtk::Button *aceptar;
  Gtk::Button *salir;
  Gtk::Button *aceptarmsgcamp;
  Gtk::Button *cancelarmsgcamp;
  Gtk::Button *aceptareliminar;
  ColumnaCampania columna;
  Glib::RefPtr<Gtk::ListStore> listaNiveles;
  Gtk::TreeView* treeniveles;
  Gtk::MessageDialog *msgCampaniaInvalida;
  Gtk::Image *imagencamp;
  Glib::RefPtr<Gtk::Builder> refBuilder;
  ImagenBase64 codificadorimagen;
};
//////////////////////////////////////////////////
class VentanaEditar: public Ventana{
protected:
 bool verificarFileName(std::string filename);
public:  
  VentanaEditar();
  void on_aceptar_clicked();
  void on_cancelar_clicked();
  void set_button_signals();
private:
  Gtk::Button *aceptar;
  Gtk::Button *cancelar; 
  Gtk::FileChooserDialog *filedialog;
  Glib::RefPtr<Gtk::Builder> refBuilder;
};

//////////////////////////////////////////////////
class Mapa:public Ventana{
	public:
	 Mapa(Campania* camp);
	void on_listo_clicked();
	void on_salir_clicked();
	void on_aplicar_clicked();
	void on_aceptar_cancelar_bomber_clicked();
	void on_aceptar_cancelar_zombie_clicked();
	void set_buttons_signals();
	void configurarToolbox();
	void configurarMapa();
	bool verificarMatrizDeNivel();
	Gtk::Window* get_window();
        protected:
        Gtk::ToolItemGroup* agregarToolBoxToolItemGroup(std::string title);
	void agregarToolButtonAToolGroup(std::string imagepath, Glib::ustring label,Gtk::ToolItemGroup *grupo);
        private:
	int dificultad;
	DibujoMapa *levelview;
	Gtk::Viewport *port;
	Gtk::ToolPalette *toolbox;
	//boton listo
	Gtk::Button *listo;
	//botones seleccion dificultad.
	Gtk::Button *salir;
	Gtk::Button *aplicar;
	Gtk::Button *aceptarbomber;
	Gtk::Button *aceptarzombie;
	Gtk::SpinButton *alto,*ancho;
	Glib::RefPtr< Gdk::Pixbuf >  imagenNivel;
	Gtk::MessageDialog *msgbomberman;
	Gtk::MessageDialog *msgzombies;
	
        bool guardarImagen;
       ///**matriz numerica de tiles**///
	LevelGrid *matrizdenivel;	
	Glib::RefPtr<Gtk::Builder> refBuilder;
};

#endif 














































