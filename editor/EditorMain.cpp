#include "ManejadorVentanas.h"
#include <iostream>
#define CAMPANIA1 3
#define MAPA 4

int main(int argc, char *argv[])
{ 
  Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "com.gtkmm.tutorial0.base");
  Ventana *ventana,*usada;
  ManejadorVentanas manejador;
  ventana= manejador.ObtenerSiguiente(0);
  app->run(*(ventana->get_window()));
  std::stringstream ss;
  int i=1;
  while((ventana!=NULL)&&(ventana->siguiente()!=-1)){
 
    if((ventana->siguiente()==MAPA)||(ventana->siguiente()==CAMPANIA1)){
    usada=ventana;
    ventana=manejador.ObtenerSiguiente(ventana->siguiente(),ventana->getCampania());
    delete usada;
    }else{ 
      usada=ventana;
      ventana=manejador.ObtenerSiguiente(ventana->siguiente());
      delete usada;
    }
     ss<<i;
     Glib::RefPtr<Gtk::Application> app1 = Gtk::Application::create(argc, argv, "com.gtkmm.tutorial"+ ss.str()+".base");
       
    app1->run(*(ventana->get_window()));
 
     i++;  
    
    }

    delete ventana;

   return 0;
}