#include "operation.h"
#include <server/network/gameClient.h>

void Operation::execute(GameClient*){
	printf("Se intentó ejecutar una operación no soportada\n");
	throw "Operation not supported";
}

Operation::~Operation(){}
