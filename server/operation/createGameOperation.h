#ifndef CREATE_QUEUE_OPERATION
#define CREATE_QUEUE_OPERATION
#include "operation.h"
#include <string>

class GameClient;
class CreateGameOperation : public Operation{
public:
	explicit CreateGameOperation(int campaignId,std::string &nombre,int cant,std::string &name);
	virtual void execute(GameClient* client);
private:
	int campaignId;
	std::string player_name;
	std::string gameName;
	int maxPlayers;
};
#endif
