#ifndef START_GAME_OPERATION
#define START_GAME_OPERATION
#include "operation.h"

class StartGameOperation : public Operation{
public:
	explicit StartGameOperation();
	virtual void execute(GameClient* game);
};
#endif
