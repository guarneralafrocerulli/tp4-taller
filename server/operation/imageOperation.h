#ifndef IMAGE_OPERATION_H
#define IMAGE_OPERATION_H
#include "operation.h"

class ImageOperation : public Operation{
public:
	explicit ImageOperation(int img);
	virtual void execute(GameClient* client);
private:
	int img;
};

#endif