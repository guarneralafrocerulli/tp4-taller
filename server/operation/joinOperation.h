#ifndef JOIN_OPERATION
#define JOIN_OPERATION
#include "operation.h"
#include <string>

class GameClient;

class JoinOperation : public Operation{
public:
	/**
	 * Le paso el id de la lista a la que se quiere unir.
	 */
	explicit JoinOperation(int gameId,std::string &name);
	virtual void execute(GameClient* client);
private:
	std::string player_name;
	int gameId;
};
#endif
