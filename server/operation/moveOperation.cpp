#include "moveOperation.h"
#include <server/game/gameThread.h>

MoveOperation::MoveOperation(Direction direction){
	this->direction = direction;
}

void MoveOperation::execute(GameClient* client){
	client->getGame()->getGameCore()->movePlayer(client,  this->direction);
}
