#include "startGameOperation.h"
#include <server/network/gameClient.h>

StartGameOperation::StartGameOperation(){
}

void StartGameOperation::execute(GameClient* client){
	client->setReady(true);
    client->startGame();
}
