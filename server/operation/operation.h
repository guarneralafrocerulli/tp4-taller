#ifndef OPERATION_H
#define OPERATION_H
#include <string>
class GameClient;

class Operation{
public:
	virtual void execute(GameClient* game);
	virtual ~Operation();
};
#endif
