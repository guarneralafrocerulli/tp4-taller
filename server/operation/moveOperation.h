#ifndef MOVE_OPERATION
#define MOVE_OPERATION
#include "operation.h"
#include <server/game/placeable/direction.h>

class GameClient;
class MoveOperation : public Operation{
public:
	/**
	 * Le paso el cliente y el id de la lista a la que se quiere unir.
	 */
	explicit MoveOperation(Direction direction);
	virtual void execute(GameClient* game);
private:
	Direction direction;
};
#endif
