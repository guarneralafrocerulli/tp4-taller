#include "disconnectOperation.h"
#include <server/network/gameClient.h>

void DisconnectOperation::execute(GameClient* client){
    client->exit();
}
