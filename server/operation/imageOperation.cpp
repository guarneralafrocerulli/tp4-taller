#include "imageOperation.h"
#include <server/network/gameClient.h>
#include <server/serverThread.h>
#include <iostream>
ImageOperation::ImageOperation(int img){
	this->img=img;
}

void ImageOperation::execute(GameClient* client){
	ImagenCampania* imagen= client->getServer()->getImage(img);
	SerializadorEvento ser;
	EventoServerImagen* event=new EventoServerImagen(imagen);
	std::string eventStr = event->serializar(&ser);
	std::cerr<<eventStr<<std::endl;
	client->send(eventStr);
	delete event;
}