#ifndef OPERATION_FACTORY_H
#define OPERATION_FACTORY_H
#include <string>
#include <common/eventos.h>
#include <common/Serializadores.h>
class Operation;
using std::string;
class OperationFactory{
public:
	static Operation* getOperation(string &evento);
private:
	static Operation* getOperationClient(string &evento);
};
#endif
