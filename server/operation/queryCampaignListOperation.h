#ifndef QUERY_CAMPAIGN_LIST_OPERATION
#define QUERY_CAMPAIGN_LIST_OPERATION
#include "operation.h"

class QueryCampaignListOperation : public Operation{
public:
	virtual void execute(GameClient* client);
};
#endif
