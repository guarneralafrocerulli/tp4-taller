#ifndef QUERY_GAME_LIST_OPERATION
#define QUERY_GAME_LIST_OPERATION
#include "operation.h"

class QueryGameListOperation : public Operation{
public:
	virtual void execute(GameClient* client);
};
#endif
