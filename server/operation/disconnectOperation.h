#ifndef DISCONNECT_OPERATION_H
#define DISCONNECT_OPERATION_H
#include "operation.h"

class DisconnectOperation : public Operation{
public:
	virtual void execute(GameClient* game);
};
#endif
