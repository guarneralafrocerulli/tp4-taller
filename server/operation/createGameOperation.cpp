#include "createGameOperation.h"
#include <server/network/gameClient.h>

CreateGameOperation::CreateGameOperation(int campaignId,std::string &nombre,int cant,std::string &name){
	this->campaignId = campaignId;
	this->gameName=nombre;
	this->maxPlayers=cant;
	player_name=name;
}

void CreateGameOperation::execute(GameClient* client){
	client->createGame(this->campaignId, this->gameName, this->maxPlayers);
	client->setPlayerName(player_name);
}
