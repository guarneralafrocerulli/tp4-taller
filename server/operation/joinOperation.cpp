#include "joinOperation.h"
#include <server/network/gameClient.h>

JoinOperation::JoinOperation(int gameId,std::string &name){
	this->gameId = gameId;
	this->player_name=name;
}

void JoinOperation::execute(GameClient* client){
	client->setPlayerName(player_name);
	client->joinGame(gameId);
}
