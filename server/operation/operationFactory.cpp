#include "operationFactory.h"
#include "joinOperation.h"
#include "moveOperation.h"
#include "bombOperation.h"
#include "createGameOperation.h"
#include "startGameOperation.h"
#include "queryCampaignListOperation.h"
#include "queryGameListOperation.h"
#include "disconnectOperation.h"
#include "imageOperation.h"
#include <cstdio>
#include <string>
#include <sstream>

using std::stringstream;

class Operation;

Operation* OperationFactory:: getOperation(string &evento){
	string tipo;
	Operation* op=NULL;
	stringstream ss;
	TiXmlDocument xml("evento");
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	if(root->ValueStr()=="EventoCliente"){
		op=OperationFactory::getOperationClient(evento);
	}else if(root->ValueStr()=="EventoDesconectar"){
		op = new DisconnectOperation();
	}
	else{
		tipo=root->Attribute("tipo");
		if(tipo=="RIGHT"){
			op=new MoveOperation(RIGHT);
		} else if(tipo=="LEFT"){
			op=new MoveOperation(LEFT);
		} else if(tipo=="UP"){
			op=new MoveOperation(UP);
		} else if(tipo=="DOWN"){
			op=new MoveOperation(DOWN);
		} else if(tipo=="BOMB"){
			op=new BombOperation();
		}
	}
   return op;
}

Operation* OperationFactory::getOperationClient(string &evento){
	TiXmlDocument xml("evento");
	SerializadorEvento ser;
	Operation* op=NULL;
	xml.Parse((const char*)evento.c_str(),0,TIXML_ENCODING_UTF8);
	TiXmlElement* root=xml.FirstChildElement();
	TiXmlElement* even=root->FirstChildElement();
	if(even->ValueStr()=="PedirLista"){
		EventoClientePedirLista* plista = ser.deserializarPedirLista(evento);
		string tipo = even->Attribute("tipo");
		if (tipo == CAMPANIA){
			op = new QueryCampaignListOperation();
		} else if (tipo == PARTIDA){
			op = new QueryGameListOperation();
		}
		delete plista;
	}
	if(even->ValueStr()=="EventoClienteCrearPartida"){
		EventoClienteCrearPartida* eCrear;
		eCrear = ser.deserializarClienteCrearPartida(evento);
		std::string nombrePartida(eCrear->getNombrePartida());
		std::string nombreJugador(eCrear->getNombreJugador());
		op = new CreateGameOperation(eCrear->getId(), nombrePartida, eCrear->getCantJugadores(), nombreJugador);
		delete eCrear;
	}
	if(even->ValueStr()=="EventoClienteUnirsePartida"){
		EventoClienteUnirsePartida* eUnirse;
		eUnirse = ser.deserializarUnirsePartida(evento);
		std::string nombreJugador(eUnirse->getNombreJugador());
		op = new JoinOperation(eUnirse->getId(), nombreJugador);
		delete eUnirse;
	}
	if(even->ValueStr()=="EventoClienteEmpezarPartida"){
		EventoClienteEmpezarPartida*eEmpezar;
		eEmpezar = ser.deserializarClienteEmpezarPartida(evento);
		op = new StartGameOperation();
		delete eEmpezar;
	}
	if(even->ValueStr()=="EventoClientePedirImagen"){
		EventoClientePedirImagen* plista = ser.deserializarPedirImagen(evento);
		op=new ImageOperation(plista->getId());
		delete plista;
	}
	return op;
}
