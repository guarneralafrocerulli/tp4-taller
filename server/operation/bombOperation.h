#ifndef BOMB_OPERATION
#define BOMB_OPERATION
#include "operation.h"
#include <server/game/placeable/direction.h>

class GameClient;
class BombOperation : public Operation{
public:
	virtual void execute(GameClient* client);
};
#endif
