#include "queryGameListOperation.h"
#include <server/network/gameClient.h>
#include <server/serverThread.h>
#include <vector>
#include <string>

void QueryGameListOperation::execute(GameClient* client){
	std::vector<PartidaCampania> list = client->getServer()->getGameList();
	EventoServerLista event;
	SerializadorEvento ser;
	for (unsigned int i = 0; i < list.size(); ++i){
		event.agregar(&list.at(i));
	}
	std::string eventStr = event.serializar(&ser);
	client->send(eventStr);
}
