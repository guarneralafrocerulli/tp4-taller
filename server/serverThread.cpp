#include "serverThread.h"
#include "network/clientList.h"
#include <vector>
#include <string>
#include <tinyxml.h>

ServerThread::ServerThread(std::string &configFilename){
	TiXmlDocument config(configFilename);
	if(!config.LoadFile()){
		printf("Cargadas opciones default");
		loadDefaultConfig();
	} else {
		printf("Cargado archivo %s\n", configFilename.c_str());
		loadConfig(config);
	}
	printf("Cargando campañas en %s\n", this->campaignDir.c_str());
	campaignLoader.loadCampaigns(this->campaignDir);
	this->exit = false;
}

ServerThread::~ServerThread(){
}

void ServerThread::runFunction(){
	listener.start(port);
	while(!this->exit){
		Socket* clientSocket = 0;
		try{
			clientSocket = listener.acceptClient();
			/**
			 * Una vez que se aceptó un socket
			 * limpio los clientes desconectados
			 */
			this->clientList.removeDisconnected();
			printf("[INFO] Se conectó un nuevo cliente: %s : %d\n",
				   clientSocket->getAddress().c_str(), clientSocket->getFD());
			GameClient* client = new GameClient(clientSocket, *this);
			bool added = this->clientList.addClient(client);
			if (!added){ 
				//Máxima cantidad de conexiones alcanzadas
				delete client;
			}
		}catch(char const *e){
			printf("[ERROR]: Se produjo un error al escuchar clientes - %s\n", e);
		}
	}
}

std::vector<PartidaCampania> ServerThread::getCampaignList(){
	return this->campaignLoader.getCampaignInfo();
}

std::vector<PartidaCampania> ServerThread::getGameList(){
	return this->gameList.getGameInfoList();
}

int ServerThread::createGame(int campaignId, std::string &campaignName, int maxPlayers){
	Campania campaign = campaignLoader.getCampaign(campaignId);
	return this->gameList.createGame(campaign, campaignName, maxPlayers);
}

void ServerThread::joinGame(GameClient* client, int gameId, bool sendEvent){
	bool joined = this->gameList.join(client, gameId);
	if (sendEvent){
		if(joined){
			printf("Envío que se unió a la partida\n");
		}else{
			printf("Envío que NO se unió a la partida\n");
		}
		EventoServerUnirPartida event(joined);
		SerializadorEvento ser;
		std::string strEvent(event.serializar(&ser));
		client->send(strEvent);
	}
}

void ServerThread::startGame(int gameId){
	this->gameList.startGame(gameId);
}

void ServerThread::destroyGame(GameThread* game){
	gameList.destroy(game);
}

ImagenCampania* ServerThread::getImage(int img){
	return campaignLoader.getImage(img);
}

void ServerThread::stop(){
	printf("Stop called!\n");
	this->exit = true;
	this->gameList.stopGames();
	this->clientList.deleteClients();
	this->listener.shutdownSocket();
	this->listener.closeSocket();
}

void ServerThread::loadDefaultConfig(){
	this->port = 8080;
	this->clientList.setMaxPlayers(1000);
}

void ServerThread::loadConfig(TiXmlDocument &config){
	this->clientList.setMaxPlayers(1000);
	TiXmlElement* root = config.FirstChildElement();
	TiXmlElement* campaignDir = root->FirstChildElement();
	TiXmlElement* port = campaignDir->NextSiblingElement();
	this->campaignDir =campaignDir->Attribute("dir");
	this->port = atoi(port->Attribute("port"));
}
