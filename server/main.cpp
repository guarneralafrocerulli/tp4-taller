#include <iostream>
#include <string>
#include <unistd.h>
#include "config.h"
#include "serverThread.h"
#include <common/logger.h>
#define CONFIG_FILENAME _SERVER_PATH_CONFIGURE_"config.xml"
int main(int , char** ){
	std::string text;
	bool exit = false;
	std::string configFilename(CONFIG_FILENAME);
	std::cout << "Archivo de configuraciones: "<< configFilename << std::endl;
	ServerThread server(configFilename);
	try{
		server.run();
		while (!exit){
			std::cin >> text;
			exit = (text.compare("salir") == 0);
		}
		server.stop();
	} catch(char const *e) {
		Logger::getInstance()->log(e);
	} catch (...){
		Logger::getInstance()->log("Excepción desconocida");
	}
	std::cout << "Apagando" << std::endl;
	server.join();
	Logger::getInstance()->destroy();
}
