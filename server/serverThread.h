#ifndef SERVER_THREAD_H
#define SERVER_THREAD_H
#include <vector>
#include <string>
#include <common/thread.h>
#include "network/serverSocket.h"
#include "network/clientList.h"
#include "game/campaignLoader.h"
#include "game/gameList.h"

class TiXmlDocument;

class ServerThread : public Thread{
public:
	explicit ServerThread(std::string &configFilename);
	/**
	 * Devuelvo la lista de campañas que se pueden crear
	 */
	std::vector<PartidaCampania> getCampaignList();
	/**
	 * Devuelvo la lista de juegos abiertos
	 */
	std::vector<PartidaCampania> getGameList();
	void runFunction();
	virtual ~ServerThread();
	/**
	 * Detengo todos los juegos y cierro todos los sockets
	 */
	void stop();
	/**
	 * Creo un juego y devuelvo su Id
	 */
	int createGame(int campaignId, std::string &campaignName, int maxPlayers);
	void joinGame(GameClient* client, int gameId, bool sendEvent);
	void startGame(int gameId);
	/**
	 * Pre: el juego ya terminó
	 */
	void destroyGame(GameThread* game);
	ImagenCampania* getImage(int img);

private:
	ServerSocket listener;
	CampaignLoader campaignLoader;
	ClientList clientList;
	GameList gameList;
	int port;
	bool exit;
	std::string campaignDir;
	void loadDefaultConfig();
	void loadConfig(TiXmlDocument &config);
};

#endif
