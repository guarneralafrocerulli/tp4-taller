#include "campaignLoader.h"
#include <dirent.h>
#include <vector>
#include <string>

/**
 * Chequeo la extensión, es bastante feo consideranco
 * que me puede levantar cualquier xml, como el config.xml
 */
bool isCampaignFile(std::string &fileName){
	if (fileName.size() < 5){
		return false;
	}
	if (fileName.compare("config.xml") == 0) return false;
	return (fileName.substr(fileName.size()-4, 4).compare(".xml") == 0);
}

CampaignLoader::~CampaignLoader(){
	for (unsigned int i = 0; i < this->picture.size(); ++i){
		delete this->picture[i];
	}
}

void CampaignLoader::loadCampaigns(string &campDir){
	DIR* directory = opendir(campDir.c_str());
	struct dirent *entry;
	int indice=0;
	do{
		entry = readdir(directory);
		if (entry){
			std::string fileName(entry->d_name);
			if (isCampaignFile(fileName)){
				Campania campania(campDir + fileName);
					if(!campania.getError()){
						ImagenCampania* pic=new ImagenCampania(campania.getInfoImagen());
						PartidaCampania campInfo = campania.getPartidaCampania();
						campInfo.setId(indice);
						printf("Cargando campaña %d: %s(%s) cant niveles:%d\n",
							(int) this->campaignInfo.size(), fileName.c_str(), campInfo.getNombre().c_str(),campania.getCantNiveles());
						this->picture.push_back(pic);
						this->campaignList.push_back(campania);
						this->campaignInfo.push_back(campInfo);
						indice++;
					}
			}	
		}
	} while (entry != NULL);
	closedir(directory);
}

Campania CampaignLoader::getCampaign(int id){
	return this->campaignList.at(id);
}

std::vector<PartidaCampania> CampaignLoader::getCampaignInfo(){
	return this->campaignInfo;
}

ImagenCampania* CampaignLoader::getImage(int img){
	return picture[img];
}
