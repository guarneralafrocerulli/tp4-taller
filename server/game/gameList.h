#ifndef GAMELIST_H
#define GAMELIST_H
#include "gameThread.h"
#include <common/partidasCampanias.h>
#include <map>

class GameList{
public:
	GameList();
	int createGame(Campania &campaign, std::string &campaignName, int maxPlayers);
	void stopGames();
	/**
	 * Une al cliente en la partida 
	 */
	bool join(GameClient* client, unsigned int gameId);
	bool startGame(unsigned int gameId);
	std::vector<PartidaCampania> getGameInfoList();
	void destroy(GameThread* game);
private:
	std::map<unsigned int, GameThread*> gameList;
	unsigned int currentId;
	/**
	 * Nombres, cantidad de jugadores y demás información de la partida
	 */
	std::vector<PartidaCampania> gameInfoList;
	Mutex mutex;
	void deleteInfoById(int gameId);
};

#endif
