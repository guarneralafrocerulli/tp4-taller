#include "gameCore.h"
#include <common/eventos.h>
#include <common/tiposEventoNuevoElemento.h>
#include "placeable/placeable.h"
#include "placeable/bomberman.h"
#include "placeable/bombermanBomb.h"
#include "placeable/placeableFactory.h"
#include <cstdio>
#include <vector>

GameCore::GameCore(Campania& campaign) : campaign(campaign){
	endgame=false;
}

void GameCore::startCampaign(ClientList &clientList){
	this->currentLevel = 0;
	this->clientList = clientList;
	loadMap();
	//Envío el mapa
	sendMap();
	//Agrego bombermans por cada jugador
	addBombermans();
	//Flusheo la cola de eventos, para enviar los bombermans
	sendUpdates();
	//Le digo al cliente que el bomberman recién creado es suyo
	assignBombermans();
}

EventList& GameCore::getEventList(){
	return this->eventList;
}

PlaceableList& GameCore::getPlaceableList(){
	return this->placeableList;
}

void GameCore::movePlayer(GameClient* client, Direction direction){
	int playerId = clientPlaceableMap[client->getId()];
	Placeable* player = this->placeableList.getPlaceableById(playerId);
	player->move(direction, playerSpeed);
}

void GameCore::addBomb(GameClient* client){
	int playerId=clientPlaceableMap[client->getId()];
	Bomberman* player = (Bomberman*)this->placeableList.getPlaceableById(playerId);
	if(this->placeableList.getPlaceableById(playerId)->isActive()&&player->canCreateBomb()){
		player->resetTimeToBomb();
		Coordinate coord=player->getPosition();
		BombermanBomb* placeableBomb = new BombermanBomb(coord, *this);
		int id=placeableList.addPlaceable(placeableBomb);
		placeableBomb->setOwner(playerId);
		EventoNuevoElemento* evento=new EventoNuevoElemento(BOMBAB, id, coord);
		eventList.addEvent(evento);
	}
}


void GameCore::tick(){
	placeableList.tick();
	updateClientsLives();
	//updatePunctuation();
	nextLevel();
	//std::cerr<<"CantZombies"<<placeableList.zombies<<std::endl;
}

bool GameCore::noMoreLives(){
	/*devuelve true si no le quedan
	 * vidas a ningun jugador*/
	map<int, int>::iterator it;
	for(it=clientPlaceableMap.begin();it!=clientPlaceableMap.end();++it){
		GameClient* client=clientList.getClientById((*it).first);
		if(client->getLives()>=0) return false;
	}
	return true;
}

void GameCore::sendUpdates(){
	if (!hasEnded()){
		this->clientList.sendUpdates(eventList);
	}
}

/**
 * Privadas
 */
void GameCore::loadMap(){
	/**
	 * Convierto los elementos del mapa en placeables
	 */
	playerSpawn.clear();
	PlaceableFactory factory(*this);
	LevelGrid* levelGrid = this->campaign.getNivel(this->currentLevel);
	unsigned int alto = levelGrid->getAlto();
	unsigned int ancho = levelGrid->getAncho();
	printf("Cargando mapa de alto: %d, ancho %d\n", alto, ancho);
	for (unsigned int y = 0; y < alto; ++y){
		printf("{|");
		Placeable* placea = factory.createPlaceable(5, -1, y);
			//Las tierras me las devuelve como null
		if (placea){
			this->placeableList.addPlaceable(placea);
		}
		placea = factory.createPlaceable(5, ancho, y);
			//Las tierras me las devuelve como null
		if (placea){
			this->placeableList.addPlaceable(placea);
		}
		for (unsigned int x = 0; x < ancho; ++x){
			placea = factory.createPlaceable(5, x, -1);
			//Las tierras me las devuelve como null
			if (placea){
				this->placeableList.addPlaceable(placea);
			}
			placea = factory.createPlaceable(5, x, alto);
				//Las tierras me las devuelve como null
			if (placea){
				this->placeableList.addPlaceable(placea);
			}
			// 'Y' y 'X' están invertidos en el levelGrid... sin palabras.
			int data = levelGrid->getData(y, x);//JAJAJJA
			printf("%d", data);
			if(data==1){// guardo las posiciones de los spawn de bomberman
				Coordinate newSpawn(x*GRID_WIDTH, y*GRID_HEIGHT);
				playerSpawn.push_back(newSpawn);
			}
			Placeable* placeable = factory.createPlaceable(data, x, y);
			//Las tierras me las devuelve como null
			if (placeable)
				this->placeableList.addPlaceable(placeable);
		}
		printf("|}\n");
	}
}
void GameCore::resetLives(){
	/*todos los jugadores tienen 3 vidas
	 * y se envian los eventos,modificar visibilidad
	 eventoPosicion*/
	clientList.resetLives();
	std::map<int, int>::iterator it;
	for(it=clientPlaceableMap.begin();it!=clientPlaceableMap.end();++it){
		EventoModificarCantidadVidas* evento=new EventoModificarCantidadVidas((*it).second,3);
		EventoModificarVisibilidad* vis=new EventoModificarVisibilidad((*it).second,true);
		int pos=rand()%playerSpawn.size();
		EventoPosicion* epos=new EventoPosicion((*it).second,playerSpawn[pos].x,playerSpawn[pos].y);
		Placeable* bomberman=placeableList.getPlaceableById((*it).second);
		bomberman->setPosition(playerSpawn[pos]);
		eventList.addEvent(epos);
		eventList.addEvent(evento);
		eventList.addEvent(vis);
		
	}
}

void GameCore::sendMap(){
	EventoNuevoMapa* even=new EventoNuevoMapa(
		campaign.getNivel(this->currentLevel)->getGrilla());
	eventList.addEvent(even);
}
/*
void GameCore::updatePunctuation(){
	std::vector<Placeable*> placeables=this->placeableList.getPlaceableList();
	vector<Placeable*>::iterator placeable;
	for (placeable=placeables.begin();placeable!=placeables.end();++placeable){
		if(!(*placeable)->isActive()){
			if((*placeable)->getKilledBy()!=-1){//si lo mato algo q no sea planta
				std::map<int,int>::iterator clients;
				int clientId;
				for (clients=clientPlaceableMap.begin();clients!=clientPlaceableMap.end();++clients){
					if ((*clients).second==(*placeable)->getKilledBy()){
						(clientList.getClientById((*clients).first))->addPoints((*placeable)->getPoints());
						EventoModificarPuntuacion* evento=new EventoModificarPuntuacion((*placeable)->getKilledBy(),clientList.getClientById((*clients).first)->getPoints());
						eventList.addEvent(evento);
						(*placeable)->setKilledBy(-1);
					}
			}
		}
	}
	}
}*/
void GameCore::addBombermans(){
	std::vector<GameClient*> clients = this->clientList.getGameClientList();
	srand(time(NULL));
	for (unsigned int i = 0; i < clients.size(); ++i){
		GameClient* client = clients[i];
		int pos=rand()%playerSpawn.size();
		//Agrego un objeto tipo Bomberman al escenario
		std::cerr<<"x"<<playerSpawn[pos].x<<"Y"<<playerSpawn[pos].y<<std::endl;
		Placeable* placeablePlayer = new Bomberman(playerSpawn[pos], *this);
		int bombermanId = placeableList.addPlaceable(placeablePlayer);
		//int idzomb= placeableList.addPlaceable(zombie);
		clientPlaceableMap[client->getId()] = bombermanId;
	}
}

void GameCore::assignBombermans(){
	std::vector<GameClient*> clients = this->clientList.getGameClientList();
	for (unsigned int i = 0; i < clients.size(); ++i){
		GameClient* client = clients[i];
		int bombermanId = clientPlaceableMap[client->getId()];
		client->sendInitData(bombermanId);
	}
}

void GameCore::nextLevel(){
	/*cambia de nivel si es necesario
	 * envia evento fin del juego si se terminarn=on
	 * los niveles*/
	if(placeableList.getNumberOfZombies()==0){
		if((currentLevel) < campaign.getCantNiveles()-1){
			placeableList.clean();
			currentLevel++;
			loadMap();
			//Envío el mapa
			sendMap();
			///hago visibles los bomberman y los cambio de lugar
			sendUpdates();
			resetLives();
		}
		/* termino el juego si la cant de zombies es 0 y no hay siguiente
		 * nivel, mandar evento fin del juego*/
		else{
			/*envio puntuacion y cierro sockets*/
			sendUpdates();
			endGame();//no se envian mas updates
		}
	}
	/*aca verificar si no quedan mas vidas de los bomberman
	 * y si no quedan q currentLevel sea la mitad del nivel 
	 * en el q estan*/
	else if(noMoreLives()){
		placeableList.clean();
		currentLevel=currentLevel/2;
		loadMap();
			//Envío el mapa
		sendMap();
			///hago visibles los bomberman y los cambio de lugar
		resetLives();
		sendUpdates();
	}
}

void GameCore::endGame(){
	this->endgame = true;
	clientList.closeClients();
}

void GameCore::updateClientsLives(){
	std::map<int, int>::iterator it;
	for(it=clientPlaceableMap.begin();it!=clientPlaceableMap.end();++it){
		if (!placeableList.getPlaceableById((*it).second)->isActive()){
			Placeable* bomb=placeableList.getPlaceableById((*it).second);
			GameClient* client=clientList.getClientById((*it).first);
			if(client->getLives()>=0){
				client->subLive();
				bomb->activate();
				EventoModificarCantidadVidas* evento=new EventoModificarCantidadVidas((*it).second,client->getLives());
				EventoModificarVisibilidad* vis=new EventoModificarVisibilidad((*it).second,true);
				eventList.addEvent(evento);
				eventList.addEvent(vis);
				int pos=rand()%playerSpawn.size();
				Coordinate cor=playerSpawn[pos];
				EventoPosicion* Epos=new EventoPosicion((*it).second,cor.x,cor.y);
				eventList.addEvent(Epos);
				bomb->setPosition(cor);
				bomb->activate();
			}
		}
	}
}