#ifndef GAME_CORE_H
#define GAME_CORE_H
#include "common/coordinate.h"
#include <vector>
#include <map>
#include <common/eventList.h>
#include <common/Campania.h>
#include <server/network/clientList.h>
#include "placeable/placeableList.h"

class GameCore{
public:
	explicit GameCore(Campania &campaign);
	void addBomb(GameClient* client);
	void tick();
	/**
	 * devuelve true si todos
	 * los jugadores se quedaron sin
	 * vidas
	 */
	void endGame();
	bool hasEnded(){return endgame;}
	bool noMoreLives();
	PlaceableList& getPlaceableList();
	EventList& getEventList();
	ClientList& getClientList(){return clientList;}
	std::map<int, int> getClientPlaceableMap(){return clientPlaceableMap;}
	/**
	 * Muevo el bomberman del cliente que me manda la acción.
	 */
	void movePlayer(GameClient* client, Direction direction);
	void sendUpdates();
	void startCampaign(ClientList &clientList);
private:
	/**
	 * Convierto el Levelgrid a objetos Placeables
	 */
	void loadMap();
	void sendMap();
	/**
	 * Agrego bombermans por jugador
	 */
	void addBombermans();
	/**
	 * A cada jugador le digo el id de su bomberman
	 */
	void assignBombermans();
	/**
	 * Mapa para saber qué ubicable le toca a cada jugador según
	 * el id del GameClient (o sea, según el fd del socket)
	 */
	void updateClientsLives();
	void updatePunctuation();
	void resetLives();
	void nextLevel();
	std::map<int, int> clientPlaceableMap;
	Campania campaign;
	PlaceableList placeableList;
	ClientList clientList;
	EventList eventList;
	bool endgame;
	int currentLevel;
	static const unsigned int playerSpeed = 8;
	vector<Coordinate> playerSpawn; //los spawns de los bomermans
};

#endif
