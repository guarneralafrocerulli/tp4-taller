#include "gameList.h"

GameList::GameList(){
	this->currentId = 0;
}

int GameList::createGame(Campania& campaign, string &campaignName, int maxPlayers){
	Lock lock(mutex);
	GameThread* game = new GameThread(campaign, campaignName, maxPlayers, currentId);
	PartidaCampania gameInfo = game->getGameInfo();
	gameList[currentId] = game;
	this->gameInfoList.push_back(gameInfo);
	return currentId++;
}

void GameList::stopGames(){
	Lock lock(mutex);
	std::map<unsigned int, GameThread*>::iterator it;
	for (it = gameList.begin(); it != gameList.end(); ++it){
		GameThread* game = it->second;
		game->stop();
		game->join();
		delete game;
	}
	gameList.empty();
}

bool GameList::join(GameClient* client, unsigned int gameId){
	Lock lock(mutex);
	printf("Uniendo al jugador %s al juego %d\n",
			client->getName().c_str(), gameId);
	GameThread* game = this->gameList.find(gameId)->second;
	if (game != NULL){
		return game->addClient(client);
	} else {
		return false;
	}
}

bool GameList::startGame(unsigned int gameId){
	Lock lock(mutex);
	GameThread* game = this->gameList.find(gameId)->second;
	bool ready = game->clientsReady();
	if (ready){
		printf("Clientes listos, arrancando la partida!\n");
		game->startGame();
		deleteInfoById(gameId);
	}
	EventoServerEmpezarPartida evento(ready);
	SerializadorEvento s;
	printf("[DEBUG] Enviando %s al los clientes del juego %d\n", evento.serializar(&s).c_str(), gameId);
	std::string strEvento = evento.serializar(&s);
	game->broadcast(strEvento);
	return ready;
}

void GameList::destroy(GameThread* game){
	printf("Destruyendo el juego %s\n", game->getGameInfo().getNombre().c_str());
	game->join();
	int id = game->getGameInfo().getId();
	delete this->gameList[id];
	this->gameList.erase(id);
}

void GameList::deleteInfoById(int gameId){
	std::vector<PartidaCampania>::iterator it;
	for (it = this->gameInfoList.begin(); it != this->gameInfoList.end(); ++it){
		if (it->getId() == gameId){
			this->gameInfoList.erase(it);
			return;
		}
	}
}

std::vector<PartidaCampania> GameList::getGameInfoList(){
	return this->gameInfoList;
}
