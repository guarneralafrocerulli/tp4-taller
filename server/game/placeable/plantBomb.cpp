#include "plantBomb.h"
#include "box.h"
#include "zombie.h"
#include "bomberman.h"
#include <server/game/gameCore.h>
PlantBomb::PlantBomb(Coordinate& coordinate, GameCore& gameCore,
				int speed, Direction dir):
	Bomb(coordinate, gameCore){
		this->speed=speed;
		this->direction=dir;
		crash=false;
	}
	
void PlantBomb::sendCreateEvent(){
	EventoNuevoElemento* event = new EventoNuevoElemento(BOMBAP,
			this->id, this->position.x, this->position.y);
	this->gameCore.getEventList().addEvent(event);
}

bool PlantBomb::collide(Direction, int, Box* box){
	if( isColliding(box)) crash=true;
	return crash;
}
bool PlantBomb::collide(Direction, int, Zombie* zomb){
	if( isColliding(zomb)) crash=true;
	return crash;
}
bool PlantBomb::collide(Direction, int, Bomberman* bomber){
	if( isColliding(bomber)) crash=true;
	return crash;
}

bool PlantBomb::collide(Direction direction, int speed, Placeable* placeable){
	return placeable->collide(direction,speed,this);
}

void PlantBomb::tick(){
	if(crash) explode();
	else{
		move(direction,speed);
	}
}