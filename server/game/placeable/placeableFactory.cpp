#include "placeableFactory.h"
#include "box.h"
#include "greenZombie.h"
#include "redZombie.h"
#include "blueZombie.h"
#include "plant.h"

PlaceableFactory::PlaceableFactory(GameCore& gameCore):
	gameCore(gameCore){
}

Placeable* PlaceableFactory::createPlaceable(int type, int x, int y){
	//TODO Sacar ifs, poner el resto de los placeables
	//TODO Esas constantes...
	//Type 4: caja de madera
	//Type 5: caja de acero
	Coordinate coord(x*GRID_WIDTH, y*GRID_HEIGHT);
	switch(type){
		case 1:
			break;
		case 2:
			return new GreenZombie(coord, gameCore);
		case 3:{
			WoodBox* box=new WoodBox(coord, gameCore);
			box->hideZombie(HIDE_RED);
			return box;
		}
		case 4:
			return new WoodBox(coord, gameCore);
		case 5:
			return new SteelBox(coord, gameCore);
		case 6:{
			WoodBox* box=new WoodBox(coord, gameCore);
			box->hideZombie(HIDE_GREEN);
			return box;
		}
		case 7:
			return new BlueZombie(coord, gameCore);	
		case 8:
			return new RedZombie(coord, gameCore);
		case 9:{
			WoodBox* box=new WoodBox(coord, gameCore);
			box->hideZombie(HIDE_BLUE);
			return box;
		}	
		case 11:
			return new Plant(GREENFREQ,UP,coord, gameCore);
		case 21:
			return new Plant(GREENFREQ,DOWN,coord, gameCore);
		case 31:
			return new Plant(GREENFREQ,LEFT,coord, gameCore);
		case 41:
			return new Plant(GREENFREQ,RIGHT,coord, gameCore);
		case 12:
			return new Plant(BLUEFREQ,UP,coord, gameCore);
		case 22:
			return new Plant(BLUEFREQ,DOWN,coord, gameCore);
		case 32:
			return new Plant(BLUEFREQ,LEFT,coord, gameCore);
		case 42:
			return new Plant(BLUEFREQ,RIGHT,coord, gameCore);
		case 13:
			return new Plant(REDFREQ,UP,coord, gameCore);
		case 23:
			return new Plant(REDFREQ,DOWN,coord, gameCore);
		case 33:
			return new Plant(REDFREQ,LEFT,coord, gameCore);
		case 43:
			return new Plant(REDFREQ,RIGHT,coord, gameCore);	
	}
	return 0;
}
