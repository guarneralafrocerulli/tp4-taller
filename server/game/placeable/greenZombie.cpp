#include "greenZombie.h"
#include <server/game/gameCore.h>
#include <math.h>
#define SPEED 10
#define HITPOINTS 1
#define DISTMAX 192
#include "common/eventos.h"
#include "common/tiposEventoNuevoElemento.h"


GreenZombie::GreenZombie(Coordinate& coordinate, GameCore& gameCore)
	: Zombie(coordinate, gameCore){
		speed=10;
		std::cerr<<"ZOMBIE VERDE CREADO"<<std::endl;
}

void GreenZombie::tick(){
	double aux=(position.x-inicial.x)*(position.x-inicial.x)
	+ (position.y-inicial.y)*(position.y-inicial.y);
	double distancia=sqrt(aux);
	if(distancia>DISTMAX){
		if (actual_direction==0) actual_direction=2;
		else if (actual_direction==1) actual_direction=3;
		else if (actual_direction==2) actual_direction=0;
		else if (actual_direction==3) actual_direction=1;
		inicial.x=position.x;
		inicial.y=position.y;
	}
	move(directions[actual_direction],speed);
}

void GreenZombie::sendCreateEvent(){
	EventoNuevoElemento *evento;
	evento = new EventoNuevoElemento(ZVERDE,id,position.x,position.y);
	this->gameCore.getEventList().addEvent(evento);
}
