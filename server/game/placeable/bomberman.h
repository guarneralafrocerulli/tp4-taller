#ifndef BOMBERMAN_H
#define BOMBERMAN_H
#include "placeable.h"
#include "zombie.h"

class GameCore;

class Bomberman : public Placeable{
private:
	int timeToBomb;
public:
	Bomberman(Coordinate& coordinate, GameCore& gameCore);
    virtual void sendCreateEvent();
    //bool collide(Direction dir,int speed,Box* box);
    void resetTimeToBomb();
    bool canCreateBomb();
    void tick();
    void kill();
    bool collide(Direction direction,int speed,Placeable* plac);
    bool collide(Direction direction,int speed,Zombie* zomb);
};
#endif
