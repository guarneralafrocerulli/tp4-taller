#ifndef RED_ZOMBIE_H
#define RED_ZOMBIE_H
#include "zombie.h"

class RedZombie:public Zombie{
public:
	RedZombie(Coordinate& coordinate, GameCore& gameCore);
	void sendCreateEvent();
	void tick();
	void changeDirection();
	void hit(int id);
	int getPoints(){return 30;}
	
private:
	int timeToChangeDirection;
};


#endif