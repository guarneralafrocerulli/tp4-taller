#ifndef PLANT_BOMB
#define PLANT_BOMB
#include "bomb.h"
class PlantBomb:public Bomb{
private:
	bool crash;///si choco, para q en el proximo tick explote
	int speed;
	Direction direction;
public:	
	PlantBomb(Coordinate& coordinate, GameCore& gameCore, int speed, Direction dir);
	void tick();
	bool collide(Direction direction, int speed, Placeable* placeable);
	bool collide(Direction direction, int speed, Bomberman* bomber);
	bool collide(Direction direction, int speed, Zombie* zombie);
	bool collide(Direction direction, int speed, Box* box);
	virtual void sendCreateEvent();
};

#endif