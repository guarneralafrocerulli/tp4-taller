#include "bomb.h"
#include "common/eventos.h"
#include <common/tiposEventoNuevoElemento.h>
#include <iostream>
#include "fire.h"
#include <server/game/gameCore.h>

Bomb::Bomb(Coordinate& coordinate, GameCore& gameCore):
	Placeable(coordinate, gameCore){
	owner=-1;
}


void Bomb::explode(){
	Coordinate cor(position.x,position.y);
	Fire * fire0=new Fire(cor, this->gameCore, owner, UP);
	Fire * fire1=new Fire(cor, this->gameCore, owner, DOWN);
	Fire * fire2=new Fire(cor, this->gameCore, owner, RIGHT);
	Fire * fire3=new Fire(cor, this->gameCore, owner, LEFT);
	this->gameCore.getPlaceableList().addPlaceable(fire0);
	this->gameCore.getPlaceableList().addPlaceable(fire1);
	this->gameCore.getPlaceableList().addPlaceable(fire2);
	this->gameCore.getPlaceableList().addPlaceable(fire3);
	kill();
}

