#ifndef BOX_H
#define BOX_H
#include "placeable.h"
#define HIDE_NONE -1
#define HIDE_GREEN 0
#define HIDE_RED 1
#define HIDE_BLUE 2

class GameCore;

class Box : public Placeable{
public:
	Box(Coordinate& coordinate, GameCore& gameCore);
	virtual void sendCreateEvent();
	virtual void hit(){};
	virtual bool collide(Direction direction, int speed, Placeable* placeable);
};

class SteelBox : public Box{
public:
	SteelBox(Coordinate& coordinate,
			 GameCore& gameCore);
};

class WoodBox : public Box{
public:
    WoodBox(Coordinate& coordinate, GameCore& gameCore);
	int isHiding(){return zombie;}
	void hideZombie(int type); 
	Zombie* obtainZombie();
	void sendCreateEvent();
	void hit();
private:
	bool crash;
	int zombie;
	void tick();
};
#endif
