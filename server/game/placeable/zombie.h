#ifndef ZOMBIE_H
#define ZOMBIE_H
#include "placeable.h"
#include "box.h"
#include "bomberman.h"
#include "bombermanBomb.h"
#include "fire.h"
#include "plantBomb.h"

class Bomberman;

class Zombie:public Placeable{
protected:
	Coordinate inicial;
	int killedBy;/// el duenio de la bomba q mato al zombie
	int speed;
	unsigned int hitPoints;
	int timeZ;
	Direction directions[4];
	int actual_direction;
public:
	Zombie(Coordinate& coordinate, GameCore& gameCore);
	virtual int getPoints(){return 0;}
	virtual void hit(int id);
	void kill();
	int getKilledBy(){return killedBy;}
	void setKilledBy(int id){killedBy=id;}
	bool collide(Direction direction, int speed, BombermanBomb* box);
	bool collide(Direction direction, int speed, Box* box);
	bool collide(Direction direction, int speed, Bomberman* bomberman);
	bool collide(Direction direction, int speed, Fire* fire);
	bool collide(Direction direction, int speed, Placeable* placeable);
};

#endif
