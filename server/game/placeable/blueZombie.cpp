#include "blueZombie.h"
#include <cstdlib>
#include <ctime>
#include <server/game/gameCore.h>

BlueZombie::BlueZombie(Coordinate& coordinate, GameCore& gameCore)
	: Zombie(coordinate, gameCore){
		speed=20;
		timeZ=0;
		timeToChangeDirection=100;
}
void BlueZombie::tick(){
	if(timeZ>=timeToChangeDirection){
		actual_direction=rand()%4;
		timeZ=0;
	}
	timeZ++;
	move(directions[actual_direction],speed);
}
void BlueZombie::sendCreateEvent(){
	EventoNuevoElemento *evento;
	evento = new EventoNuevoElemento(ZAZUL,id,position.x,position.y);
	this->gameCore.getEventList().addEvent(evento);
}