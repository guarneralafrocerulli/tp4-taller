#ifndef ZOMBIE_VERDE
#define ZOMBIE_VERDE
#include "zombie.h"

class GreenZombie : public Zombie{
public:
	GreenZombie(Coordinate& coordinate, GameCore& gameCore);
	void sendCreateEvent();
	void tick();
	int getPoints(){return 10;}
};

#endif
