#include "fire.h"
#include <server/game/gameCore.h>
#include <math.h>
#include <common/eventos.h>

Fire::Fire(Coordinate& coordinate, GameCore& gameCore,
		   int ownerId, Direction direction):
	Placeable(coordinate, gameCore){
	this->initPos.x=coordinate.x;
	this->initPos.y=coordinate.y;
	this->speed=20;//velocidad por defecto
	this->range=3;//alcance por defecto 3 tiles
	this->owner=ownerId;
	this->direction = direction;
}

void Fire::tick(){
	/*si se supera la distancia permitida desaparece*/
	double aux=(position.x-initPos.x)*(position.x-initPos.x)
	+ (position.y-initPos.y)*(position.y-initPos.y);
	double distance=sqrt(aux);
	if(distance>range*width){
		kill();
	}
	move(direction,speed);
}

bool Fire::collide(Direction direction, int speed, Placeable* placeable){
	return placeable->collide(direction,speed,this);
}

bool Fire::collide(Direction, int, Zombie* zomb){
	bool collided = isColliding(zomb);
	if(collided){
		zomb->hit(getOwner());
		kill();
	}
	return false;
}
bool Fire::collide(Direction, int, Bomberman* bomber){
	bool collided = isColliding(bomber);
	if(collided){
		bomber->kill();
		kill();
	}
	return false;
}

bool Fire::collide(Direction, int, Box* box){
	bool collideX = (this->position.x > (box->getPosition().x - this->width)) &&
					(this->position.x < (box->getPosition().x + box->width));
	bool collideY = (this->position.y > (box->getPosition().y - this->height)) &&
					(this->position.y < (box->getPosition().y + box->height));
	bool collided = collideX && collideY;
	if(collided){
		box->hit();
		kill();
	}
	return collided;
}

void Fire::sendCreateEvent(){
	EventoNuevoElemento* event = new EventoNuevoElemento(FIRE,
			this->id, this->position.x, this->position.y);
	this->gameCore.getEventList().addEvent(event);
}