#include "bomberman.h"
#include <common/eventos.h>
#include <common/tiposEventoNuevoElemento.h>
#include <server/game/gameCore.h>
#include <cstdio>

Bomberman::Bomberman(Coordinate& coordinate, GameCore& gameCore):
	Placeable(coordinate, gameCore){
	timeToBomb=0;
		/*height=64;
		width=50;*/
}
void Bomberman::tick(){
	timeToBomb--;
}
bool Bomberman::canCreateBomb(){
	if(timeToBomb<=0) return true;
	return false;
}
void Bomberman::resetTimeToBomb(){
	timeToBomb=30;
}
void Bomberman::sendCreateEvent(){
	printf("Enviando un nuevo bomberman: id: %d ; pos: %d,%d",
			this->id, this->position.x, this->position.y);
	EventoNuevoElemento* event = new EventoNuevoElemento(BOMBERMAN,
			this->id, this->position.x, this->position.y);
	gameCore.getEventList().addEvent(event);
}

bool Bomberman::collide(Direction, int, Zombie* zomb){
		bool collided=isColliding(zomb);
		if (collided){
			kill();
		}
		return collided;
}

bool Bomberman::collide(Direction direction,int speed,Placeable* plac){
	return plac->collide(direction, speed, this);
}
void Bomberman::kill(){
	/*desactiva y envia evento modificar visibilidad a la lista
	 * de eventos*/
	this->active=false;
	EventoModificarVisibilidad* vis=new EventoModificarVisibilidad(id,false);
	gameCore.getEventList().addEvent(vis);
}