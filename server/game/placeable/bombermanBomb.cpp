#include "bombermanBomb.h"
#include "zombie.h"
#include <server/game/gameCore.h>
BombermanBomb::BombermanBomb(Coordinate& coordinate, GameCore& gameCore):
	Bomb(coordinate, gameCore){
	this->timeToDie = 80;
}

void BombermanBomb::tick(){
	if(! --timeToDie){
		explode();
	}
}

bool  BombermanBomb::collide(Direction direction, int speed, Placeable* placeable){
    return placeable->collide(direction, speed, this);
}

bool BombermanBomb::collide(Direction direction, int speed, Zombie* zomb){
	bool collided=isColliding(zomb);
	if(collided){
		zomb->collide(direction,  speed,this);
	}
	return collided;
}

void BombermanBomb::sendCreateEvent(){
	EventoNuevoElemento* event = new EventoNuevoElemento(BOMBAB,
										this->id, this->position.x, this->position.y);
	this->gameCore.getEventList().addEvent(event);
}