#ifndef PLACEABLE_LIST_H
#define PLACEABLE_LIST_H
#include "direction.h"
#include <common/coordinate.h>
#include <common/LevelGrid.h>
#include <vector>
#include <common/mutex.h>

class EventList;
class Placeable;
class Zombie;
class WoodBox;
class GameClient;

class PlaceableList{
public:
	PlaceableList(){zombies=0;}
	~PlaceableList();
	/**
	 * Agrega un ubicable y devuelve su id
	 */
	int addPlaceable(Placeable* placeable);
	/**
	 * Devuelve un objeto placeable según su Id. Los jugadores, además de
	 * tener su id por gameClient, tienen una instancia Placeable con otro
	 * id distinto.
	 */
	Placeable* getPlaceableById(int id);
	/**
	 * Prueba colisionar contra todos los objetos, siempre de a uno.
	 */
	Coordinate collide(Direction direction, int speed, Placeable* placeable);
	/**
	 * Tick a todos los objetos
	 */
	void tick();
	void subZombie(){zombies--;} //cuando muere
	void addZombie(){zombies++;}//cuando se crea un zombie 
	/*deletea los elemenos de la placeable list y 
	 * envia evento modificar visibilidad a los 
	 * clientes*/
	int getNumberOfZombies(){return zombies;}
	void clean();
	std::vector<Placeable*> getPlaceableList(){return placeableList;}
private:
	//No se pueden agregar items y chequear colisiones en simultaneo
	int zombies;/// cantidad de zombies del nivel
	Mutex mutex;
	std::vector<Placeable*> placeableList;
	
};
#endif
