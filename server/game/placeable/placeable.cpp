#include "placeable.h"
#include "box.h"
#include <server/game/gameCore.h>
#include <common/eventos.h>
#include <cstdio>

Placeable::Placeable(Coordinate& coordinate, GameCore& gameCore)
	: gameCore(gameCore){
	this->position = coordinate;
	this->active = true;
}

Placeable::~Placeable(){
}

bool Placeable::isColliding(Placeable* placeable){
	/*devuelve true si hay colision*/
	bool collideX = (this->position.x > (placeable->getPosition().x - this->width)) &&
					(this->position.x < (placeable->getPosition().x + placeable->width));
	bool collideY = (this->position.y > (placeable->getPosition().y - this->height)) &&
					(this->position.y < (placeable->getPosition().y + placeable->height));
	bool collided = collideX && collideY;
	return collided;
}

void Placeable::tick(){
}

Coordinate Placeable::getPosition(){
	return this->position;
}

void Placeable::setPosition(Coordinate& position){
	this->position = position;
}

void Placeable::setId(unsigned int id){
	this->id = id;
}

Coordinate Placeable::move(Direction direction, int speed){
	/**
	 * Primero realizo el movimiento
	 */
	switch (direction){
		case RIGHT:
			this->position.x += speed;
			break;
		case LEFT:
			this->position.x -= speed;
			break;
		case UP:
			this->position.y -= speed;
			break;
		case DOWN:
			this->position.y += speed;
			break;
	}
	/**
	 * Ahora realizo los ajustes por colisión
	 */
	this->position = this->gameCore.getPlaceableList()
					.collide(direction, speed, this);
	/**
	 * Envío update de posición a todos los jugadores
	 */
	EventoPosicion* event = new EventoPosicion(this->id,
												this->position.x,
												this->position.y);
	 this->gameCore.getEventList().addEvent(event);
	return this->position;
}

bool Placeable::collide(Direction, int, Placeable*){
	return false;
}

bool Placeable::collide(Direction, int, Bomberman*){
	return false;
}

bool Placeable::collide(Direction direction, int, Box* box){
	bool collided = isColliding(box);
	if (collided){
		switch (direction){
			case DOWN:
				this->position.y = (box->position.y - this->height);
				break;
			case UP:
				this->position.y = (box->position.y + box->height);
				break;
			case RIGHT:
				this->position.x = (box->position.x - this->width);
				break;
			case LEFT:
				this->position.x = (box->position.x + box->width);
				break;
		}
	}
	return collided;
}

bool Placeable::collide(Direction, int, Fire*){
	return false;
}

bool Placeable::collide(Direction, int, PlantBomb*){
	return false;
}

bool Placeable::collide(Direction, int, Zombie*){
	return false;
}

bool Placeable::isActive(){
	return active;
}

void Placeable::setKilledBy(int){
}

void Placeable::kill(){
	/*desactiva y encola evento modificar visibilidad*/
	active=false;
	EventoModificarVisibilidad* evento=new EventoModificarVisibilidad(id,false);
	this->gameCore.getEventList().addEvent(evento);
}