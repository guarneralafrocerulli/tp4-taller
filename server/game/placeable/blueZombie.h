#ifndef BLUE_ZOMBIE
#define BLUE_ZOMBIE
#include "zombie.h"

class BlueZombie:public Zombie{
public:
	int getPoint(){return 20;}
	BlueZombie(Coordinate& coordinate, GameCore& gameCore);
	void tick();
	virtual void sendCreateEvent();
private:
	int timeToChangeDirection;
};


#endif