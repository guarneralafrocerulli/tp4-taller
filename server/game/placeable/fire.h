#ifndef FIRE_H
#define FIRE_H
#include "placeable.h"
#include "zombie.h"
class Fire:public Placeable{
private:
	int range;//cant de tiles q se puede mover
	Direction direction;
	Coordinate initPos;//posicion de inicio
	int speed;
	int owner;
public:
	Fire(Coordinate& coordinate, GameCore& gameCore, int ownerId, Direction direction);
	void setSpeed(int speed){this->speed=speed;}
	/*para saber a q jugador darle puntos solo usado si 
	 * la bomba era de un bomberman*/
	void setRange(int range){this->range=range;}
	void sendCreateEvent();
	void tick();
	bool collide(Direction direction, int speed, Bomberman* bomber);
	bool collide(Direction direction, int speed, Box* box);
	//void kill();
	bool collide(Direction direction, int speed, Placeable* placeable);
	bool collide(Direction direction, int speed, Zombie* placeable);
	int getOwner(){return owner;}
};


#endif