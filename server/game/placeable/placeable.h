#ifndef PLACEABLE_H
#define PLACEABLE_H
#include "direction.h"
#include <common/eventos.h>

class Bomberman;
class Box;
class Fire;
class GameCore;
class PlantBomb;
class Zombie;
class ZombieVerde;

class Placeable {
public:
	Placeable(Coordinate& coordinate, GameCore& gameCore);
	virtual ~Placeable();
	Coordinate getPosition();
	void setPosition(Coordinate &position);
	void setId(unsigned int id);
	void activate(){active=true;}
	virtual int getPoints(){return 0;}
	virtual void setKilledBy(int id);
	/**
	 * Devuelve la posición en donde quedó el objeto luego de todas las colisiones.
	 */
	virtual Coordinate move(Direction direction,
							int speed);
	/**
	 * Transcurre una unidad de tiempo
	 */
	virtual void tick();
	virtual int getKilledBy(){return -1;}
	/**
	 * Encolo un evento NuevoElemento
	 */
	virtual void sendCreateEvent() = 0;
	/**
	 * Colisiono 2 objetos, con dispatch
	 */
	void kill();
	bool isColliding(Placeable* placeable);
	virtual bool collide(Direction direction, int speed, Placeable* placeable);
	virtual bool collide(Direction direction, int speed, Bomberman* bomberman);
	virtual bool collide(Direction direction, int speed, Box* box);
	virtual bool collide(Direction direction, int speed, Fire* fire);
	virtual bool collide(Direction direction, int speed, PlantBomb* placeable);
	virtual bool collide(Direction direction, int speed, Zombie* zomb);
	bool isActive();
	static const int width = 64;
	static const int height = 64;
	
protected:
	GameCore& gameCore;
	unsigned int id;
	Coordinate position;
	bool active;
	
};
#endif
