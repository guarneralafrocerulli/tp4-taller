#include "box.h"
#include "greenZombie.h"
#include "redZombie.h"
#include "blueZombie.h"
#include <server/game/gameCore.h>

Box::Box(Coordinate& coordinate, GameCore& gameCore):
	Placeable(coordinate, gameCore){
}

void Box::sendCreateEvent(){
	//No hago nada, porque el cliente ya carga los sprites aparte
}

bool Box::collide(Direction direction, int speed, Placeable* placeable){
    return placeable->collide(direction, speed, this);
}

SteelBox::SteelBox(Coordinate& coordinate,
				   GameCore& gameCore)
: Box(coordinate, gameCore){
}

WoodBox::WoodBox(Coordinate& coordinate,
				  GameCore& gameCore)
: Box(coordinate, gameCore){
	zombie=HIDE_NONE;
	crash=false;
}

void WoodBox::hit(){
	crash=true;
}

void WoodBox::sendCreateEvent(){
	EventoNuevoElemento* event = new EventoNuevoElemento(CAJA,
			this->id, this->position.x, this->position.y);
	this->gameCore.getEventList().addEvent(event);
}

void WoodBox::hideZombie(int type){
/*permite setear el tipo de zombie q se 
 * esconde en la caja*/
	this->gameCore.getPlaceableList().addZombie();//sumo 1 al contador de zombies
	zombie=type;
}
void WoodBox::tick(){
	if (crash){
		kill();
		Zombie* zomb=obtainZombie();
		if(zomb){
			/*resto el zombie q sume cuando cree la caja 
			* ya q ahora se va a sumar uno cuando cree el
			* zombie*/
			this->gameCore.getPlaceableList().subZombie(); 
			this->gameCore.getPlaceableList().addPlaceable(zomb);
		}
	}
}

Zombie* WoodBox::obtainZombie(){
/*devuelve el zombie escondido en la caja, null si no hay*/
	Zombie* hidingZombie=NULL;
	Coordinate coor(position.x,position.y);
	switch(zombie){
		case HIDE_NONE:
			break;
		case HIDE_GREEN:
			hidingZombie=new GreenZombie(coor, gameCore);
			break;
		case HIDE_RED:
			hidingZombie=new RedZombie(coor,gameCore);
			break;
		case HIDE_BLUE:
			hidingZombie=new BlueZombie(coor, gameCore);
			break;	
	}
	return hidingZombie;
}