#include "redZombie.h"
#include <server/game/gameCore.h>
#include <cstdlib>
#include <ctime>
RedZombie::RedZombie(Coordinate& coordinate, GameCore& gameCore)
	: Zombie(coordinate, gameCore){
		speed=5;
		hitPoints=3;
		timeZ=0;
		timeToChangeDirection=100;
}
void RedZombie::tick(){
	if(timeZ>=timeToChangeDirection){
		changeDirection();
	}
	timeZ++;
	move(directions[actual_direction],speed);
}

void RedZombie::changeDirection(){
	actual_direction=rand()%4;
	timeZ=0;
}

void RedZombie::hit(int id){
	changeDirection();
	hitPoints--;
	if(hitPoints<=0) {
		setKilledBy(id);
		kill();
		this->gameCore.getPlaceableList().subZombie();
	}
}
void RedZombie::sendCreateEvent(){
	EventoNuevoElemento *evento;
	evento = new EventoNuevoElemento(ZROJO,id,position.x,position.y);
	this->gameCore.getEventList().addEvent(evento);
}
