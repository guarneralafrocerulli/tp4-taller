#ifndef BOMBERMAN_BOMB_H
#define BOMBERMAN_BOMB_H
#include "bomb.h"

class BombermanBomb: public Bomb{
protected:
	  int timeToDie;
public:
    void setOwner(int id){owner=id;}
    int getOwner(){return owner;}
    BombermanBomb(Coordinate& coordinate, GameCore& gameCore);
    void tick();
    //void explode();
    virtual void sendCreateEvent();
    bool collide(Direction direction, int speed, Zombie* zomb);
    bool collide(Direction direction, int speed, Placeable* placeable);
};

#endif