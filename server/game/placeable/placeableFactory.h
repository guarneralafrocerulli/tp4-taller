#ifndef PLACEABLE_FACTORY_H
#define PLACEABLE_FACTORY_H
#include <cstdlib>
#include <ctime>
#define GRID_WIDTH 64
#define GRID_HEIGHT 64

class GameCore;
class Placeable;

class PlaceableFactory{
public:
	PlaceableFactory(GameCore& gameCore);
	/**
	 * El valor de 'x' e 'y' es de la posición de la grilla, ¡no coordenada!
	 */
	Placeable* createPlaceable(int type, int x, int y);
private:
	GameCore& gameCore;
};
#endif
