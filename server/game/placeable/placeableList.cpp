#include "placeableList.h"
#include "placeable.h"
#include "zombie.h"
#include "box.h"
#include <common/LevelGrid.h>
#include <cstdio>

int PlaceableList::addPlaceable(Placeable* placeable){
	Lock lock(mutex);
	int id = placeableList.size();
	this->placeableList.push_back(placeable);
	placeable->setId(id);
	//Encolo el evento de que se agregó el placeable a la lista.
	placeable->sendCreateEvent();
	return id;
}


Placeable* PlaceableList::getPlaceableById(int id){
	return this->placeableList[id];
}

/**
 * @Param placeable: el objeto que se movio y se va a colisionar con 
 * cada uno de los objetos de la lista.
 */
Coordinate PlaceableList::collide(Direction direction,
								  int speed,
								  Placeable* placeable){
	Lock lock(mutex);
	/**
	 * Acá debería chequear contra todos los objetos si alguno no puede traspasar, o si 
	 * con alguno ocurre algún evento (onda, si toco un zombie, muero)
	 * Resuelvo las colisiones con dispatch.
	 */
	for (unsigned int i = 0; i < this->placeableList.size(); ++i){
		if(this->placeableList.at(i)->isActive()){
			this->placeableList.at(i)->collide(direction, speed, placeable);
		}
	}
	return placeable->getPosition();
}

PlaceableList::~PlaceableList(){
	for (unsigned int i = 0; i < this->placeableList.size(); ++i){
		delete this->placeableList[i];
	}
}
void PlaceableList::clean(){
	/*desactiva los elementos de la placeable list*/
	zombies=0;
	for (unsigned int i = 0; i < this->placeableList.size(); ++i){
		this->placeableList[i]->kill();
	}
}

void PlaceableList::tick(){
	for (unsigned int i = 0; i < this->placeableList.size(); ++i){
		if(this->placeableList[i]->isActive()){
			this->placeableList[i]->tick();
		}
	}
}

