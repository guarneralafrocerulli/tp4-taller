#include "zombie.h"
#include <server/game/gameCore.h>
#include <cstdlib>
#include <ctime>
#include "common/eventos.h"

Zombie::Zombie(Coordinate& coordinate, GameCore& gameCore):
	Placeable(coordinate, gameCore){
	gameCore.getPlaceableList().addZombie();// sumo 1 a la cantidad de zombies de la lista
	hitPoints=1;
	this->speed=speed;
	inicial.x=coordinate.x;
	inicial.y=coordinate.y;
	actual_direction=rand()%2;
	directions[0]=LEFT;
	directions[1]=DOWN;
	directions[2]=RIGHT;
	directions[3]=UP;
}

bool Zombie::collide(Direction direction, int, Box* box){
	bool collided=isColliding(box);
	if (collided){
		switch (direction){
			case DOWN:
				this->position.y = (box->getPosition().y - this->height);
				break;
			case UP:
				this->position.y = (box->getPosition().y + box->height);
				break;
			case RIGHT:
				this->position.x = (box->getPosition().x - this->width);
				break;
			case LEFT:
				this->position.x = (box->getPosition().x + box->width);
				break;
		}
		if(actual_direction==0){
			actual_direction=2;
		}else if(actual_direction==1) {
			actual_direction=3;
		}else if(actual_direction==2){
			actual_direction=0;
		}else if(actual_direction==3){
			actual_direction=1;
			}
	}
	
	return collided;
}
void Zombie::kill(){
	if(killedBy!=-1){
		std::map<int,int>::iterator clients;
		for (clients=gameCore.getClientPlaceableMap().begin();clients!=gameCore.getClientPlaceableMap().end();++clients){
			if ((*clients).second==killedBy){
				(gameCore.getClientList().getClientById((*clients).first))->addPoints(this->getPoints());
				EventoModificarPuntuacion* evento=new EventoModificarPuntuacion(killedBy,gameCore.getClientList().getClientById((*clients).first)->getPoints());
				gameCore.getEventList().addEvent(evento);
				}
		}
	}
	active=false;
	EventoModificarVisibilidad* evento=new EventoModificarVisibilidad(id,false);
	this->gameCore.getEventList().addEvent(evento);
}
void Zombie::hit(int id){
	hitPoints--;
	std::cerr<<"Hits:"<<hitPoints<<std::endl;
	if(hitPoints<=0) {
		setKilledBy(id);
		kill();
		this->gameCore.getPlaceableList().subZombie();
	}
}
bool Zombie::collide(Direction direction,int speed,Fire* fire){
	return fire->collide(direction,speed,this);
}

bool Zombie::collide(Direction direction,int speed,Bomberman* bomberman){
	return bomberman->collide(direction, speed, this);
}
bool Zombie::collide(Direction direction, int speed, Placeable* placeable){
    return placeable->collide(direction, speed, this);
}


bool Zombie::collide(Direction direction, int, BombermanBomb* box){
	bool collided=isColliding(box);
	if (collided){
		switch (direction){
			case DOWN:
				this->position.y = (box->getPosition().y - this->height);
				break;
			case UP:
				this->position.y = (box->getPosition().y + box->height);
				break;
			case RIGHT:
				this->position.x = (box->getPosition().x - this->width);
				break;
			case LEFT:
				this->position.x = (box->getPosition().x + box->width);
				break;
		}
		if(actual_direction==0){
			actual_direction=2;
		}else if(actual_direction==1) {
			actual_direction=3;
		}else if(actual_direction==2){
			actual_direction=0;
		}else if(actual_direction==3){
			actual_direction=1;
			}
	}
	return collided;
}