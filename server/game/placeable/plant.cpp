#include "plant.h"
#include "plantBomb.h"
#include <server/game/gameCore.h>

Plant::Plant(int speed,Direction dir, Coordinate& coordinate, GameCore& gameCore):
	Placeable(coordinate, gameCore){
	this->timeToShoot=speed;
	this->direction=dir;
	timePassed=0;
}


void Plant::sendCreateEvent(){
	/*el cliente lee la planta desde el mapa*/
}
 
bool Plant::collide(Direction, int, Placeable*){
    return false;
}

bool Plant::collide(Direction, int, Box*){
    return false;
}

bool Plant::collide(Direction, int, ZombieVerde*){
    return false;
}

void Plant::tick(){
	if(timePassed>=timeToShoot){
		Coordinate cor;
		cor.x=this->getPosition().x;
		cor.y=this->getPosition().y;
		PlantBomb* bomb=new PlantBomb(cor, this->gameCore, 5,direction);
		this->gameCore.getPlaceableList().addPlaceable(bomb);
		timePassed=0;
	}
	timePassed++;
}	