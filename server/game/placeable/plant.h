#ifndef PLANT
#define PLANT
#include "placeable.h"
#include "direction.h"
#define GREENFREQ 300
#define REDFREQ 100
#define BLUEFREQ 150
class Plant:public Placeable{
protected:
	Direction direction;//la direccion a donde dispara
	int timeToShoot;//la frecuencia de disparo
	int timePassed; //tiempo q paso desde el ultimo disparo
public:
	Plant(int speed, Direction dir, Coordinate& coordinate, GameCore& gameCore);
	void tick();
	void setTimeToShoot(int time){timeToShoot=time;}
	 void sendCreateEvent();
	 bool collide(Direction direction, int speed, Box* box);
	 bool collide(Direction direction, int speed, Placeable* placeable);
	 bool collide(Direction direction, int speed, ZombieVerde* zomb);
};


#endif