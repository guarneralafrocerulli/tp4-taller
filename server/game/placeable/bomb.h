#ifndef BOMB_H
#define BOMB_H
#include "placeable.h"

class Bomb: public Placeable{
protected:
	  //int timeToDie;
	  int owner;
public:
    Bomb(Coordinate& coordinate, GameCore& gameCore);
    virtual void tick(){};
    virtual void explode();
    virtual void sendCreateEvent(){};
};
#endif
