#ifndef CAMPAIGN_LOADER_H
#define CAMPAIGN_LOADER_H
#include <common/Campania.h>
#include <vector>
#include <string>

class CampaignLoader{
public:
	~CampaignLoader();
	void loadCampaigns(std::string &dir);
	std::vector<PartidaCampania> getCampaignInfo();
	Campania getCampaign(int id);
	ImagenCampania* getImage(int img);
private:
	std::vector<Campania> campaignList;
	/**
	 * Clase con el nombre, cantidad de jugadores y dificultad de la campaña
	 */
	std::vector<ImagenCampania*> picture;
	std::vector<PartidaCampania> campaignInfo;
};
#endif
