#include "gameThread.h"
#include <cstdio>

GameThread::GameThread(Campania &campania, std::string &name,
					   int maxPlayers, unsigned int id)
	: gameInfo(id, name, campania.getDificultad(), maxPlayers){
	this->playerList.setMaxPlayers(maxPlayers);
	this->exit = false;
	this->started = false;
	this->game = new GameCore(campania);
}

GameThread::~GameThread(){
	delete this->game;
}

void GameThread::runFunction(){
	this->started = true;
	game->startCampaign(this->playerList);
	this->idleSleepTime.tv_sec = 0;
	this->idleSleepTime.tv_nsec = 50000000; //5.10^7 nSec = 50mSec
	while (!this->exit){
		this->game->tick();
		this->game->sendUpdates();
		this->exit = this->game->hasEnded();
		nanosleep(&this->idleSleepTime, 0);
	}
}

bool GameThread::addClient(GameClient* client){
	bool added = this->playerList.addClient(client);
	if (added){
		client->setGame(this);
	}
	return added;
}

GameCore* GameThread::getGameCore(){
	return this->game;
}

PartidaCampania GameThread::getGameInfo(){
	return this->gameInfo;
}

bool GameThread::clientsReady(){
	return this->playerList.ready();
}

void GameThread::startGame(){
	Lock lock(this->startMutex);
	if (!this->started){
		printf("El juego arrancó\n");
		this->run();
	}
}

void GameThread::broadcast(string &message){
	this->playerList.broadcast(message);
}

void GameThread::endGame(GameClient* client){
	printf("El cliente %s salió de la partida\n", client->getName().c_str());
	this->game->endGame();
	stop();
}

void GameThread::stop(){
	this->exit = true;
}
