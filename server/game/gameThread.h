#ifndef GAME_THREAD_H
#define GAME_THREAD_H
#include <ctime>
#include <common/thread.h>
#include <server/network/clientList.h>
#include "gameCore.h"

class GameClient;

/**
 * Clase que ejecuta una campaña para los GameClient conectados
 */
class GameThread : public Thread{
public:
	explicit GameThread(Campania &campania, std::string &name,
						int maxPlayers, unsigned int id);
	virtual ~GameThread();
	virtual void runFunction();
	bool addClient(GameClient* client);
	GameCore* getGameCore();
	PartidaCampania getGameInfo();
    bool clientsReady();
	/**
	 * Ejecuta run solo si el juego no se inició
	 */
    void startGame();
	/**
	 * Le digo a todos los jugadores que ya terminó la partida
	 */
    void broadcast(string &message);
    void endGame(GameClient* client);
	void stop();
    void setId(unsigned int currentId);
private:
	Mutex startMutex;
	ClientList playerList;
	bool started, exit;
	GameCore* game;
	PartidaCampania gameInfo;
	struct timespec idleSleepTime;
};

#endif
