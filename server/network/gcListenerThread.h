#ifndef GC_LISTENER_THREAD_H
#define GC_LISTENER_THREAD_H
#include <common/thread.h>

class GameClient;

/**
 * Clase que se encarga de escuchar los comandos enviados por el cliente
 * y ejecutarlos
 */
class GCListenerThread : public Thread{
public:
    explicit GCListenerThread(GameClient& client);
    virtual void runFunction();
    void stop();
private:
	GameClient& client;
	bool exit;
};

#endif
