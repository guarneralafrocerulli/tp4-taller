#include "gcListenerThread.h"
#include "gameClient.h"
#include <server/operation/operationFactory.h>
#include <server/operation/operation.h>
#include <common/socket.h>
#include <string>

GCListenerThread::GCListenerThread(GameClient& client)
	: client(client){
	this->exit = false;
}

void GCListenerThread::runFunction(){
	try{
		while (!exit){
			std::string msg = client.receive();
			printf("Recibi un evento: %s", msg.c_str());
			Operation* op = OperationFactory::getOperation(msg);
			//Las operaciones tienen que ser rápidas para no trabar el socket
			op->execute(&client);
			delete op;
		}
	} catch(char const* e){
		printf("[ERROR] Se produjo un error de lectura en uno de los sockets de los clientes - %s\n", e);
	}
}

void GCListenerThread::stop(){
	this->exit = true;
}
