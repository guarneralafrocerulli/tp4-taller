#ifndef CLIENT_LIST_H
#define CLIENT_LIST_H
#include <common/socket.h>
#include <map>
#include "gameClient.h"
#include <common/eventList.h>

class ClientList{
public:
	ClientList();
	~ClientList();
	/**
	 * Devuelvo true si se pudo agregar el cliente.
	 */
	bool addClient(GameClient* gameClient);
	void setMaxPlayers(int maxPlayers);
	bool listFull();
	GameClient* getClientById(int id);
	void closeClients();
	void resetLives();
	void deleteClients();
	void removeDisconnected();
	std::vector<GameClient*> getGameClientList();
    bool ready();
	void sendUpdates(EventList &eventList);
	void broadcast(std::string &message);
private:
	unsigned int maxPlayers;
	std::map<unsigned int, GameClient*> gameClientList;
};
#endif
