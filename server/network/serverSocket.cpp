#include "serverSocket.h"
#include <common/logger.h>
#include <string>
#include <cstring> //memset
#include <arpa/inet.h>

const int listenBacklog = 10;

ServerSocket::ServerSocket(){
}


ServerSocket::ServerSocket(int port){
	start(port);
}

Socket* ServerSocket::acceptClient(){
	Logger::getInstance()->log("Esperando un cliente");
	int clientFd = 0;
	struct sockaddr_in address;
	unsigned int sSize = sizeof(struct sockaddr_in);
	clientFd = accept(this->fd, (struct sockaddr *) &address, &sSize);
	if (clientFd == -1){
		throw "El server no aceptó ninguna conexión";
	}
	int clientIP = address.sin_addr.s_addr;
	char auxStr[INET_ADDRSTRLEN];
	inet_ntop(AF_INET, &clientIP, auxStr, INET_ADDRSTRLEN);
	return new Socket(clientFd, address);
}

void ServerSocket::start(int port){
	this->fd = socket(PF_INET, SOCK_STREAM, 0);
	//Inicio el puerto
	this->address.sin_family = AF_INET;
	this->address.sin_port = htons(port);
	this->address.sin_addr.s_addr = INADDR_ANY;
	memset(this->address.sin_zero, 0, sizeof this->address.sin_zero);
	//Me bindeo al puerto
	struct sockaddr *bindAddress = (struct sockaddr *) &this->address;
	int yes=1;
	setsockopt(this->fd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int));
	bind(this->fd, bindAddress, sizeof(struct sockaddr_in));
	//Inicio la cola del listen
	listen(this->fd, ServerSocket::listenBacklog);
}
