#include "gameClient.h"
#include <server/serverThread.h>
#include <common/socket.h>
#include <common/eventos.h>
#include <common/Serializadores.h>
#include <cstdio>
#include <string>

/**
 * El id del jugador podría ser generado por una clase idGenerator, que
 * genere ids únicos,pero el fileDescriptor del socket cumple lo
 * que necesito, así que lo aprovecho.
 */
GameClient::GameClient(Socket* socket, ServerThread &server)
 :listener(*this), server(server){
	this->name = "(No name)";
	this->ready = false;
	this->connected = true;
	this->socket = socket;
	this->id = socket->getFD();
	this->listener.run();
	points=0;
	lives=3;
}

GameClient::~GameClient(){
	this->listener.stop();
	this->listener.join();
	delete this->socket;
}

void GameClient::addPoints(int point){
	points=points+point;
}

int GameClient::getId(){
	return this->id;
}

void GameClient::setPlayerName(string& name){
	this->name = name;
}

string GameClient::getName(){
	return this->name;
}

void GameClient::sendInitData(int idPlaceable){
	EventoAsignarIdJugador jugadorNuevo(idPlaceable);
	SerializadorEvento serializador;
	std::string strEvent(serializador.serializar(&jugadorNuevo));
	this->send(strEvent);
}

void GameClient::send(std::string &message){
	this->socket->sendString(message);
}

std::string GameClient::receive(){
	return this->socket->recvString();
}

GameThread* GameClient::getGame(){
	return this->game;
}

void GameClient::setGame(GameThread* game){
	this->game = game;
}

ServerThread* GameClient::getServer(){
	return &this->server;
}

void GameClient::createGame(int campaignId,
							std::string &campaignName,
							int maxPlayers){
	this->gameId = this->server.createGame(campaignId, campaignName, maxPlayers);
	this->server.joinGame(this, gameId, false);
}

void GameClient::joinGame(int gameId){
	this->server.joinGame(this, gameId, true);
	this->gameId = gameId;
}

void GameClient::startGame(){
	this->server.startGame(this->gameId);
}

bool GameClient::getReady(){
	return this->ready;
}

void GameClient::setReady(bool ready){
	this->ready = ready;
}

bool GameClient::isConnected(){
	this->connected &= this->socket->isConnected();
	return this->connected;
}

void GameClient::exit(){
	/**
	 * Le aviso a todos los jugadores que terminó la partida.
	 */
	this->game->endGame(this);
	//Destruyo la instancia del juego. (El juego no conoce al server)
	this->server.destroyGame(this->game);
}


void GameClient::close(){
	this->connected = false;
	this->socket->shutdownSocket();
	this->socket->closeSocket();
}
