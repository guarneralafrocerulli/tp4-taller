#ifndef GAME_CLIENT_H
#define GAME_CLIENT_H
#include <common/sharedPtr.hpp>
#include "gcListenerThread.h"
#include <string>

class GameThread;
class ServerThread;
class Socket;

class GameClient{
public:
	GameClient(Socket* socket, ServerThread &server);
	int getId();
	void setPlayerName(std::string &name);
	std::string getName();
	void send(std::string &message);
	std::string receive();
	void close();
	void sendInitData(int idPlaceable);
	ServerThread* getServer();
	bool isConnected();
	/**
	//Métodos de juego
	 * Creo un juego nuevo, tomando como base la campaña con el id seleccionado
	 */
	void createGame(int campaignId, std::string &campaignName, int maxPlayers);
	/**
	 * Me uno a la partida con el id seleccionado
	 */
	void joinGame(int gameId);
	GameThread* getGame();
	void setGame(GameThread* gameThread);
	void startGame();
	void subLive(){lives--;}
	int getLives(){return lives;}
	void addPoints(int point);
	int getPoints(){return points;}
	void resetLives(){lives=4;}
	bool getReady();
	void setReady(bool ready);
	/**
	 * Se llama cuando el cliente se desconecta voluntariamente
	 */
	void exit();
	~GameClient();

private:
	int lives; //vidas del jugador
	int points; // puntos del jugador
	bool ready;
	GCListenerThread listener;
	Socket* socket;
	std::string name;
	ServerThread& server;
	GameThread* game;
	bool connected;
	int id;
	// Para saber a qué juego se conectan
	int gameId;
};
#endif
