#include "clientList.h"
#include <common/Serializadores.h>
#include <vector>

ClientList::ClientList(){
}

ClientList::~ClientList(){
}

bool ClientList::addClient(GameClient* gameClient){
	if (this->gameClientList.size() < this->maxPlayers){
		this->gameClientList[gameClient->getId()] = gameClient;
		printf("El cliente %s entra en la partida de %d jugadores\n",
			gameClient->getName().c_str(), this->maxPlayers);
		return true;
	}
	printf("El cliente %s no entra en la partida de %d jugadores\n",
			gameClient->getName().c_str(), this->maxPlayers);
	return false;
}
void ClientList::resetLives(){
	std::map<unsigned int, GameClient*>::iterator cliente;
	for (cliente=gameClientList.begin();cliente!=gameClientList.end();++cliente){
		(*cliente).second->resetLives();
	}
}

void ClientList::setMaxPlayers(int maxPlayers){
	this->maxPlayers = maxPlayers;
}

bool ClientList::listFull(){
	return this->gameClientList.size() == this->maxPlayers;
}

GameClient* ClientList::getClientById(int id){
	return this->gameClientList.at(id);
}

void ClientList::closeClients(){
	EventoFinDelJuego event;
	EventoListaPuntuacion puntuaciones;
	SerializadorEvento ser;
	std::map<unsigned int, GameClient*>::iterator it;
	for (it = gameClientList.begin(); it != gameClientList.end(); ++it){
		GameClient* client = it->second;
		puntuaciones.agregarJugador(client->getName(),client->getPoints());
	}
	//client->close();
	for (it = gameClientList.begin(); it != gameClientList.end(); ++it){
		GameClient* client = it->second;
		try{
			std::string strEvent(event.serializar(&ser));
			client->send(strEvent);
			std::string strPuntos(puntuaciones.serializar(&ser));
			client->send(strPuntos);
		} catch (const char* exception){
			printf("[Error] No se pudo enviar fin del juego al cliente %s.\n", client->getName().c_str());
		}
		client->close();
	}
}

void ClientList::deleteClients(){
	std::map<unsigned int, GameClient*>::iterator it;
	for (it = gameClientList.begin(); it != gameClientList.end(); ++it){
		GameClient* client = it->second;
		client->close();
		delete client;
	}
}

void ClientList::removeDisconnected(){
	std::map<unsigned int, GameClient*>::iterator it;
	for (it = gameClientList.begin(); it != gameClientList.end(); ++it){
		GameClient* client = it->second;
		if (!client->isConnected()){
			printf("Eliminando al cliente %s - %d\n",
				   client->getName().c_str(), client->getId());
			delete client;
		}
		this->gameClientList.erase(it);
	}
}

std::vector<GameClient*> ClientList::getGameClientList(){
	std::vector<GameClient*> result;
	std::map<unsigned int, GameClient*>::iterator it;
	for (it = gameClientList.begin(); it != gameClientList.end(); ++it){
		GameClient* client = it->second;
		result.push_back(client);
	}
	return result;
}

bool ClientList::ready(){
	std::map<unsigned int, GameClient*>::iterator it;
	for (it = gameClientList.begin(); it != gameClientList.end(); ++it){
		GameClient* client = it->second;
		if (!client->getReady()){
			return false;
		}
	}
	return listFull();
}

void ClientList::sendUpdates(EventList& eventList){
	Lock lock(eventList.mutex);
	SerializadorEvento serializador;
	while (!eventList.eventList.empty()){
		Evento* evento=eventList.eventList.front();
		//No concateno eventos porque el cliente lee de a uno
		std::string strEvento = serializador.serializar(evento);
		broadcast(strEvento);
		delete evento;
		eventList.eventList.pop();
	}
}

void ClientList::broadcast(std::string &message){
	std::map<unsigned int, GameClient*>::iterator it;
	for (it = gameClientList.begin(); it != gameClientList.end(); ++it){
		GameClient* client = it->second;
		client->send(message);
	}
}
