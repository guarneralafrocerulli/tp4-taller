#include "serverSocket.h"
#include <server/game/gameThread.h>

ClientHandler::ClientHandler(){
	this->exit = false;
	this->idleSleepTime.tv_sec = 0;
	this->idleSleepTime.tv_nsec = 500000000; //5.10^8 nSec = 500mSec
}

void ClientHandler::runFunction(){
	while (!this->exit){
		if (operationQueue.hasOperation()){
			Operation* op = this->operationQueue.popOperation();
			op->execute(this);
			delete op;
		} else {
			nanosleep(&this->idleSleepTime, 0);
		}
	}
}

void ClientHandler::addClient(Socket* clientSocket){
	this->clientList.addClient(clientSocket, &this->operationQueue);
}

void ClientHandler::createQueue(int clientId){
	int queueId = this->gameQueueManager.createQueue();
	joinClient(clientId, queueId);
}

void ClientHandler::startGame(int queueId){
	GameQueue queue = this->gameQueueManager.getQueueById(queueId);
	this->gameList.createGame(queue);
}


void ClientHandler::joinClient(int clientId, int queueId){
	printf("Uniendo el cliente %d en la cola %d\n", clientId, queueId);
	GameClient* client = this->clientList.getClientById(clientId);
	if (client){
		bool result = this->gameQueueManager.queueGameClient(client, queueId);
	} else {
		printf("No se encontró el cliente\n");
	}
}

void ClientHandler::stop(){
	this->gameList.stopGames();
	this->clientList.deleteClients();
	this->exit = true;
}
