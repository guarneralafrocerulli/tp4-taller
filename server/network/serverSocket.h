#ifndef SERVER_SOCKET_H
#define SERVER_SOCKET_H
#include <common/socket.h>

class ServerSocket : public Socket{
public:
	ServerSocket();
	/**
	 * Inicia el socket y llama a start() para escuchar las conexiones.
	 */
	explicit ServerSocket(int port);
	/**
	 * Devuelve un socket con el fd y address del cliente.
	 */
	Socket* acceptClient();
	/**
	 * Inicia el socket, lo bindea al puerto y llama a
	 * listen para encolar conexiones.
	 */
	void start(int port);
private:
	static const int listenBacklog = 10;
};
#endif
