Bomberman vs Plants vs Zombies
This game was developed for "Programming workshop" subject at FIUBA university, using posix threads and sockets, SDL, and GTKmm.
The project was done in 6 weeks :)
Instructions

In order to compile, you must have:
CrossMake 2.8 or higher.
Gtkmm 3.0 or higher
SDL_Mixer, SDL_Image, SDL_tft (dev libraries)
Tinyxml

Currently, since we use POSIX library, it only works on linux :(

Compile:
Run 
cmake .
make

and make install to copy binaries to the destination folder. Default destination is ~/bomberman

Play:
Start the server application, default connection port is 8080.
Start the client application and configure your ip and port.
Select crear/create (translation doesn't always work) to create a new game 
Select Unirse/Join to join an existing game.

Move your bomberman with directional keys and plant bombs with space.